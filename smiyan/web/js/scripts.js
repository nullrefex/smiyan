// GLOBALS
var smiyan,
    loaderWaiting = 0.2;
// GLOBALS END

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function discount() {
    var discountChance = getRandomInt(0, 3),
        discountCounter = sessionStorage.getItem('discountCounter'),
        $discount = $('.discount'),
        $discountNumbers = $('.discount > span.text, .basket-menu .total');

    if (discountCounter == '') {
        discountCounter = 1;
    }

    $discount.each(function () {
        var $discountScale = $(this).find('.lines > *');

        for (var i = 0; i < discountCounter; i++) {
            var $discountScale = $(this).find('.lines > *');

            $discountScale.eq(i).addClass('active');
        }
    })

    if (discountCounter == 5) {
        $('.discount-close-outer .el-2').addClass('active');
        $discountNumbers.addClass('active');

        return true;
    }

    if (discountChance == 0) {
        var bodyWidth = $('body').width(),
            bodyHeight = $('body').height();
        iconVar = getRandomInt(0, 6),
            iconTop = getRandomInt(200, bodyHeight - 200),
            iconLeft = getRandomInt(30, bodyWidth - 60);

        if (bodyWidth >= 1440) {
            var sideWidth = (bodyWidth - 1380) / 2;

            if (iconLeft > sideWidth) {
                iconLeft = getRandomInt(1380 + sideWidth, bodyWidth - 60);
            }
        } else {
            var iconOnLeftSide = getRandomInt(0, 2);

            if (iconOnLeftSide == true) {
                iconLeft = 15;
            } else {
                iconLeft = bodyWidth - 32 - 15;
            }
        }

        var iconPosition = 'style="top: ' + iconTop + 'px; left: ' + iconLeft + 'px;"';

        $('body').append('<div class="discount-icon var-' + iconVar + '" ' + iconPosition + '></div>');

        $('body').on('click', '.discount-icon', function () {
            $discount = $('body').find('.discount');
            $discountNumbers = $('body').find('.discount > span.text, .basket-menu .total');

            TweenLite.to($(this), 1, {
                opacity: 0,
                display: 'none',
                scale: 0,
                pointerEvents: 'none',
                ease: Back.easeInOut
            });

            discountCounter++;

            sessionStorage.setItem('discountCounter', discountCounter);

            $discount.each(function () {
                var $discountScale = $(this).find('.lines > *');

                $discountScale.eq(discountCounter - 1).addClass('active');
            })

            if (discountCounter == 5) {
                $discountNumbers.addClass('active');
                $(document).on('pjax:complete', '#cart', function() {
                    $('.discount-close-outer .el-2').addClass('active');
                });
                $('.discount-close-outer .el-2').addClass('active');
                TweenLite.to($('.discount-pop-up'), 1, {opacity: 1, visibility: 'visible', ease: Power3.easeInOut});
                smiyan.refreshCart();
                smiyan.updateCheckoutPrice();
                setTimeout(function () {
                    TweenLite.to($('.discount-pop-up'), 1, {opacity: 0, display: 'none', ease: Power3.easeInOut});
                }, 4000);
            }
        });
    }

    return false;
}

smiyan = {
    init: function () {
        var self = this;

        $(function () {
            self.loader();
            self.onLoad();
            self.animatedAnchor();
            self.header();
            self.footer();

            self.sectionType_1();
            self.sectionType_2();
            self.sectionType_3();
            self.sectionType_4();
            self.sectionType_5();
            self.sectionType_6();
            self.sectionType_7();
            self.sectionType_8();
            self.sectionType_9();
            self.sectionType_10();

            var hasDiscount = discount();

            if (hasDiscount) {
                $('.total.title.type-4.var-a').addClass('active');
                $(document).on('pjax:complete', '#cart', function() {
                    $('.discount-close-outer .el-2').addClass('active');
                });
            }
            jQuery('#cart').on('pjax:complete', function () {
                var $scrollbarOuter = $('body').find('.basket-menu .blocks.type-1 .block.type-2');

                $scrollbarOuter.mCustomScrollbar({
                    onTotalScrollOffset: 100,
                    alwaysTriggerOffsets: false
                });

            });
        });
    },
    loader: function () {
        TweenLite.to($('.loader'), 1, {
            delay: loaderWaiting + 0.1,
            opacity: 0,
            display: 'none',
            ease: Power2.easeInOut
        });
    },
    onLoad: function(){
        $(window).load(function(){
            Waypoint.refreshAll();

            $(window).resize(function(){
                Waypoint.refreshAll();
            });
        });
    },
    animatedAnchor: function () {
        $('.anchors').on('click', 'a', function (event) {
            if ($($.attr(this, 'href'))[0]) {
                event.preventDefault();

                $('html, body').animate({
                    scrollTop: $($.attr(this, 'href')).offset().top - 100
                }, 500);
            }
        });
    },
    header: function () {
        if ($('header')[0]) {
            $('.language-list .text').each(function(){
                if ($(this).text().toLowerCase() == 'eng') {
                    $(this).remove();
                }
            })
            
            // MENU
            var menuTl = new TimelineLite(),
                darkBgTl = new TimelineLite(),
                menuToggle = false,
                $menu = $('header menu'),
                $menuBackground = $('header menu .background'),
                $menuBlocks = $('header menu .blocks'),
                $header = $('header'),
                $darkBG = $('.dark-background');

            var menuButtonTl = new TimelineLite(),
                $menuButton = $('.menu-button');

            menuTl
                .fromTo($menu, 0.01, {display: 'none'}, {display: 'block'}, 'first')
                .fromTo($menuBackground, 0.5, {width: 0}, {width: '100%', ease: Power2.easeInOut}, 'first')
                .fromTo($menuBlocks, 0.5, {opacity: 0}, {delay: 0.25, opacity: 1, ease: Power2.easeInOut}, 'first');
            menuTl.pause();

            darkBgTl
                .fromTo($darkBG, 0.5, {opacity: 0, display: 'none'}, {
                    opacity: 1,
                    display: 'block',
                    ease: Power2.easeInOut
                });
            darkBgTl.pause();

            menuButtonTl
                .to($menuButton.find('.lines div:nth-child(1)'), 0.5, {y: 7, ease: Power2.easeInOut}, 'together')
                .to($menuButton.find('.lines div:nth-child(3)'), 0.5, {y: -7, ease: Power2.easeInOut}, 'together')
                .set($menuButton.find('.lines div:nth-child(1)'), {width: '100%'})
                .set($menuButton.find('.lines div:nth-child(2)'), {opacity: 0})
                .set($menuButton.find('.lines div:nth-child(3)'), {width: '100%'})
                .to($menuButton.find('.lines div:nth-child(1)'), 0.5, {rotationZ: 45, ease: Power1.linear}, 'cross')
                .to($menuButton.find('.lines div:nth-child(3)'), 0.5, {rotationZ: -45, ease: Power1.linear}, 'cross');
            menuButtonTl.pause();

            menuButtonTl.timeScale(1.6);

            $menuButton.click(function () {
                if (menuToggle == false) {
                    menuToggle = true;
                    menuButtonTl.play();
                    menuTl.play();
                    darkBgTl.play();

                    $menuButton.addClass('active');
                    $header.addClass('menu-opened');

                    if (basketMenuToggle == true) {
                        basketMenuToggle = false;
                        basketMenuTl.reverse();
                    }
                } else {
                    menuToggle = false;
                    menuTl.reverse();
                    menuButtonTl.reverse();
                    darkBgTl.reverse();

                    $menuButton.removeClass('active');
                    $header.removeClass('menu-opened');
                }
            });

            $darkBG.click(function () {
                menuToggle = false;
                menuTl.reverse();
                menuButtonTl.reverse();
                $menuButton.removeClass('active');
                $header.removeClass('menu-opened');

                darkBgTl.reverse();

                basketMenuToggle = false;
                basketMenuTl.reverse();
            });

            $('header menu .list.type-1 a').on('click', function () {
                menuToggle = false;
                menuTl.reverse();
                menuButtonTl.reverse();
                darkBgTl.reverse();

                $menuButton.removeClass('active');
                $header.removeClass('menu-opened');
            });
            // MENU END

            // BASKET MENU
            var basketMenuTl = new TimelineLite(),
                basketMenuToggle = false,
                $basketMenu = $('.basket-menu'),
                $basketMenuBackground = $('.basket-menu .background'),
                $basketMenuBlocks = $('.basket-menu .blocks'),
                $basketMenuButton = $('.basket-button');

            basketMenuTl
                .fromTo($basketMenu, 0.01, {display: 'none'}, {display: 'block'}, 'first')
                .fromTo($basketMenuBackground, 0.5, {width: 0}, {width: '100%', ease: Power2.easeInOut}, 'first')
                .fromTo($basketMenuBlocks, 0.5, {opacity: 0}, {
                    delay: 0.25,
                    opacity: 1,
                    ease: Power2.easeInOut
                }, 'first');
            basketMenuTl.pause();

            $(document).on('click', $basketMenuButton.selector, function () {
                if (basketMenuToggle == false) {
                    basketMenuToggle = true;
                    basketMenuTl.play();
                    darkBgTl.play();

                    if (menuToggle == true) {
                        menuToggle = false;
                        menuTl.reverse();
                        menuButtonTl.reverse();

                        $menuButton.removeClass('active');
                        $header.removeClass('menu-opened');
                    }
                } else {
                    basketMenuToggle = false;
                    basketMenuTl.reverse();
                    darkBgTl.reverse();
                }
            });

            $(window).on('load', function(){
                var $scrollbarOuter = $('body').find('.basket-menu .blocks.type-1 .block.type-2');

                $scrollbarOuter.mCustomScrollbar({
                    onTotalScrollOffset: 100,
                    alwaysTriggerOffsets: false
                });
            });
            // BASKET MENU END

            // MENU LINKS
            var $menuLinks = $('header menu .list.type-1 ul li a');

            $menuLinks.hover(function () {
                $menuLinks.addClass('inactive');
                $(this).removeClass('inactive');
            }, function () {
                $menuLinks.removeClass('inactive');
            });
            // MENU LINKS END

            var $backCallButton = $('.back-call-button-outer'),
                $backCallButtonEl_2 = $backCallButton.find('.el-2'),
                $backCallPopUp = $('#backcall-popup');

            $backCallButton.hover(function(){
                $backCallButtonEl_2.stop().fadeIn(300);
            }, function(){
                $backCallButtonEl_2.stop().fadeOut(300);
            });

            $backCallPopUp.find('.inner').hover(function(){
                $backCallPopUp.addClass('hovered');
            }, function(){
                $backCallPopUp.removeClass('hovered');
            });

            $backCallPopUp.on('click', function(){
                if(!$(this).hasClass('hovered')) {
                    TweenLite.to($backCallPopUp, 1, {opacity: 0, pointerEvents:'none', ease:Power3.easeInOut});
                }
            });

            $backCallButton.on('click', function(){
                TweenLite.to($backCallPopUp, 1, {opacity: 1, visibility: 'visible', pointerEvents:'all', ease:Power3.easeInOut});
            });
        }
    },
    footer: function () {
        if ($('.section-pagination')[0]) {
            var $footer = $('footer'),
                $sectionPagination = $('.section-pagination'),
                paginationTL = new TimelineLite();

            paginationTL.to($sectionPagination, 0.5, {opacity:0, display:'none'});
            paginationTL.pause();

            var footer_WP = new Waypoint({
                element: $footer,
                handler: function (direction) {
                    if (direction == 'down') {
                        paginationTL.play();
                    } else {
                        paginationTL.reverse();
                    }
                },
                offset: '50%'
            });
        }
    },
    sectionType_1: function () {
        if ($('section.type-1')[0]) {
            var $header = $('header'),
                $sectionPagination = $('.section-pagination'),
                $sectionPaginationTitle = $('.section-pagination .section-title div'),
                $sectionPaginationItem = $('.section-pagination .pagination a'),
                $sectionHeader = $('section.type-1 .section-type-1-header');

            $header.addClass('hidden');

            var headerWP = new Waypoint({
                element: $('body'),
                handler: function (direction) {
                    if (direction == 'down') {
                        $header.removeClass('hidden');

                        $sectionPagination.removeClass('light');
                        $sectionPaginationItem.removeClass('active');
                        $sectionPaginationItem.eq(1).addClass('active');

                        TweenLite.fromTo($sectionPaginationTitle.eq(0), 0.5, {opacity: 1, y: 0}, {
                            opacity: 0,
                            y: 50,
                            ease: Power2.easeInOut
                        });
                        TweenLite.fromTo($sectionPaginationTitle.eq(1), 0.5, {opacity: 0, y: -50}, {
                            opacity: 1,
                            y: 0,
                            ease: Power2.easeInOut
                        });
                    } else {
                        $header.addClass('hidden');

                        $sectionPagination.addClass('light');
                        $sectionPaginationItem.removeClass('active');
                        $sectionPaginationItem.eq(0).addClass('active');
                        TweenLite.fromTo($sectionPaginationTitle.eq(1), 0.5, {opacity: 1, y: 0}, {
                            opacity: 0,
                            y: -50,
                            ease: Power2.easeInOut
                        });
                        TweenLite.fromTo($sectionPaginationTitle.eq(0), 0.5, {opacity: 0, y: 50}, {
                            opacity: 1,
                            y: 0,
                            ease: Power2.easeInOut
                        });
                    }
                },
                offset: '-40%'
            });

            // ANIM
            var sectionType_1_TL = new TimelineLite(),
                $lines_1 = $('section.type-1 .title.type-2 .lines').eq(0),
                $lines_2 = $('section.type-1 .title.type-2 .lines').eq(1);

            sectionType_1_TL
                .from($('.anim-1'), 1.5, {
                    delay: loaderWaiting + 0.1,
                    y: 25,
                    opacity: 0,
                    ease: Power3.easeInOut
                }, 'first')
                .from($('.anim-2'), 1.5, {
                    delay: loaderWaiting + 0.2,
                    y: 35,
                    opacity: 0,
                    ease: Power3.easeInOut
                }, 'first')
                .from($('.anim-3'), 1.5, {
                    delay: loaderWaiting + 0.2,
                    y: 30,
                    opacity: 0,
                    ease: Power3.easeInOut
                }, 'first')
                .from($lines_1.find('>*').eq(3), 0.22, {
                    delay: loaderWaiting + 0.6,
                    opacity: 0,
                    x: 10,
                    ease: Power1.linear
                }, 'first')
                .from($lines_1.find('>*').eq(2), 0.22, {
                    delay: loaderWaiting + 0.8,
                    opacity: 0,
                    x: 10,
                    ease: Power1.linear
                }, 'first')
                .from($lines_1.find('>*').eq(1), 0.22, {
                    delay: loaderWaiting + 1,
                    opacity: 0,
                    x: 10,
                    ease: Power1.linear
                }, 'first')
                .from($lines_1.find('>*').eq(0), 0.22, {
                    delay: loaderWaiting + 1.2,
                    opacity: 0,
                    x: 10,
                    ease: Power1.linear
                }, 'first')
                .from($lines_2.find('>*').eq(3), 0.22, {
                    delay: loaderWaiting + 0.6,
                    opacity: 0,
                    x: 10,
                    ease: Power1.linear
                }, 'first')
                .from($lines_2.find('>*').eq(2), 0.22, {
                    delay: loaderWaiting + 0.8,
                    opacity: 0,
                    x: 10,
                    ease: Power1.linear
                }, 'first')
                .from($lines_2.find('>*').eq(1), 0.22, {
                    delay: loaderWaiting + 1,
                    opacity: 0,
                    x: 10,
                    ease: Power1.linear
                }, 'first')
                .from($lines_2.find('>*').eq(0), 0.22, {
                    delay: loaderWaiting + 1.2,
                    opacity: 0,
                    x: 10,
                    ease: Power1.linear
                }, 'first')

                .from($sectionHeader, 1.5, {delay: loaderWaiting + 0.2, opacity: 0, ease: Power3.easeInOut}, 'first')
                .from($sectionPagination, 1.5, {
                    delay: loaderWaiting + 0.4,
                    opacity: 0,
                    ease: Power3.easeInOut
                }, 'first')
                .from($('section.type-1 .socials, section.type-1 .tip-arrow-outer'), 1.5, {
                    delay: loaderWaiting + 0.6,
                    opacity: 0,
                    ease: Power3.easeInOut
                }, 'first')
            // ANIM END
        }
    },
    sectionType_2: function () {
        if ($('section.type-2')[0]) {
            if ($('section.type-2').hasClass('var-a')) {
                // SLIDER
                var $totalSlides = $('section.type-2 .product .slides-counter .total');

                $totalSlides.text($('section.type-2 .product .inner .images img').length);

                var slider = new SerjiSlyder({
                    $controls: $('section.type-2 .product .inner .another-product, section.type-2 .product .side'),
                    $slides: $('section.type-2 .product .inner .images img'),
                    $actionField: $('section.type-2 .product'),
                    animFn: function (direction, $slides, $actionField, oldIndex, index) {
                        var $info = $('section.type-2 .product .info-outer .info'),
                            $prevImages = $('section.type-2 .product .side.left .images img'),
                            $nextImages = $('section.type-2 .product .side.right .images img'),
                            $prevTitles = $('section.type-2 .product .inner .another-product.prev .list div'),
                            $nextTitles = $('section.type-2 .product .inner .another-product.next .list div'),
                            $currentSlide = $('section.type-2 .product .slides-counter .current'),
                            animTime = 1.1;

                        $currentSlide.text(index + 1);

                        if (direction == 'right') {
                            TweenLite.fromTo($slides.eq(oldIndex), animTime, {opacity: 1, x: 0, scale: 1}, {
                                opacity: 0,
                                position: 'absolute',
                                display: 'none',
                                x: -150,
                                scale: 0.96,
                                ease: Power3.easeInOut
                            });
                            TweenLite.fromTo($slides.eq(index), animTime, {
                                opacity: 0,
                                position: 'absolute',
                                x: 150,
                                scale: 0.96
                            }, {
                                opacity: 1,
                                display: 'block',
                                position: 'static',
                                x: 0,
                                scale: 1,
                                ease: Power3.easeInOut
                            });

                            TweenLite.fromTo($info.eq(oldIndex), animTime, {opacity: 1, x: 0}, {
                                opacity: 0,
                                position: 'absolute',
                                display: 'none',
                                ease: Power2.easeInOut
                            });
                            TweenLite.fromTo($info.eq(index), animTime, {opacity: 0, position: 'absolute'}, {
                                opacity: 1,
                                display: 'flex',
                                position: 'static',
                                x: 0,
                                ease: Power2.easeInOut
                            });

                            TweenLite.fromTo($prevImages.eq(oldIndex), animTime, {opacity: 0.2, x: 0}, {
                                opacity: 0,
                                position: 'absolute',
                                display: 'none',
                                x: -300,
                                ease: Power2.easeInOut
                            });
                            TweenLite.fromTo($prevImages.eq(index), animTime, {
                                opacity: 0,
                                position: 'absolute',
                                x: 300
                            }, {opacity: 0.2, display: 'block', position: 'static', x: 0, ease: Power2.easeInOut});

                            TweenLite.fromTo($nextImages.eq(oldIndex), animTime, {opacity: 0.2, x: 0}, {
                                opacity: 0,
                                position: 'absolute',
                                display: 'none',
                                x: -300,
                                ease: Power2.easeInOut
                            });
                            TweenLite.fromTo($nextImages.eq(index), animTime, {
                                opacity: 0,
                                position: 'absolute',
                                x: 300
                            }, {opacity: 0.2, display: 'block', position: 'static', x: 0, ease: Power2.easeInOut});

                            TweenLite.fromTo($prevTitles.eq(oldIndex), 0.5, {
                                opacity: 1,
                                position: 'static',
                                y: 0
                            }, {opacity: 0, position: 'absolute', y: -50, ease: Power2.easeInOut});
                            TweenLite.fromTo($prevTitles.eq(index), 0.5, {
                                opacity: 0,
                                position: 'absolute',
                                y: 50
                            }, {opacity: 1, position: 'static', y: 0, ease: Power2.easeInOut});

                            TweenLite.fromTo($nextTitles.eq(oldIndex), 0.5, {
                                opacity: 1,
                                position: 'static',
                                y: 0
                            }, {opacity: 0, position: 'absolute', y: -50, ease: Power2.easeInOut});
                            TweenLite.fromTo($nextTitles.eq(index), 0.5, {
                                opacity: 0,
                                position: 'absolute',
                                y: 50
                            }, {opacity: 1, position: 'static', y: 0, ease: Power2.easeInOut});
                        } else {
                            TweenLite.fromTo($slides.eq(oldIndex), animTime, {opacity: 1, x: 0, scale: 1}, {
                                opacity: 0,
                                position: 'absolute',
                                display: 'none',
                                x: 150,
                                scale: 0.93,
                                ease: Power3.easeInOut
                            });
                            TweenLite.fromTo($slides.eq(index), animTime, {
                                opacity: 0,
                                position: 'absolute',
                                x: -150,
                                scale: 0.93
                            }, {
                                opacity: 1,
                                display: 'block',
                                position: 'static',
                                x: 0,
                                scale: 1,
                                ease: Power3.easeInOut
                            });

                            TweenLite.fromTo($info.eq(oldIndex), animTime, {opacity: 1, x: 0}, {
                                opacity: 0,
                                position: 'absolute',
                                display: 'none',
                                ease: Power2.easeInOut
                            });
                            TweenLite.fromTo($info.eq(index), animTime, {opacity: 0, position: 'absolute'}, {
                                opacity: 1,
                                display: 'flex',
                                position: 'static',
                                x: 0,
                                ease: Power2.easeInOut
                            });

                            TweenLite.fromTo($prevImages.eq(oldIndex), animTime, {opacity: 0.2, x: 0}, {
                                opacity: 0,
                                position: 'absolute',
                                display: 'none',
                                x: 300,
                                ease: Power2.easeInOut
                            });
                            TweenLite.fromTo($prevImages.eq(index), animTime, {
                                opacity: 0,
                                position: 'absolute',
                                x: -300
                            }, {opacity: 0.2, display: 'block', position: 'static', x: 0, ease: Power2.easeInOut});

                            TweenLite.fromTo($nextImages.eq(oldIndex), animTime, {opacity: 0.2, x: 0}, {
                                opacity: 0,
                                position: 'absolute',
                                display: 'none',
                                x: 300,
                                ease: Power2.easeInOut
                            });
                            TweenLite.fromTo($nextImages.eq(index), animTime, {
                                opacity: 0,
                                position: 'absolute',
                                x: -300
                            }, {opacity: 0.2, display: 'block', position: 'static', x: 0, ease: Power2.easeInOut});

                            TweenLite.fromTo($prevTitles.eq(oldIndex), 0.5, {
                                opacity: 1,
                                position: 'static',
                                y: 0
                            }, {opacity: 0, position: 'absolute', y: 50, ease: Power2.easeInOut});
                            TweenLite.fromTo($prevTitles.eq(index), 0.5, {
                                opacity: 0,
                                position: 'absolute',
                                y: -50
                            }, {opacity: 1, position: 'static', y: 0, ease: Power2.easeInOut});

                            TweenLite.fromTo($nextTitles.eq(oldIndex), 0.5, {
                                opacity: 1,
                                position: 'static',
                                y: 0
                            }, {opacity: 0, position: 'absolute', y: 50, ease: Power2.easeInOut});
                            TweenLite.fromTo($nextTitles.eq(index), 0.5, {
                                opacity: 0,
                                position: 'absolute',
                                y: -50
                            }, {opacity: 1, position: 'static', y: 0, ease: Power2.easeInOut});
                        }
                    }
                });
                // SLIDER END
            }

            // CALCULATIONS
            var $calcButton = $('section.type-2 .product .variants .counter .button');

            $calcButton.on('click', function () {
                var $varTotal = $(this).parent().find('.variant-total'),
                    varPrice = $(this).parents('.variant').find('.price').data('price'),
                    $totalPrice = $(this).parents('.info').find('.total .number span');

                if ($(this).hasClass('minus')) {
                    if ($varTotal.text() != 0) {
                        $varTotal.text(parseInt($varTotal.text()) - 1);

                        $totalPrice.text(parseInt($totalPrice.text()) - parseInt(varPrice));
                    }
                } else {
                    $varTotal.text(parseInt($varTotal.text()) + 1);

                    $totalPrice.text(parseInt($totalPrice.text()) + parseInt(varPrice));
                }
            });
            // CALCULATIONS END

            // ANIM
            var sectionType_2_TL_1 = new TimelineLite(),
                sectionType_2_TL_2 = new TimelineLite(),
                $lines_1 = $('section.type-2 .title.type-2 .lines').eq(0),
                $lines_2 = $('section.type-2 .title.type-2 .lines').eq(1),
                $sliderSides = $('section.type-2 .product .side, section.type-2 .product .inner .another-product'),
                $sliderCounter = $('section.type-2 .product .slides-counter'),
                $sliderImages = $('section.type-2 .product .inner .images'),
                $sliderInfos = $('section.type-2 .product .info-outer');

            sectionType_2_TL_1
                .from($('section.type-2 .el-2'), 1.1, {y: 25, opacity: 0, ease: Power3.easeInOut}, 'first')
                .from($('section.type-2 .el-3'), 1.1, {delay: 0.1, y: 35, opacity: 0, ease: Power3.easeInOut}, 'first')
                .from($lines_1.find('>*').eq(3), 0.22, {delay: 0.6, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(2), 0.22, {delay: 0.8, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(1), 0.22, {delay: 1, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(0), 0.22, {delay: 1.2, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(3), 0.22, {delay: 0.6, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(2), 0.22, {delay: 0.8, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(1), 0.22, {delay: 1, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(0), 0.22, {delay: 1.2, opacity: 0, x: 10, ease: Power1.linear}, 'first');
            sectionType_2_TL_1.pause();

            sectionType_2_TL_2
                .from($('section.type-2 .frame-1'), 1, {y: 25, opacity: 0, ease: Power3.easeInOut}, 'first')
                .from($('section.type-2 .frame-2'), 1, {
                    delay: 0.1,
                    y: -25,
                    opacity: 0,
                    ease: Power3.easeInOut
                }, 'first')
                .from($sliderImages, 1.5, {delay: 0.3, opacity: 0, y: 35, ease: Power3.easeInOut}, 'first')
                .from($sliderInfos, 1.5, {delay: 0.3, opacity: 0, y: 35, ease: Power3.easeInOut}, 'first')
                .from($sliderCounter, 1.5, {delay: 0.4, opacity: 0, y: 35, ease: Power3.easeInOut}, 'first')
                .from($sliderSides, 1.5, {delay: 0.5, opacity: 0, y: 35, ease: Power3.easeInOut}, 'first')
            sectionType_2_TL_2.pause();

            var sectionType_2_WP_1 = new Waypoint({
                element: $('section.type-2'),
                handler: function (direction) {
                    if (direction == 'down') {
                        sectionType_2_TL_1.play();
                    } else {
                        sectionType_2_TL_1.reverse();
                    }
                },
                offset: '80%'
            });

            var sectionType_2_WP_2 = new Waypoint({
                element: $('section.type-2'),
                handler: function (direction) {
                    if (direction == 'down') {
                        sectionType_2_TL_2.play();
                    } else {
                        sectionType_2_TL_2.reverse();
                    }
                },
                offset: '60%'
            });
            // ANIM END
        }
    },
    sectionType_3: function () {
        if ($('section.type-3')[0]) {
            var $sectionType_3 = $('section.type-3'),
                $sectionPagination = $('.section-pagination'),
                $sectionPaginationTitle = $('.section-pagination .section-title div'),
                $sectionPaginationItem = $('.section-pagination .pagination a');

            var sectionType_3_WP = new Waypoint({
                element: $sectionType_3,
                handler: function (direction) {
                    if (direction == 'down') {
                        $sectionPaginationItem.removeClass('active');
                        $sectionPaginationItem.eq(2).addClass('active');

                        TweenLite.fromTo($sectionPaginationTitle.eq(1), 0.5, {opacity: 1, y: 0}, {
                            opacity: 0,
                            y: 50,
                            ease: Power2.easeInOut
                        });
                        TweenLite.fromTo($sectionPaginationTitle.eq(2), 0.5, {opacity: 0, y: -50}, {
                            opacity: 1,
                            y: 0,
                            ease: Power2.easeInOut
                        });
                    } else {
                        $sectionPaginationItem.removeClass('active');
                        $sectionPaginationItem.eq(1).addClass('active');

                        TweenLite.fromTo($sectionPaginationTitle.eq(2), 0.5, {opacity: 1, y: 0}, {
                            opacity: 0,
                            y: -50,
                            ease: Power2.easeInOut
                        });
                        TweenLite.fromTo($sectionPaginationTitle.eq(1), 0.5, {opacity: 0, y: 50}, {
                            opacity: 1,
                            y: 0,
                            ease: Power2.easeInOut
                        });
                    }
                },
                offset: '50%'
            });

            // ANIM
            var sectionType_3_TL_1 = new TimelineLite(),
                sectionType_3_TL_2 = new TimelineLite(),
                sectionType_3_TL_3 = new TimelineLite(),
                $lines_1 = $('section.type-3 .title.type-2 .lines').eq(0),
                $lines_2 = $('section.type-3 .title.type-2 .lines').eq(1),
                $title_1 = $('section.type-3 .row.type-3 .inner');

            sectionType_3_TL_1
                .from($('section.type-3 .el-2'), 1.1, {y: 25, opacity: 0, ease: Power3.easeInOut}, 'first')
                .from($('section.type-3 .el-3'), 1.1, {delay: 0.1, y: 35, opacity: 0, ease: Power3.easeInOut}, 'first')
                .from($lines_1.find('>*').eq(3), 0.22, {delay: 0.6, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(2), 0.22, {delay: 0.8, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(1), 0.22, {delay: 1, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(0), 0.22, {delay: 1.2, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(3), 0.22, {delay: 0.6, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(2), 0.22, {delay: 0.8, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(1), 0.22, {delay: 1, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(0), 0.22, {delay: 1.2, opacity: 0, x: 10, ease: Power1.linear}, 'first');
            sectionType_3_TL_1.pause();

            sectionType_3_TL_2
                .from($('section.type-3 .row.type-2 .blocks.type-1 .block.type-1 img'), 1, {
                    opacity: 0,
                    ease: Power3.easeInOut
                }, 'first')
                .from($('section.type-3 .el-4'), 1, {delay: 0.1, y: 40, opacity: 0, ease: Power3.easeInOut}, 'first')
                .staggerFrom($('section.type-3 .row.type-2 .blocks.type-1 .block.type-2 ul li'), 0.77, {
                    delay: 0.2,
                    y: 40,
                    opacity: 0,
                    ease: Power1.easeInOut
                }, 0.05, 'first');
            sectionType_3_TL_2.pause();

            sectionType_3_TL_3
                .from($('section.type-3 .frame-1'), 1, {y: 20, opacity: 0, ease: Power3.easeInOut}, 'first')
                .from($('section.type-3 .frame-2'), 1, {
                    delay: 0.1,
                    y: -20,
                    opacity: 0,
                    ease: Power3.easeInOut
                }, 'first')
                .from($title_1, 1.5, {opacity: 0, ease: Power3.easeInOut}, 'first');
            sectionType_3_TL_3.pause();

            var sectionType_3_WP_1 = new Waypoint({
                element: $('section.type-3'),
                handler: function (direction) {
                    if (direction == 'down') {
                        sectionType_3_TL_1.play();
                    } else {
                        sectionType_3_TL_1.reverse();
                    }
                },
                offset: '70%'
            });

            var sectionType_3_WP_2 = new Waypoint({
                element: $('section.type-3'),
                handler: function (direction) {
                    if (direction == 'down') {
                        sectionType_3_TL_2.play();
                    } else {
                        sectionType_3_TL_2.reverse();
                    }
                },
                offset: '50%'
            });

            var sectionType_3_WP_3 = new Waypoint({
                element: $('section.type-3'),
                handler: function (direction) {
                    if (direction == 'down') {
                        sectionType_3_TL_3.play();
                    } else {
                        sectionType_3_TL_3.reverse();
                    }
                },
                offset: '0%'
            });
            // ANIM END
        }
    },
    sectionType_4: function () {
        if ($('section.type-4')[0]) {
            if ($('section.type-4')[0]) {
                // SLIDER
                var $totalSlides = $('section.type-4 .slides-counter .total');

                $totalSlides.text($('section.type-4 .review .inner .blocks.type-1 .block.type-2 .list.type-2 > *').length);

                var slider = new SerjiSlyder({
                    $controls: $('section.type-4 .review .inner .another-review'),
                    $slides: $('section.type-4 .review .inner .blocks.type-1 .block.type-2 .list.type-2 > *'),
                    $actionField: $('section.type-4 .review'),
                    animFn: function (direction, $slides, $actionField, oldIndex, index) {
                        var $info = $('section.type-4 .review .inner .blocks.type-1 .block.type-1 .list.type-1 > *'),
                            $prevTitles = $('section.type-4 .review .inner .another-review.prev .list > *'),
                            $nextTitles = $('section.type-4 .review .inner .another-review.next .list > *'),
                            $currentSlide = $('section.type-4 .review .slides-counter .current'),
                            animTime = 1.1;

                        $currentSlide.text(index + 1);

                        if (direction == 'right') {
                            TweenLite.fromTo($slides.eq(oldIndex), animTime, {opacity: 1, x: 0}, {
                                opacity: 0,
                                position: 'absolute',
                                display: 'none',
                                x: -50,
                                ease: Power3.easeInOut
                            });
                            TweenLite.fromTo($slides.eq(index), animTime, {
                                opacity: 0,
                                position: 'absolute',
                                x: 50
                            }, {opacity: 1, display: 'block', position: 'relative', x: 0, ease: Power3.easeInOut});

                            TweenLite.fromTo($info.eq(oldIndex), animTime, {opacity: 1, x: 0}, {
                                opacity: 0,
                                position: 'absolute',
                                display: 'none',
                                ease: Power2.easeInOut
                            });
                            TweenLite.fromTo($info.eq(index), animTime, {opacity: 0, position: 'absolute'}, {
                                opacity: 1,
                                display: 'block',
                                position: 'relative',
                                x: 0,
                                ease: Power2.easeInOut
                            });

                            TweenLite.fromTo($prevTitles.eq(oldIndex), 0.5, {
                                opacity: 1,
                                position: 'relative',
                                y: 0
                            }, {opacity: 0, position: 'absolute', y: -50, ease: Power2.easeInOut});
                            TweenLite.fromTo($prevTitles.eq(index), 0.5, {
                                opacity: 0,
                                position: 'absolute',
                                y: 50
                            }, {opacity: 1, position: 'relative', y: 0, ease: Power2.easeInOut});

                            TweenLite.fromTo($nextTitles.eq(oldIndex), 0.5, {
                                opacity: 1,
                                position: 'relative',
                                y: 0
                            }, {opacity: 0, position: 'absolute', y: -50, ease: Power2.easeInOut});
                            TweenLite.fromTo($nextTitles.eq(index), 0.5, {
                                opacity: 0,
                                position: 'absolute',
                                y: 50
                            }, {opacity: 1, position: 'relative', y: 0, ease: Power2.easeInOut});
                        } else {
                            TweenLite.fromTo($slides.eq(oldIndex), animTime, {opacity: 1, x: 0}, {
                                opacity: 0,
                                position: 'absolute',
                                display: 'none',
                                x: 50,
                                ease: Power3.easeInOut
                            });
                            TweenLite.fromTo($slides.eq(index), animTime, {
                                opacity: 0,
                                position: 'absolute',
                                x: -50
                            }, {opacity: 1, display: 'block', position: 'relative', x: 0, ease: Power3.easeInOut});

                            TweenLite.fromTo($info.eq(oldIndex), animTime, {opacity: 1, x: 0}, {
                                opacity: 0,
                                position: 'absolute',
                                display: 'none',
                                ease: Power2.easeInOut
                            });
                            TweenLite.fromTo($info.eq(index), animTime, {opacity: 0, position: 'absolute'}, {
                                opacity: 1,
                                display: 'block',
                                position: 'relative',
                                x: 0,
                                ease: Power2.easeInOut
                            });

                            TweenLite.fromTo($prevTitles.eq(oldIndex), 0.5, {
                                opacity: 1,
                                position: 'relative',
                                y: 0
                            }, {opacity: 0, position: 'absolute', y: 50, ease: Power2.easeInOut});
                            TweenLite.fromTo($prevTitles.eq(index), 0.5, {
                                opacity: 0,
                                position: 'absolute',
                                y: -50
                            }, {opacity: 1, position: 'relative', y: 0, ease: Power2.easeInOut});

                            TweenLite.fromTo($nextTitles.eq(oldIndex), 0.5, {
                                opacity: 1,
                                position: 'relative',
                                y: 0
                            }, {opacity: 0, position: 'absolute', y: 50, ease: Power2.easeInOut});
                            TweenLite.fromTo($nextTitles.eq(index), 0.5, {
                                opacity: 0,
                                position: 'absolute',
                                y: -50
                            }, {opacity: 1, position: 'relative', y: 0, ease: Power2.easeInOut});
                        }
                    }
                });
                // SLIDER END

                var $sectionType_4 = $('section.type-4'),
                    $sectionPagination = $('.section-pagination'),
                    $sectionPaginationTitle = $('.section-pagination .section-title div'),
                    $sectionPaginationItem = $('.section-pagination .pagination a');

                var sectionType_4_WP = new Waypoint({
                    element: $sectionType_4,
                    handler: function (direction) {
                        if (direction == 'down') {
                            $sectionPaginationItem.removeClass('active');
                            $sectionPaginationItem.eq(3).addClass('active');

                            TweenLite.fromTo($sectionPaginationTitle.eq(2), 0.5, {opacity: 1, y: 0}, {
                                opacity: 0,
                                y: 50,
                                ease: Power2.easeInOut
                            });
                            TweenLite.fromTo($sectionPaginationTitle.eq(3), 0.5, {opacity: 0, y: -50}, {
                                opacity: 1,
                                y: 0,
                                ease: Power2.easeInOut
                            });
                        } else {
                            $sectionPaginationItem.removeClass('active');
                            $sectionPaginationItem.eq(2).addClass('active');

                            TweenLite.fromTo($sectionPaginationTitle.eq(3), 0.5, {opacity: 1, y: 0}, {
                                opacity: 0,
                                y: -50,
                                ease: Power2.easeInOut
                            });
                            TweenLite.fromTo($sectionPaginationTitle.eq(2), 0.5, {opacity: 0, y: 50}, {
                                opacity: 1,
                                y: 0,
                                ease: Power2.easeInOut
                            });
                        }
                    },
                    offset: '50%'
                });

                // ANIM
                var sectionType_4_TL_1 = new TimelineLite(),
                    sectionType_4_TL_2 = new TimelineLite(),
                    $lines_1 = $('section.type-4 .title.type-2 .lines').eq(0),
                    $lines_2 = $('section.type-4 .title.type-2 .lines').eq(1),
                    $blocks = $('section.type-4 .review .inner .blocks.type-1 .block'),
                    $controls = $('section.type-4 .review .inner .another-review');

                sectionType_4_TL_1
                    .from($('section.type-4 .el-2'), 1.1, {y: 25, opacity: 0, ease: Power3.easeInOut}, 'first')
                    .from($('section.type-4 .el-3'), 1.1, {
                        delay: 0.1,
                        y: 35,
                        opacity: 0,
                        ease: Power3.easeInOut
                    }, 'first')
                    .from($lines_1.find('>*').eq(3), 0.22, {
                        delay: 0.6,
                        opacity: 0,
                        x: 10,
                        ease: Power1.linear
                    }, 'first')
                    .from($lines_1.find('>*').eq(2), 0.22, {
                        delay: 0.8,
                        opacity: 0,
                        x: 10,
                        ease: Power1.linear
                    }, 'first')
                    .from($lines_1.find('>*').eq(1), 0.22, {delay: 1, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                    .from($lines_1.find('>*').eq(0), 0.22, {
                        delay: 1.2,
                        opacity: 0,
                        x: 10,
                        ease: Power1.linear
                    }, 'first')

                    .from($lines_2.find('>*').eq(3), 0.22, {
                        delay: 0.6,
                        opacity: 0,
                        x: 10,
                        ease: Power1.linear
                    }, 'first')
                    .from($lines_2.find('>*').eq(2), 0.22, {
                        delay: 0.8,
                        opacity: 0,
                        x: 10,
                        ease: Power1.linear
                    }, 'first')
                    .from($lines_2.find('>*').eq(1), 0.22, {delay: 1, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                    .from($lines_2.find('>*').eq(0), 0.22, {
                        delay: 1.2,
                        opacity: 0,
                        x: 10,
                        ease: Power1.linear
                    }, 'first');
                sectionType_4_TL_1.pause();

                sectionType_4_TL_2
                    .from($('section.type-4 .frame-1'), 1, {y: 25, opacity: 0, ease: Power3.easeInOut}, 'first')
                    .from($('section.type-4 .frame-2'), 1, {
                        delay: 0.1,
                        y: -25,
                        opacity: 0,
                        ease: Power3.easeInOut
                    }, 'first')

                    .from($blocks, 1.5, {delay: 0.2, opacity: 0, y: 35, ease: Power3.easeInOut}, 'first')
                    .from($controls, 1.5, {delay: 0.3, opacity: 0, y: 35, ease: Power3.easeInOut}, 'first');
                sectionType_4_TL_2.pause();

                var sectionType_4_WP_1 = new Waypoint({
                    element: $('section.type-4'),
                    handler: function (direction) {
                        if (direction == 'down') {
                            sectionType_4_TL_1.play();
                        } else {
                            sectionType_4_TL_1.reverse();
                        }
                    },
                    offset: '90%'
                });

                var sectionType_4_WP_2 = new Waypoint({
                    element: $('section.type-4'),
                    handler: function (direction) {
                        if (direction == 'down') {
                            sectionType_4_TL_2.play();
                        } else {
                            sectionType_4_TL_2.reverse();
                        }
                    },
                    offset: '80%'
                });
                // ANIM END
            }
        }
    },
    sectionType_5: function () {
        if ($('section.type-5')[0]) {
            function googleMapInit() {
                var map;

                if ($('#map')[0]) {
                    function center_map_by_marker(map, obj) {
                        var bounds = new google.maps.LatLngBounds();

                        $.each(map.markers, function (i, marker) {
                            marker.setIcon(mapMarkerUrl);
                        });

                        $.each(map.markers, function (i, marker) {
                            if (parseFloat(marker.position.lat().toFixed(7)) == parseFloat(obj.attr('data-lat')).toFixed(7) && parseFloat(marker.position.lng().toFixed(7)) == parseFloat(obj.attr('data-lng')).toFixed(7)) {
                                var latlng = new google.maps.LatLng(parseFloat(marker.position.lat().toFixed(7)), parseFloat(marker.position.lng().toFixed(7)));

                                bounds.extend(latlng);
                                map.setCenter(bounds.getCenter());
                                marker.setIcon(mapMarkerActiveUrl);

                                infowindow.close();
                                infowindow.setContent(infoWindowTextArray[i]);
                                infowindow.open(map,marker);

                                return;
                            }
                        });
                    }

                    $('section.type-5 .blocks.type-1 .elements.type-2 .element .link').on('click', function () {
                        center_map_by_marker(map, $(this));
                    });

                    googleMapInited = true;

                    var $map = $('#map'),
                        $markers = $map.find('.marker');

                    var args = {
                        zoom: 16,
                        center: new google.maps.LatLng(0, 0),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        scrollwheel: false,
                        styles: [{
                            "featureType": "all",
                            "elementType": "geometry.fill",
                            "stylers": [{"weight": "2.00"}]
                        }, {
                            "featureType": "all",
                            "elementType": "geometry.stroke",
                            "stylers": [{"color": "#9c9c9c"}]
                        }, {
                            "featureType": "all",
                            "elementType": "labels.text",
                            "stylers": [{"visibility": "on"}]
                        }, {
                            "featureType": "landscape",
                            "elementType": "all",
                            "stylers": [{"color": "#f2f2f2"}]
                        }, {
                            "featureType": "landscape",
                            "elementType": "geometry.fill",
                            "stylers": [{"color": "#ffffff"}]
                        }, {
                            "featureType": "landscape.man_made",
                            "elementType": "geometry.fill",
                            "stylers": [{"color": "#ffffff"}]
                        }, {
                            "featureType": "poi",
                            "elementType": "all",
                            "stylers": [{"visibility": "off"}]
                        }, {
                            "featureType": "road",
                            "elementType": "all",
                            "stylers": [{"saturation": -100}, {"lightness": 45}]
                        }, {
                            "featureType": "road",
                            "elementType": "geometry.fill",
                            "stylers": [{"color": "#eeeeee"}]
                        }, {
                            "featureType": "road",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#7b7b7b"}]
                        }, {
                            "featureType": "road",
                            "elementType": "labels.text.stroke",
                            "stylers": [{"color": "#ffffff"}]
                        }, {
                            "featureType": "road.highway",
                            "elementType": "all",
                            "stylers": [{"visibility": "simplified"}]
                        }, {
                            "featureType": "road.arterial",
                            "elementType": "labels.icon",
                            "stylers": [{"visibility": "off"}]
                        }, {
                            "featureType": "transit",
                            "elementType": "all",
                            "stylers": [{"visibility": "off"}]
                        }, {
                            "featureType": "water",
                            "elementType": "all",
                            "stylers": [{"color": "#46bcec"}, {"visibility": "on"}]
                        }, {
                            "featureType": "water",
                            "elementType": "geometry.fill",
                            "stylers": [{"color": "#c8d7d4"}]
                        }, {
                            "featureType": "water",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#070707"}]
                        }, {
                            "featureType": "water",
                            "elementType": "labels.text.stroke",
                            "stylers": [{"color": "#ffffff"}]
                        }]
                    };

                    map = new google.maps.Map($map[0], args);

                    map.markers = [];

                    // INFO WINDOW INIT
                    infoWindowTextArray = [];

                    $('section.type-5 .blocks.type-1 .elements.type-2 .element').each(function(){
                        var infoWindowContent = '';

                        infoWindowContent += $(this).find('.el-4').text() + '<br>' + $(this).find('.el-5').text();

                        infoWindowTextArray.push(infoWindowContent);
                    });

                    var infowindow = new google.maps.InfoWindow({
                        content: ''
                    });
                    // INFO WINDOW INIT END

                    $markers.each(function () {
                        if ($(this).data('lat') != '' || $(this).data('lng') != '') {
                            add_marker($(this), map);
                        }
                    });

                    center_map(map);

                    return map;

                    function add_marker($marker, map) {
                        var latlng = new google.maps.LatLng($marker.data('lat'), $marker.data('lng'));

                        var image = mapMarkerUrl;
                        var marker = new google.maps.Marker({
                            position: latlng,
                            map: map,
                            icon: image
                        });

                        marker.addListener('click', function() {
                            var self = this,
                                currentIndex;

                            $.each(map.markers, function (i, marker) {
                                marker.setIcon(mapMarkerUrl);

                                if (self == marker) {
                                    currentIndex = i;
                                }
                            });

                            marker.setIcon(mapMarkerActiveUrl);
                            map.setCenter(marker.getPosition());

                            infowindow.close();
                            console.log(this);
                            infowindow.setContent(infoWindowTextArray[currentIndex]);
                            infowindow.open(map,marker);
                        });

                        map.markers.push(marker);
                    }

                    function center_map(map) {
                        var bounds = new google.maps.LatLngBounds();
                        var $CoorA = [];

                        $markers.each(function () {
                            $CoorA.push($(this));
                        });

                        $.each(map.markers, function (i, marker) {
                            var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());

                            $CoorA[i].data('lat', marker.position.lat());
                            $CoorA[i].data('lng', marker.position.lng());

                            bounds.extend(latlng);
                        });

                        if (map.markers.length == 1) {
                            map.setCenter(bounds.getCenter());
                            map.setZoom(16);
                        } else {
                            map.fitBounds(bounds);
                        }
                    }
                }
            }

            if ($('.section-pagination')[0]) {
                var $sectionType_5 = $('section.type-5'),
                    $sectionPagination = $('.section-pagination'),
                    $sectionPaginationTitle = $('.section-pagination .section-title div'),
                    $sectionPaginationItem = $('.section-pagination .pagination a'),
                    googleMapInited = false;

                var sectionType_5_WP = new Waypoint({
                    element: $sectionType_5,
                    handler: function (direction) {
                        if (direction == 'down') {
                            $sectionPaginationItem.removeClass('active');
                            $sectionPaginationItem.eq(4).addClass('active');

                            TweenLite.fromTo($sectionPaginationTitle.eq(3), 0.5, {opacity: 1, y: 0}, {
                                opacity: 0,
                                y: 50,
                                ease: Power2.easeInOut
                            });
                            TweenLite.fromTo($sectionPaginationTitle.eq(4), 0.5, {opacity: 0, y: -50}, {
                                opacity: 1,
                                y: 0,
                                ease: Power2.easeInOut
                            });

                            if (googleMapInited != true) {
                                googleMapInit();
                            }

                            googleMapInited = true;


                        } else {
                            $sectionPaginationItem.removeClass('active');
                            $sectionPaginationItem.eq(3).addClass('active');

                            TweenLite.fromTo($sectionPaginationTitle.eq(4), 0.5, {opacity: 1, y: 0}, {
                                opacity: 0,
                                y: -50,
                                ease: Power2.easeInOut
                            });
                            TweenLite.fromTo($sectionPaginationTitle.eq(3), 0.5, {opacity: 0, y: 50}, {
                                opacity: 1,
                                y: 0,
                                ease: Power2.easeInOut
                            });
                        }
                    },
                    offset: '50%'
                });
            } else {
                googleMapInit();
            }

            // ANIM
            var sectionType_5_TL_1 = new TimelineLite(),
                $lines_1 = $('section.type-5 .title.type-2 .lines').eq(0),
                $lines_2 = $('section.type-5 .title.type-2 .lines').eq(1);

            sectionType_5_TL_1
                .from($('section.type-5 .el-2'), 1.1, {y: 25, opacity: 0, ease: Power3.easeInOut}, 'first')
                .from($('section.type-5 .el-3'), 1.1, {delay: 0.1, y: 35, opacity: 0, ease: Power3.easeInOut}, 'first')
                .from($lines_1.find('>*').eq(3), 0.22, {delay: 0.6, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(2), 0.22, {delay: 0.8, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(1), 0.22, {delay: 1, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(0), 0.22, {delay: 1.2, opacity: 0, x: 10, ease: Power1.linear}, 'first')

                .from($lines_2.find('>*').eq(3), 0.22, {delay: 0.6, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(2), 0.22, {delay: 0.8, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(1), 0.22, {delay: 1, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(0), 0.22, {delay: 1.2, opacity: 0, x: 10, ease: Power1.linear}, 'first');
            sectionType_5_TL_1.pause();

            var sectionType_5_WP_1 = new Waypoint({
                element: $('section.type-5'),
                handler: function (direction) {
                    if (direction == 'down') {
                        sectionType_5_TL_1.play();
                    } else {
                        sectionType_5_TL_1.reverse();
                    }
                },
                offset: '90%'
            });
            // ANIM END

            // ADAPTIVE SCROLL TO
            $showOnMapButton = $('section.type-5 .blocks.type-1 .elements.type-2 .element .link');

            $showOnMapButton.on('click', function(){
                if ($(window).width() < 768) {
                    $('html, body').stop().animate({
                        scrollTop: $('section.type-5 .blocks.type-1 .block.type-2').offset().top - $('header').height() - 15
                    }, 500);
                }
            });
            // ADAPTIVE SCROLL TO END
        }
    },
    sectionType_6: function () {
        if ($('section.type-6')[0]) {
            // ANIM
            var sectionType_6_TL = new TimelineLite(),
                $lines_1 = $('section.type-6 .title.type-2 .lines').eq(0),
                $lines_2 = $('section.type-6 .title.type-2 .lines').eq(1);

            sectionType_6_TL
                .from($('section.type-6 .el-2'), 1.1, {y: 25, opacity: 0, ease: Power3.easeInOut}, 'first')
                .from($('section.type-6 .el-3'), 1.1, {delay: 0.1, y: 35, opacity: 0, ease: Power3.easeInOut}, 'first')
                .from($lines_1.find('>*').eq(3), 0.22, {delay: 0.6, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(2), 0.22, {delay: 0.8, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(1), 0.22, {delay: 1, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(0), 0.22, {delay: 1.2, opacity: 0, x: 10, ease: Power1.linear}, 'first')

                .from($lines_2.find('>*').eq(3), 0.22, {delay: 0.6, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(2), 0.22, {delay: 0.8, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(1), 0.22, {delay: 1, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(0), 0.22, {delay: 1.2, opacity: 0, x: 10, ease: Power1.linear}, 'first');
            sectionType_6_TL.pause();

            setTimeout(function () {
                sectionType_6_TL.play();
            }, loaderWaiting * 1000);
            // ANIM END

            // SCROLL OLD
            // var $scrollbarOuter = $('section.type-6 .blocks.type-1 .block.type-2'),
            //     $categoryButton = $('section.type-6 .blocks.type-1 .block.type-1 .list.type-1 .item'),
            //     $category = $('section.type-6 .blocks.type-1 .block.type-2 .divider'),
            //     activeCategory = $('section.type-6 .blocks.type-1 .block.type-1 .item').data('category');

            // setTimeout(function () {
            //     $scrollbarOuter.mCustomScrollbar({
            //         callbacks: {
            //             whileScrolling: function () {
            //                 myCustomFn(this);
            //             }
            //         }
            //     });

            //     function myCustomFn(el) {
            //         var top = el.mcs.top;

            //         sessionStorage.setItem('catalogScrollTop', top);

            //         $category.each(function () {
            //             if (-$(this).position().top >= top - 100) {
            //                 if (activeCategory != $(this).data('category')) {
            //                     activeCategory = $(this).data('category');

            //                     $('section.type-6 .blocks.type-1 .block.type-1 .item[data-category="' + $(this).data('category') + '"]').addClass('active');
            //                     $('section.type-6 .blocks.type-1 .block.type-1 .item[data-category="' + $(this).data('category') + '"]').prevAll().removeClass('active');
            //                     $('section.type-6 .blocks.type-1 .block.type-1 .item[data-category="' + $(this).data('category') + '"]').nextAll().removeClass('active');
            //                 }
            //             }
            //         });
            //     }

            //     $categoryButton.on('click', function () {
            //         var selectedCategory = $(this).data('category'),
            //             $currentCategory = $('section.type-6 .blocks.type-1 .block.type-2 .divider[data-category="' + selectedCategory + '"]');

            //         $scrollbarOuter.mCustomScrollbar('scrollTo', $currentCategory);
            //     });
            // }, 1000);
            // SCROLL OLD END

            // CALCULATIONS
            var $calcButton = $('section.type-6 .blocks.type-1 .block.type-2 .item.type-2 .counter .button');

            $calcButton.on('click', function () {
                var $varTotal = $(this).parent().find('.variant-total'),
                    $totalPrice = $(this).parents('.item').find('.price span'),
                    varPrice = $(this).parents('.item').find('.price').data('price');

                if ($(this).hasClass('minus')) {
                    if ($varTotal.text() != 0) {
                        $varTotal.text(parseInt($varTotal.text()) - 1);

                        if ($varTotal.text() >= 1) {
                            $totalPrice.text(parseInt($totalPrice.text()) - parseInt(varPrice));
                        }
                    }
                } else {
                    $varTotal.text(parseInt($varTotal.text()) + 1);

                    if ($varTotal.text() > 1) {
                        $totalPrice.text(parseInt($totalPrice.text()) + parseInt(varPrice));
                    }
                }
            });
            // CALCULATIONS END

            if($('section.type-6.var-a')[0]){
                // ADAPTIVE CATEGORIES
                var $adaptiveCategoriesTitle = $('section.type-6 .category-title-adaptive'),
                    $categoriesList = $('section.type-6 .blocks.type-1 .block.type-1 .list.type-1'),
                    $categoriesItem = $('section.type-6 .blocks.type-1 .block.type-1 .list.type-1 .item');

                $adaptiveCategoriesTitle.on('click', function(){
                    if (!$adaptiveCategoriesTitle.hasClass('active')) {
                        $categoriesList.show();
                    } else {
                        $categoriesList.hide();
                    }

                    $adaptiveCategoriesTitle.toggleClass('active');
                });

                $categoriesItem.on('click', function(){
                    if (!$adaptiveCategoriesTitle.hasClass('active')) {
                        $categoriesList.show();
                    } else {
                        $categoriesList.hide();
                    }

                    $adaptiveCategoriesTitle.toggleClass('active');
                });
                // ADAPTIVE CATEGORIES END

                // SCROLL NEW
                new Waypoint({
                    element: $('section.type-6 .blocks.type-1'),
                    handler: function (direction) {
                        if (direction == 'down') {
                            $('section.type-6 .blocks.type-1 .block.type-1').addClass('fixed');
                            $('section.type-6 .blocks.type-1 .block.type-1').css('top', $('header').height() + 'px');
                        } else {
                            $('section.type-6 .blocks.type-1 .block.type-1').removeClass('fixed');
                            $('section.type-6 .blocks.type-1 .block.type-1').css('top', 0 + 'px');
                        }
                    },
                    offset: function(){
                        return $('header').height();
                    }
                });
    
                var $categoryButton = $('section.type-6 .blocks.type-1 .block.type-1 .list.type-1 .item'),
                    $category = $('section.type-6 .blocks.type-1 .block.type-2 .divider'),
                    activeCategory = $('section.type-6 .blocks.type-1 .block.type-1 .item').data('category');
    
                $(window).scroll(function(){
                    var top = $(window).scrollTop();
    
                    sessionStorage.setItem('catalogScrollTop', top);
    
                    $category.each(function(){
                        if($(this).offset().top <= top + $('header').height() + ($(window).height()/2)/1.5) {
                            if (activeCategory != $(this).data('category')) {
                                activeCategory = $(this).data('category');
    
                                $('section.type-6 .blocks.type-1 .block.type-1 .item[data-category="' + $(this).data('category') + '"]').addClass('active');
                                $('section.type-6 .blocks.type-1 .block.type-1 .item[data-category="' + $(this).data('category') + '"]').prevAll().removeClass('active');
                                $('section.type-6 .blocks.type-1 .block.type-1 .item[data-category="' + $(this).data('category') + '"]').nextAll().removeClass('active');
                            }
                        }
                    });
    
                    var blockType_1_scrollTop = $('section.type-6 .blocks.type-1 .block.type-1').outerHeight() + $('section.type-6 .blocks.type-1 .block.type-1').offset().top - $('header').height(),
                        blockType_2_scrollTop = $('section.type-6 .blocks.type-1 .block.type-2').height() + $('section.type-6 .blocks.type-1 .block.type-2').offset().top;
    
                    // console.log(blockType_1_scrollTop, blockType_2_scrollTop);
                    // console.log(top + $(window).height(), blockType_2_scrollTop);
    
                    if(blockType_1_scrollTop >= blockType_2_scrollTop - $('header').height()){
                        $('section.type-6 .blocks.type-1 .block.type-1').removeClass('fixed');
                        $('section.type-6 .blocks.type-1 .block.type-1').addClass('fixed-stuck');
                        $('section.type-6 .blocks.type-1 .block.type-1').css('top', ($('section.type-6 .blocks.type-1 .block.type-2').height() - $('section.type-6 .blocks.type-1 .block.type-1').outerHeight()) + 'px');
                    }
    
                    if(top <= $('section.type-6 .blocks.type-1 .block.type-1').offset().top - $('header').height()){
                        if($('section.type-6 .blocks.type-1 .block.type-1').hasClass('fixed-stuck')){
                            $('section.type-6 .blocks.type-1 .block.type-1').addClass('fixed');
                            $('section.type-6 .blocks.type-1 .block.type-1').removeClass('fixed-stuck');
                            $('section.type-6 .blocks.type-1 .block.type-1').css('top', $('header').height() + 'px');
                        }
                    }
                });
    
                $categoryButton.on('click', function () {
                    var selectedCategory = $(this).data('category'),
                        $currentCategory = $('section.type-6 .blocks.type-1 .block.type-2 .divider[data-category="' + selectedCategory + '"]'),
                        categoryBlockHeight = $(window).width() < 768 ? $('section.type-6 .blocks.type-1 .block.type-1').outerHeight(true) : 0;

                    $('html, body').stop().animate({
                        scrollTop: $currentCategory.offset().top - $('header').height() - 1 - categoryBlockHeight
                    }, 500);
                });
                // SCROLL NEW END

                // BACK TO CONFECTIONERY
                var href = window.location.href;

                if(href.indexOf('#catalog') != -1) { 
                    var catalogScrollTop = 0;

                    if(sessionStorage.getItem('catalogScrollTop') != ''){
                        catalogScrollTop = sessionStorage.getItem('catalogScrollTop');

                        $('html, body').animate({
                            scrollTop: catalogScrollTop
                        }, 0);
                    }
                }
                // BACK TO CONFECTIONERY END
            }

            if($('section.type-6.var-b')[0]){
                $(window).scroll(function(){
                    var top = $(window).scrollTop();
                
                    sessionStorage.setItem('souvenirsScrollTop', top);
                });

                var href = window.location.href;

                if(href.indexOf('#catalog') != -1) { 
                    var souvenirsScrollTop = 0;

                    if(sessionStorage.getItem('souvenirsScrollTop') != ''){
                        souvenirsScrollTop = sessionStorage.getItem('souvenirsScrollTop');

                        $('html, body').animate({
                            scrollTop: souvenirsScrollTop
                        }, 0);
                    }
                }
            }
        }
    },
    sectionType_7: function () {
        if ($('section.type-7')[0]) {
            var deliveryText = $('section.type-7 .blocks.type-1 .block.type-2 .delivery-text');
            $('#checkout-form').find('input[type=radio]').on('change', function () {
                var input = $(this);
                var deliveryPrice = parseFloat(input.data('price'));
                $('.for-radio').hide();
                var selector = '.for-' + input.attr('id').replace('_', '-');
                $(selector).show();
                var totalCost = $('.total-cost-text');
                var regularCost = parseFloat($('.cost-text').text());
                if (deliveryPrice) {
                    deliveryText.find('.delivery-cost').text(deliveryPrice);
                    deliveryText.show();
                    totalCost.text(regularCost + deliveryPrice);
                } else {
                    deliveryText.hide();
                    totalCost.text(regularCost);
                }
            });
        }
    },
    sectionType_8: function () {
        if ($('section.type-8')[0]) {
            var $stars = $('section.type-8 .blocks.type-1 .stars > .star-outer');

            $stars.hover(function () {
                $(this).addClass('hovered');
                $(this).prevAll().addClass('hovered');
            }, function () {
                $stars.removeClass('hovered');
            });

            $stars.on('click', function () {
                $(this).addClass('active');
                $(this).prevAll().addClass('active');
                $(this).nextAll().removeClass('active');

                $('#reviews-rating').val($(this).index() + 1);

                TweenLite.fromTo($(this).find('i').eq(1), 0.5, {opacity: 1, scale: 0}, {
                    opacity: 0,
                    scale: 2.5,
                    ease: Power1.linear
                });
            });
        }
    },
    sectionType_9: function () {
        if ($('section.type-9')[0]) {
            // ANIM
            var sectionType_9_TL = new TimelineLite(),
                $lines_1 = $('section.type-9 .title.type-2 .lines').eq(0),
                $lines_2 = $('section.type-9 .title.type-2 .lines').eq(1),
                $items = $('section.type-9 .blocks.type-1 .block.type-1');

            sectionType_9_TL
                .from($('section.type-9 .el-2'), 1.1, {y: 25, opacity: 0, ease: Power3.easeInOut}, 'first')
                .from($('section.type-9 .el-3'), 1.1, {delay: 0.1, y: 35, opacity: 0, ease: Power3.easeInOut}, 'first')
                .from($lines_1.find('>*').eq(3), 0.22, {delay: 0.6, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(2), 0.22, {delay: 0.8, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(1), 0.22, {delay: 1, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(0), 0.22, {delay: 1.2, opacity: 0, x: 10, ease: Power1.linear}, 'first')

                .from($lines_2.find('>*').eq(3), 0.22, {delay: 0.6, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(2), 0.22, {delay: 0.8, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(1), 0.22, {delay: 1, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(0), 0.22, {delay: 1.2, opacity: 0, x: 10, ease: Power1.linear}, 'first')

                .staggerFrom($items, 1.1, {delay: 0.3, opacity: 0, y: 66, ease: Power3.easeInOut}, 0.1, 'first');
            sectionType_9_TL.pause();

            setTimeout(function () {
                sectionType_9_TL.play();
            }, loaderWaiting * 1000);
            // ANIM END

            $loadMore = $('section.type-9 .el-6');

            $loadMore.on('click', function (e) {
                e.preventDefault();

                var $newItems = $('section.type-9 .blocks.type-1 .block.type-1.new'); //Class "new" is important

                TweenMax.staggerFrom($newItems, 1.1, {delay: 0.3, opacity: 0, y: 66, ease: Power3.easeInOut}, 0.1);

                $newItems.removeClass('new');
            });
        }
    },
    sectionType_10: function () {
        if ($('section.type-10')[0]) {
            // ANIM
            var sectionType_10_TL = new TimelineLite(),
                $lines_1 = $('section.type-10 .title.type-2 .lines').eq(0),
                $lines_2 = $('section.type-10 .title.type-2 .lines').eq(1),
                $sectionType_10_Row_1 = $('section.type-10 .row.type-1'),
                $sectionType_10_Row_3 = $('section.type-10 .row.type-3');

            sectionType_10_TL
                .from($sectionType_10_Row_1, 1.1, {y: 25, opacity: 0, ease: Power3.easeInOut}, 'first')
                .from($('section.type-10 .el-2'), 1.1, {y: 25, opacity: 0, ease: Power3.easeInOut}, 'first')
                .from($('section.type-10 .el-3'), 1.1, {delay: 0.1, y: 35, opacity: 0, ease: Power3.easeInOut}, 'first')
                .from($lines_1.find('>*').eq(3), 0.22, {delay: 0.6, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(2), 0.22, {delay: 0.8, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(1), 0.22, {delay: 1, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_1.find('>*').eq(0), 0.22, {delay: 1.2, opacity: 0, x: 10, ease: Power1.linear}, 'first')

                .from($lines_2.find('>*').eq(3), 0.22, {delay: 0.6, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(2), 0.22, {delay: 0.8, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(1), 0.22, {delay: 1, opacity: 0, x: 10, ease: Power1.linear}, 'first')
                .from($lines_2.find('>*').eq(0), 0.22, {delay: 1.2, opacity: 0, x: 10, ease: Power1.linear}, 'first')

                .from($sectionType_10_Row_3, 1.1, {delay: 0.3, opacity: 0, y: 66, ease: Power3.easeInOut}, 'first');
            sectionType_10_TL.pause();

            setTimeout(function () {
                sectionType_10_TL.play();
            }, loaderWaiting * 1000);
            // ANIM END

            var sliderTypeClicked;

            // SLIDER TYPE 2
            var $sliderType_2_Left = $('section.type-10 .row.type-3 .slider.type-2 .controls > *.left'),
                $sliderType_2_Right = $('section.type-10 .row.type-3 .slider.type-2 .controls > *.right');

            var sliderType_2 = $('section.type-10 .row.type-3 .slider.type-2 ul').lightSlider({
                item: 10,
                loop: false,
                slideMove: 1,
                speed: 600,
                controls: false,
                pager: false,
                responsive: [
                    {
                        breakpoint: 1366,
                        settings: {
                            item: 8,
                        }
                    },
                    {
                        breakpoint: 1024,
                        settings: {
                            item: 4,
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            item: 2,
                        }
                    }
                ]
            });

            $sliderType_2_Left.on('click', function () {
                sliderType_2.goToPrevSlide()
            });

            $sliderType_2_Right.on('click', function () {
                sliderType_2.goToNextSlide()
            });

            var $sliderType_2_Slide = $('section.type-10 .row.type-3 .slider.type-2 ul li');

            $sliderType_2_Slide.on('click', function () {
                var slideIndex = parseInt($(this).index());

                slider.settings.oldIndex = slider.settings.index;
                slider.settings.index = slideIndex;

                slider.settings.animFn('right', slider.settings.$slides, slider.settings.$actionField, slider.settings.oldIndex, slider.settings.index);

                sliderType_2.goToSlide(slideIndex);
            });
            // SLIDER TYPE 2 END

            // SLIDER TYPE 1
            var slider = new SerjiSlyder({
                $controls: $('section.type-10 .row.type-3 .slider.type-1 .controls > *'),
                $slides: $('section.type-10 .row.type-3 .slider.type-1 .images .image'),
                $actionField: $('section.type-10 .row.type-3 .slider.type-1 .images .image'),
                animFn: function (direction, $slides, $actionField, oldIndex, index) {
                    var slideWidth = $slides.width(),
                        animTime = 1;

                    sliderType_2.goToSlide(index + 1);

                    if (direction == 'right') {
                        TweenLite.fromTo($slides.eq(oldIndex), 1, {}, {
                            display: 'none',
                            position: 'absolute',
                            x: -slideWidth,
                            ease: Power3.easeInOut
                        });
                        TweenLite.fromTo($slides.eq(index), 1, {x: slideWidth}, {
                            display: 'block',
                            position: 'relative',
                            x: 0,
                            ease: Power3.easeInOut
                        });
                    } else {
                        TweenLite.fromTo($slides.eq(oldIndex), 1, {}, {
                            display: 'none',
                            position: 'absolute',
                            x: slideWidth,
                            ease: Power3.easeInOut
                        });
                        TweenLite.fromTo($slides.eq(index), 1, {x: -slideWidth}, {
                            display: 'block',
                            position: 'relative',
                            x: 0,
                            ease: Power3.easeInOut
                        });
                    }
                }
            });
            // SLIDER TYPE 1 END
        }
    },
    showReviewPopup: function () {
        TweenLite.to($('#review-popup'), 1, {
            opacity: 1,
            visibility: 'visible',
            ease: Power3.easeInOut
        });

        setTimeout(function () {
            TweenLite.to($('#review-popup'), 1, {
                opacity: 0,
                display: 'none',
                ease: Power3.easeInOut
            });
        }, 4000);
    },
    showDevelopPopup: function () {
        TweenLite.to($('#develop-popup'), 1, {
            opacity: 1,
            visibility: 'visible',
            ease: Power3.easeInOut
        });

        setTimeout(function () {
            TweenLite.to($('#develop-popup'), 1, {
                opacity: 0,
                display: 'none',
                ease: Power3.easeInOut
            });
        }, 5000);
    },
    showCheckoutPopup: function () {
        TweenLite.to($('#checkout-popup'), 1, {
            opacity: 1,
            visibility: 'visible',
            ease: Power3.easeInOut
        });

        setTimeout(function () {
            TweenLite.to($('#checkout-popup'), 1, {
                opacity: 0,
                display: 'none',
                ease: Power3.easeInOut
            });
        }, 4000);
    },
    showHomePopup: function () {
        TweenLite.to($('#home-popup'), 1, {
            opacity: 1,
            visibility: 'visible',
            ease: Power3.easeInOut
        });

        setTimeout(function () {
            TweenLite.to($('#home-popup'), 1, {
                opacity: 0,
                display: 'none',
                ease: Power3.easeInOut
            });
        }, 4000);
    },
    applyDiscountStyles: function () {
        var discountCounter = sessionStorage.getItem('discountCounter'),
            $discount = $('.discount'),
            $discountNumbers = $('.discount > span.text, .basket-menu .total');

        if (discountCounter == '') {
            discountCounter = 1;
        }

        $discount.each(function () {
            var $discountScale = $(this).find('.lines > *');

            for (var i = 0; i < discountCounter; i++) {
                var $discountScale = $(this).find('.lines > *');

                $discountScale.eq(i).addClass('active');
            }
            ;
        })

        if (discountCounter == 5) {
            $discountNumbers.addClass('active');
            $('.discount-calculation').show();

            return true;
        }
    },
    checkDiscount: function () {
        var discountCounter = sessionStorage.getItem('discountCounter');
        if (discountCounter == 5) {
            return 1;
        }
        return 0;
    },
    refreshCart: function () {
        var discount = this.checkDiscount();

        $.pjax.reload({
            container: '#cart',
            timeout: 999999,
            data: {
                discount: discount
            },
            type: 'post'
        });
        this.updateCheckoutPrice();
    },
    updateCheckoutPrice: function () {
        var checkoutPrice = $('.cost-text');
        if (checkoutPrice.length) {
            var discount = this.checkDiscount();
            $.ajax({
                url: '/cart/check-price',
                data: {
                    discount: discount
                },
                success: function (data) {
                    if (data.cost !== false) {
                        checkoutPrice.text(data.cost);
                        $('#checkout-form').find('input[type=radio]:checked').trigger('change');
                        var parent = checkoutPrice.parent('.total');
                        if (discount) {
                            parent.addClass('active');
                        }
                    }
                },
                type: 'post'
            });
        }
    }
};

smiyan.init();