<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 * @var $model \app\modules\order\models\Order
 * @var $itemsDataProvider \yii\data\ActiveDataProvider
 */
use app\modules\order\models\Order;
use app\modules\order\models\OrderItem;
use app\modules\shop\models\Shop;
use yii\grid\GridView;
use yii\widgets\DetailView;

?>
<h3>Новый заказ оформлен на сайте:</h3>

<?= DetailView::widget([
    'model' => $model,
    'template' => '<tr><td{captionOptions}>{label}</td><td{contentOptions}>{value}</td></tr>',
    'options' => [
        'border' => "0",
        'cellpadding' => "0",
        'cellspacing' => "0",
        'width' => "500px",
    ],
    'attributes' => [
        [
            'attribute' => 'type',
            'value' => function (Order $model) {
                $types = $model::getTypes();
                return $types[$model->type];
            }
        ],
        [
            'label' => Yii::t('order', 'Address'),
            'value' => function (Order $model) {
                if ($model->type == $model::TYPE_SELF_DELIVERY) {
                    if ($shop = Shop::findOne($model->shop_id)) {
                        return $shop->name;
                    };
                    return $model->shop_id;
                } elseif ($model->type == $model::TYPE_DELIVERY) {
                    return $model->delivery_address;
                }
            }
        ],
        'delivery_time:deliveryTime',
        'customer_name',
        'customer_email:email',
        'customer_phone',
        'delivery_price',
        'total_cost',
        'created_at:date',
    ],
]) ?>

<br>

<?= GridView::widget([
    'dataProvider' => $itemsDataProvider,
    'layout' => '{items}',
    'columns' => [
        [
            'label' => Yii::t('order', 'Product Name'),
            'value' => function (OrderItem $model) {
                $name = $model->product_id;
                if ($product = $model->product) {
                    $name = $product->name;
                }
                if ($product->type == $product::TYPE_VARIANT) {
                    $name .= ', ' . $product->options['weight'] . 'г';
                }
                return $name;
            }
        ],
        [
            'label' => Yii::t('order', 'Product Type'),
            'value' => function (OrderItem $model) {
                if ($product = $model->product) {
                    $types = $product::getTypes();
                    return $types[$product->type];
                }
            }
        ],
        [
            'label' => Yii::t('order', 'Product Weight'),
            'value' => function (OrderItem $model) {
                if ($product = $model->product) {
                    if (isset($product->options['weight'])) {
                        return $product->options['weight'];
                    }
                }
                return 'не указано';
            }
        ],
        [
            'filter' => false,
            'attribute' => 'qty',

        ],
        [
            'filter' => false,
            'attribute' => 'price',

        ],
    ],
]) ?>
