<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/reset.css',
        'css/jquery.mCustomScrollbar.min.css',
        'css/font-awesome.min.css',
        'css/lightslider.min.css',
        'css/foundation.min.css',
        'css/styles.css',
    ];
    public $js = [
        'js/jquery.mobile.custom.min.js',
        'js/jquery.mousewheel.min.js',
        'js/jquery.waypoints.min.js',
        'js/jquery.mCustomScrollbar.concat.min.js',
        'js/lightslider.min.js',
        'js/TweenMax.min.js',
        'js/SS.js',
        'js/scripts.js',
        'js/cart.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
