<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 * @var $this \yii\web\View
 */
if (!$this->title) {
    $this->title = Yii::t('app', 'Корпоративным клиентам');
}
?>

<section class="type-13">
	<div class="wrapper">
		<div class="row">
			<div class="el-1 column">
		        <div class="el-3">
		            <div class="title type-1 var-b">
		                <img src="/img/smear_red_1.png" alt="Smear">
		                Корпоративным клиентам
		            </div>
		        </div>
			</div>
		</div>

		<div class="row">
			<div class="el-4 column small-12 medium-6 large-6">
				<div class="text-el-1 title type-4 var-a">
					Мы готовы воплотить в жизнь любые самые смелые идеи и разработать для вас уникальный ассортимент десертов.
				</div>

				<div class="text-el-2 text text-1 var-b">
					Кондитерский дом Смиян всегда открыт для сотрудничества с бизнес-партнерами, которые разделяют нашу любовь к высококачественным десертам.
				</div>

				<div class="text-el-3">
					<a class="link type-3" href="tel:044 223-78-22">
                        <i class="fa fa-phone" aria-hidden="true"></i>

                        <span>044 223-78-22</span>
                    </a>
				</div>

				<div class="text-el-4">
					<a class="link type-3" href="mailto:info@smiyan.com.ua">
	                    <i class="fa fa-envelope var-a" aria-hidden="true"></i>

	                    <span>info@smiyan.com.ua</span>
	                </a>
				</div>
			</div>

			<div class="column small-12 medium-1 large-1"></div>

			<div class="column small-12 medium-5 large-5">
				<form>
					<div class="inputs">
						<div class="input-outer">
							<input type="text" placeholder="Имя">
						</div>

						<div class="input-outer">
							<input type="text" placeholder="Почта или телефон">
						</div>

						<div class="input-outer">
							<input type="text" placeholder="Сообщение">
						</div>

						<div class="input-outer submit">
		                    <input class="button type-1" type="submit" value="Отправить">
		                </div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>