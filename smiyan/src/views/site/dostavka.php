<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 * @var $this \yii\web\View
 */
if (!$this->title) {
    $this->title = Yii::t('app', 'Доставка');
}
?>

<section class="type-12">
	<div class="wrapper">
		<div class="row">
			<div class="el-1 column">
		        <div class="el-3">
		            <div class="title type-1 var-b">
		                <img src="/img/smear_red_1.png" alt="Smear">
		                Доставка
		            </div>
		        </div>
			</div>
		</div>

		<div class="elements row">
			<div class="element column small-12 medium-6 large-3">
				<div class="text-el-1 title type-5 var-b regular">01</div>

				<div class="title type-5 var-c medium">
					Доставка <br>
					в любую точку Киева
				</div>
			</div>

			<div class="element column small-12 medium-6 large-3">
				<div class="text-el-1 title type-5 var-b regular">02</div>

				<div class="title type-5 var-c medium">
					Стоимость <br>
					доставки 100 грн
				</div>
			</div>

			<div class="element column small-12 medium-6 large-3">
				<div class="text-el-1 title type-5 var-b regular">03</div>

				<div class="title type-5 var-c medium">
					Изготовление <br>
					после 100% предоплаты
				</div>
			</div>

			<div class="element column small-12 medium-6 large-3">
				<div class="text-el-1 title type-5 var-b regular">04</div>

				<div class="title type-5 var-c medium">
					Приготовление <br>
					в течении 6 часов
				</div>
			</div>
		</div>

		<div class="el-4 row">
			<div class="column">
				<div class="title type-4 var-a">Заказ можно произвести через сайт или по телефону <a href="tel:0442237822">044 223 78 22</a></div>
			</div>
		</div>
	</div>
</section>