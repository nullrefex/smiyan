<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 * @var $this \yii\web\View
 */
if (!$this->title) {
    $this->title = Yii::t('app', 'Торт на заказ');
}
?>

<section class="type-10">
	<div class="wrapper">
		<div class="row type-2">
			<div class="el-1 column">
				<div class="el-3">
					<div class="title type-1 var-b">
						<img src="img/smear_red_1.png" alt="Smear">

						Торт на заказ
					</div>
				</div>
			</div>
		</div>

		<div class="row type-4">
			<div class="column">
				<div class="content var-b">
					<h5>Фестиваль уличной еды</h5>

					<p>
						Идейные соображения высшего порядка, а также сложившаяся структура организации позволяет выполнять важные задания по разработке соответствующий условий активизации. Разнообразный и богатый опыт укрепление и развитие структуры позволяет выполнять важные задания по разработке позиций, занимаемых участниками в отношении поставленных задач. Идейные соображения высшего порядка, а также сложившаяся структура организации позволяет выполнять важные задания по разработке соответствующий условий активизации. Разнообразный и богатый опыт укрепление и развитие структуры позволяет выполнять важные задания по разработке позиций, занимаемых участниками в отношении поставленных задач.
					</p>

					<h5>
						Идейные соображения высшего порядка, 
						а также сложившаяся структура организации 
						позволяет выполянять важные задания 
						по разработке соответствующих условий
					</h5>

					<p>
						Идейные соображения высшего порядка, а также сложившаяся структура организации позволяет выполнять важные задания по разработке соответствующий условий активизации. Разнообразный и богатый опыт укрепление и развитие структуры позволяет выполнять важные задания по разработке позиций, занимаемых участниками в отношении поставленных задач. Идейные соображения высшего порядка, а также сложившаяся структура организации позволяет выполнять важные задания по разработке соответствующий условий активизации. Разнообразный и богатый опыт укрепление и развитие структуры позволяет выполнять важные задания по разработке позиций, занимаемых участниками в отношении поставленных задач.
					</p>
				</div>
			</div>
		</div>

		<div class="row type-3">
			<div class="column">
				<div class="slider type-1">
					<div class="images">
						<div class="image" style="background-image: url(img/background_2.jpg);"></div>
						<div class="image" style="background-image: url(img/background_1.jpg);"></div>
						<div class="image" style="background-image: url(img/image_1.png);"></div>
						<div class="image" style="background-image: url(img/photo_1.jpg);"></div>
						<div class="image" style="background-image: url(img/background_2.jpg);"></div>
						<div class="image" style="background-image: url(img/background_1.jpg);"></div>
						<div class="image" style="background-image: url(img/image_1.png);"></div>
						<div class="image" style="background-image: url(img/photo_1.jpg);"></div>
						<div class="image" style="background-image: url(img/background_2.jpg);"></div>
						<div class="image" style="background-image: url(img/background_1.jpg);"></div>
						<div class="image" style="background-image: url(img/image_1.png);"></div>
						<div class="image" style="background-image: url(img/photo_1.jpg);"></div>
					</div>

					<div class="controls flex space-between">
						<div class="left">
							<img src="img/svg/arrow_black.svg" alt="Arrow">
						</div>
						<div class="right">
							<img src="img/svg/arrow_black.svg" alt="Arrow">
						</div>
					</div>

					<div class="sharing">
						<div class="main-icon flex middle center">
							<img src="img/sharing_icon.png" alt="Sharing">
						</div>

						<div class="another-icons">
							<a href="#">
								<i class="fa fa-facebook" aria-hidden="true"></i>
							</a>

							<a href="#">
								<i class="fa fa-twitter" aria-hidden="true"></i>
							</a>
						</div>
					</div>
				</div>

				<div class="slider type-2">
					<ul>
						<li style="background-image: url(img/background_2.jpg);"></li>
						<li style="background-image: url(img/background_1.jpg);"></li>
						<li style="background-image: url(img/image_1.png);"></li>
						<li style="background-image: url(img/photo_1.jpg);"></li>
						<li style="background-image: url(img/background_2.jpg);"></li>
						<li style="background-image: url(img/background_1.jpg);"></li>
						<li style="background-image: url(img/image_1.png);"></li>
						<li style="background-image: url(img/photo_1.jpg);"></li>
						<li style="background-image: url(img/background_2.jpg);"></li>
						<li style="background-image: url(img/background_1.jpg);"></li>
						<li style="background-image: url(img/image_1.png);"></li>
						<li style="background-image: url(img/photo_1.jpg);"></li>
					</ul>

					<div class="controls flex space-between">
						<div class="left">
							<img src="img/svg/arrow_black.svg" alt="Arrow">
						</div>
						<div class="right">
							<img src="img/svg/arrow_black.svg" alt="Arrow">
						</div>
					</div>
				</div>
			</div>
		</div>
</section>