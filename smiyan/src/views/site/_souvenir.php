<?php
/**
 * @var $model \app\modules\product\models\Product
 */
use app\helpers\Image;
use yii\helpers\Url;

?>

<a class="info-icon flex middle center" href="<?= Url::to(['/site/souvenir', 'id' => $model->identifier]) ?>">
    <span>i</span>
</a>

<div class="image">
    <img src="<?= Image::getThumbnail($model->image, 260, 600) ?>" alt="<?= $model->name ?>">
</div>

<div class="text-1">
    <?= $model->name ?>
</div>

<div class="text-2 price" data-price="<?= Yii::$app->formatter->asInteger($model->price) ?>">
    <span><?= Yii::$app->formatter->asInteger($model->price) ?></span> ₴
</div>

<div class="action flex middle center">
    <div class="counter flex middle">
        <div class="minus button type-2"></div>
        <div class="variant-total text text-3 var-a">0</div>
        <div class="plus button type-2"></div>
    </div>

    <a class="button type-1 add-to-cart" data-id="<?= $model->id ?>"><?= Yii::t('app', 'В корзину') ?></a>
</div>