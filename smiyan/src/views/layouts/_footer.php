<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 * @var $this \yii\web\View
 */
?>

<footer>
    <div class="wrapper">
        <div class="main-row row">
            <div class="el-1 small-12 medium-6 large-3 column flex direction-column space-between wrap">
                <div class="logo_button-outer">
                    <a class="logo" href="#">
                        <img src="/img/svg/logo_black.svg" alt="Logo">
                    </a>

                    <a class="button-el-1 button type-3" href="#">Торт на заказ</a>
                </div>

                <div class="text text-1 var-b">©<?= date('Y') ?> <?= Yii::t('app', 'СМІЯН Все права защищены') ?></div>
            </div>

            <div class="small-12 medium-6 large-3 column">
                <div class="el-2">
                    <ul class="anchors">
                        <li><a class="text text-5 var-a"
                               href="#"><?= Yii::t('app', 'Кондитерская') ?></a></li>
                        <li><a class="text text-5 var-a"
                               href="#"><?= Yii::t('app', 'Сувениры') ?></a></li>
                        <li><a class="text text-5 var-a" href="#section_3">О нас</a></li>
                        <li><a class="text text-5 var-a" href="#">Корпоративным клиентам</a></li>
                        <li><a class="text text-5 var-a"
                               href="#"><?= Yii::t('app', 'Активности') ?></a></li>
                        <li><a class="text text-5 var-a"
                               href="#"><?= Yii::t('app', 'Отзывы') ?></a></li>
                        <li><a class="text text-5 var-a" href="#">Доставка</a>
                        </li>
                        <li><a class="text text-5 var-a"
                               href="#"><?= Yii::t('app', 'Контакты') ?></a></li>
                    </ul>
                </div>
            </div>

            <div class="small-12 medium-6 large-3 column">
                <div class="el-3">
                    <div class="element">
                        <div class="text-el-1 text text-5 var-a">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            Кондитерский дом «Смиян» Киев, ул.Вышгородская 45
                        </div>

                        <div class="text text-2 var-d">
                            ПН - ВС с 8:00 до 22:00
                        </div>
                    </div>

                    <div class="element">
                        <div class="text-el-1 text text-5 var-a">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            Кондитерский дом «Смиян» Киев, ул.Вышгородская 45
                        </div>

                        <div class="text text-2 var-d">
                            ПН - ВС с 8:00 до 22:00
                        </div>
                    </div>

                    <div class="element">
                        <div class="text-el-1 text text-5 var-a">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            Кондитерский дом «Смиян» Киев, ул.Вышгородская 45
                        </div>

                        <div class="text text-2 var-d">
                            ПН - ВС с 8:00 до 22:00
                        </div>
                    </div>
                </div>
            </div>

            <div class="small-12 medium-6 large-3 column">
                <div class="el-4">
                    <div class="text-el-1">
                        <a class="link type-3" href="tel:044 223-78-22">
                            <i class="fa fa-phone" aria-hidden="true"></i>

                            <span>044 223-78-22</span>
                        </a>
                    </div>
                    
                    <div class="text-el-1">
                        <a class="link type-3" href="mailto:info@smiyan.com.ua">
                            <i class="fa fa-envelope var-a" aria-hidden="true"></i>

                            <span>info@smiyan.com.ua</span>
                        </a>
                    </div>

                    <div class="socials var-b flex middle">
                        <a href="<?= Yii::$app->get('settings')->get('contacts.facebook') ?>" target="_blank" rel="nofollow">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>

                        <a href="<?= Yii::$app->get('settings')->get('contacts.instagram') ?>" target="_blank" rel="nofollow">
                            <i class="fa fa-instagram" style="font-size: 18px;" aria-hidden="true"></i>
                        </a>

                        <a href="<?= Yii::$app->get('settings')->get('contacts.pinterest') ?>" target="_blank" rel="nofollow">
                            <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
