<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $this \yii\web\View
 */

use yii\helpers\Url;

Yii::$app->view->registerMetaTag([
    'property' => 'og:url',
    'content' => Url::current([], true),
]);
Yii::$app->view->registerMetaTag([
    'property' => 'fb:app_id',
    'content' => '269450823532589',
]);
Yii::$app->view->registerMetaTag([
    'property' => 'og:title',
    'content' => $this->title,
]);
?>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '269450823532589',
            xfbml: true,
            version: 'v2.10'
        });
        FB.AppEvents.logPageView();
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>