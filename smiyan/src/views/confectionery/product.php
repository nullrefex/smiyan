<?php
/** @var \app\modules\product\models\Product $model */
/** @var \yii\web\View $this */

use app\helpers\Image;
use yii\helpers\Url;

if (!$this->title) {
    $this->title = $model->name;
}
$neighbors = $model->getNeighbors();
$this->registerJs(<<<JS
$('.item.type-2 .counter .button').on('click', function () {
    var varTotal = $(this).parent().find('.variant-total'),
        totalPrice = $(this).parents('.item').find('.price span'),
        varPrice = $(this).parents('.item').find('.price').data('price');

    if ($(this).hasClass('minus')) {
        if (varTotal.text() != 0) {
            varTotal.text(parseInt(varTotal.text()) - 1);

            if (varTotal.text() >= 1) {
                totalPrice.text(parseInt(totalPrice.text()) - parseInt(varPrice));
            }
        }
    } else {
        varTotal.text(parseInt(varTotal.text()) + 1);

        if (varTotal.text() > 1) {
            totalPrice.text(parseInt(totalPrice.text()) + parseInt(varPrice));
        }
    }
});

$('.single-product-total-outer .add-to-cart').on('click', function () {
    $('.single-product-total-outer .price span').text($('.single-product-total-outer .price').data('price'));
});
JS
);
?>
<section class="type-2">
    <div class="wrapper">
        <div class="row">
            <div class="el-1 column">
                <div class="el-2">
                    <div class="title type-2 var-b flex inline middle">
                        <div class="lines flex">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>

                        <span><?= Yii::t('app', 'Кондитерская') ?></span>

                        <div class="lines reversed flex">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                </div>

                <div class="el-3">
                    <div class="title type-1 var-b">
                        <img src="/img/smear_red_1.png" alt="Smear">
                        <?= $model->name ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="column">
                <div class="product">
                    <?php if (isset($neighbors['prev'])): ?>
                        <div class="side left">
                            <div class="images">
                                <img src="<?= Image::getThumbnail($neighbors['prev']->image, 640, 600) ?>"
                                     alt="<?= $neighbors['prev']->name ?>">
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (isset($neighbors['next'])): ?>
                        <div class="side right">
                            <div class="images">
                                <img src="<?= Image::getThumbnail($neighbors['next']->image, 640, 600) ?>"
                                     alt="<?= $neighbors['next']->name ?>">
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="inner">
                        <?php if (isset($neighbors['prev'])): ?>

                            <a class="another-product left prev flex middle wrap"
                               href="<?= Url::to(['/confectionery/product', 'id' => $neighbors['prev']->identifier]) ?>">
                                <div class="list">
                                    <div><?= $neighbors['prev']->name ?></div>
                                </div>

                                <img class="arrow" src="/img/svg/arrow_black.svg" alt="Arrow">
                            </a>
                        <?php endif ?>

                        <?php if (isset($neighbors['next'])): ?>
                            <a class="another-product right next flex middle wrap"
                               href="<?= Url::to(['/confectionery/product', 'id' => $neighbors['next']->identifier]) ?>">
                                <div class="list">
                                    <div><?= $neighbors['next']->name ?></div>
                                </div>

                                <img class="arrow" src="/img/svg/arrow_black.svg" alt="Arrow">
                            </a>
                        <?php endif ?>

                        <div class="frame-1"></div>
                        <div class="frame-2"></div>

                        <div class="product-row flex wrap space-between middle">
                            <div class="images">
                                <img src="<?= Image::getThumbnail($model->image, 640, 600) ?>" alt="Product">
                            </div>

                            <div class="info-outer">
                                <div class="info">
                                    <div class="info-text-outer">
                                        <div class="el-4 title type-3 var-a">
                                            <?= $model->name ?>
                                        </div>

                                        <div class="el-5 content var-a">
                                            <p>
                                                <?= $model->description ?>
                                            </p>
                                        </div>
                                    </div>

                                    <?php if ($model->isVarious()): ?>
                                        <div class="variants el-6">
                                            <?php foreach ($variants = $model->variants as $variant):
                                                $options = $variant->options;
                                                ?>
                                                <div class="variant flex middle space-between">
                                                    <div class="icon">
                                                        <img src="/img/svg/part_<?= $variant->getSizeIcon() ?>.svg"
                                                             alt="Part">
                                                    </div>

                                                    <div class="weight text text-2 var-d">
                                                        <?= $options['weight'] ?> <?= Yii::t('app', 'г') ?>
                                                    </div>

                                                    <div class="price title type-4 var-a"
                                                         data-price="<?= Yii::$app->formatter->asInteger($variant->price) ?>">
                                                        <?= Yii::$app->formatter->asInteger($variant->price) ?> ₴
                                                    </div>

                                                    <div class="counter flex middle">
                                                        <div class="minus button type-2"></div>
                                                        <div class="variant-total text-3 var-a"
                                                             data-id="<?= $variant->id ?>">0
                                                        </div>
                                                        <div class="plus button type-2"></div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                        <div class="total flex middle">
                                            <div class="el-7 number title type-4 var-a">
                                                <span>0</span> ₴
                                            </div>

                                            <a class="button type-1 add-various"><?= Yii::t('app', 'В корзину') ?></a>
                                        </div>
                                    <?php endif; ?>

                                    <?php if ($model->isSingle()): ?>
                                        <div class="single-product-total-outer">
                                            <div class="total flex middle item type-2 flex wrap">
                                                <div class="el-7 number title type-4 var-a price" data-price="<?= Yii::t('app', '{price}', [
                                                    'price' => Yii::$app->formatter->asInteger($model->price)]) ?>">
                                                    <?= Yii::t('app', '<span>{price}</span> ₴', [
                                                        'price' => Yii::$app->formatter->asInteger($model->price)]) ?>
                                                </div>

                                                <div class="action flex middle center" data-id="<?= $model->id ?>">
                                                    <div class="counter flex middle">
                                                        <div class="minus button type-2"></div>
                                                        <div class="variant-total text text-3 var-a">0</div>
                                                        <div class="plus button type-2"></div>
                                                    </div>

                                                    <a class="button type-1 add-to-cart"
                                                       data-id="<?= $model->id ?>"><?= Yii::t('app', 'В корзину') ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="el-8 column">
                <a class="link type-1 var-a" href="<?= Url::to(['/confectionery']) ?>#catalog">
                    <img class="left" src="/img/svg/arrow_red.svg" alt="Arrow">
                    <?= Yii::t('app', 'Назад в кондитерскую') ?>
                </a>
            </div>
        </div>
    </div>
</section>