<?php

use nullref\core\widgets\Multilingual;
use yii\base\Model;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Shop */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs(<<<JS

	var lat = 0;
	var lng = 0;

	if (jQuery('#shop-lat').val()){
		lat = parseFloat(jQuery('#shop-lat').val());
	}
	if (jQuery('#shop-lng').val()){
		lng = parseFloat(jQuery('#shop-lng').val());
	}
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: lat, lng: lng},
    zoom: 8
  });

  if (lat && lng){
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng), 
        map: map
    });
  }

  var marker;

  google.maps.event.addListener(map, 'click', function(event) {
   	placeMarker(event.latLng);
	});

	function placeMarker(location) {
		if (marker) {
			marker.setPosition(location);
		} else {
	    marker = new google.maps.Marker({
	        position: location, 
	        map: map
	    });
		}
		jQuery('#shop-lat').val(location.lat());
		jQuery('#shop-lng').val(location.lng());
	}
JS
);
?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9sMewitXRdb34h6DvN-DtEwslTdcGSv0&sensor=false"></script>

<div class="shop-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= Multilingual::widget(['model' => $model,
                'tab' => function (ActiveForm $form, Model $model) {
                    echo $form->field($model, 'name')->textInput();
                    echo $form->field($model, 'schedule')->textInput();
                }
            ]) ?>
        </div>
    </div>

    <div class="row">

	    <div class="col-md-5">
	    <?= $form->field($model, 'lat')->textInput(['maxlength' => true, 'readonly' => true]) ?>

	    <?= $form->field($model, 'lng')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	    </div>

	    <div class="col-md-7">
	    	<div class="row">
                <div id="map" style="height:400px; width:700px"></div>
			</div>
	    </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('shop', 'Create') : Yii::t('shop', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
