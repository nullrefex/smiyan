<?php

namespace app\modules\shop\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%shop_translation}}".
 *
 * @property integer $id
 * @property integer $language
 * @property string $name
 * @property string $schedule
 * @property integer $shop_id
 */
class ShopTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'shop_id'], 'integer'],
            [['name', 'schedule'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('shop', 'ID'),
            'language' => Yii::t('shop', 'Язык'),
            'name' => Yii::t('shop', 'Название'),
            'schedule' => Yii::t('shop', 'График'),
            'shop_id' => Yii::t('shop', 'Shop ID'),
        ];
    }
}
