<?php

namespace app\modules\shop\migrations;

use yii\db\Migration;

class M170708175852Shop__improve_multilang extends Migration
{
    const SHOP_TABLE = '{{%shop}}';
    const SHOP_TRANSLATION_TABLE = '{{%shop_translation}}';

    public function safeUp()
    {
        $this->dropColumn(self::SHOP_TABLE, 'schedule');
        $this->addColumn(self::SHOP_TRANSLATION_TABLE, 'schedule', $this->string());
    }

    public function safeDown()
    {
        $this->addColumn(self::SHOP_TABLE, 'schedule', $this->string());
    }
}
