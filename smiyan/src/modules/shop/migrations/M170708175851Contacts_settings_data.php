<?php

namespace app\modules\shop\migrations;

use pheme\settings\models\Setting;
use yii\db\Migration;

class M170708175851Contacts_settings_data extends Migration
{
    public function up()
    {
        $this->insert(Setting::tableName(), [
            'type' => 'string',
            'section' => 'contacts',
            'key' => 'phone',
            'value' => '044 223-78-22',
            'active' => '1',
            'created' => '2017-07-08 20:43:30',
            'modified' => null,
        ]);

        $this->insert(Setting::tableName(), [
            'type' => 'string',
            'section' => 'contacts',
            'key' => 'email',
            'value' => 'sweetsmiyan@gmail.com',
            'active' => '1',
            'created' => '2017-07-08 20:43:31',
            'modified' => null,
        ]);
    }

    public function down()
    {
        $this->delete(Setting::tableName(), ['section' => 'contacts', 'key' => 'phone']);
        $this->delete(Setting::tableName(), ['section' => 'contacts', 'key' => 'email']);
        return true;
    }
}
