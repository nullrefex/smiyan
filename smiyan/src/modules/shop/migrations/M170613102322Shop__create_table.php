<?php

namespace app\modules\shop\migrations;

use yii\db\Migration;

class M170613102322Shop__create_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%shop}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'schedule' => $this->string(),
            'lat' => $this->decimal(12, 10),
            'lng' => $this->decimal(12, 10),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%shop}}');
    }
}
