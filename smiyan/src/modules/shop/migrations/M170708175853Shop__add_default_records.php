<?php

namespace app\modules\shop\migrations;

use app\modules\shop\models\Shop;
use yii\db\Migration;

class M170708175853Shop__add_default_records extends Migration
{
    const SHOP_TABLE = '{{%shop}}';
    const SHOP_TRANSLATION_TABLE = '{{%shop_translation}}';

    public function safeUp()
    {
        $data = [
            [
                'name_ru' => 'Кондитерский дом «Смиян» Вышгородская 45',
                'schedule_ru' => 'ПН — ВС с 12:00 до 23:00',
                'name_uk' => 'Кондитерский дом «Смиян» Вышгородская 45',
                'schedule_uk' => 'ПН — ВС с 12:00 до 23:00',
                'name_en' => 'Кондитерский дом «Смиян» Вышгородская 45',
                'schedule_en' => 'ПН — ВС с 12:00 до 23:00',
                'lat' => 50.5236355,
                'lng' => 30.5422344,
            ],
            [
                'name_ru' => '«Milk & honey» Пушкинская 9 а',
                'schedule_ru' => 'ПН — ВС с 12:00 до 23:00',
                'name_uk' => '«Milk & honey» Пушкинская 9 а',
                'schedule_uk' => 'ПН — ВС с 12:00 до 23:00',
                'name_en' => '«Milk & honey» Пушкинская 9 а',
                'schedule_en' => 'ПН — ВС с 12:00 до 23:00',
                'lat' => 50.5236355,
                'lng' => 30.5422344,
            ],
        ];

        foreach ($data as $datum) {
            $model = new Shop();
            $model->load($datum, '');
            $model->save();
        }
    }

    /**
     *
     */
    public function safeDown()
    {
        Shop::deleteAll();
    }
}
