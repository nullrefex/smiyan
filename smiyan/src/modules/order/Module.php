<?php
namespace app\modules\order;

use nullref\core\interfaces\IAdminModule;
use Yii;
use yii\base\Module as BaseModule;

/**
 * settings module definition class
 */
class Module extends BaseModule implements IAdminModule
{
    /**
     * @return array
     */
    public static function getAdminMenu()
    {
        return [
            'url' => ['/order/admin'],
            'icon' => 'shopping-cart',
            'label' => Yii::t('order', 'Order'),
        ];
    }
}
