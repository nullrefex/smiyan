<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\order\helpers;


use app\modules\order\models\Order;
use Yii;
use yii\data\ActiveDataProvider;

class Mail
{
    /**
     * @return string
     */
    public static function getAdminEmail()
    {
        return Yii::$app->get('settings')->get('contacts.email');
    }
    /**
     * @param Order $model
     * @param $to
     * @return bool
     */
    public static function sendNewOrderMail($model, $to = false)
    {
        if (!$to) {
            $to = self::getAdminEmail();
        }
        $lang = Yii::$app->language;

        Yii::$app->language = 'ru';

        $itemsDataProvider = new ActiveDataProvider([
            'query' => $model->getOrderItems(),
        ]);

        $send = Yii::$app->mailer->compose('new-order', [
            'model' => $model,
            'itemsDataProvider' => $itemsDataProvider,
        ])
            ->setFrom(self::getAdminEmail())
            ->setTo($to)
            ->setSubject('Новый заказ')
            ->send();

        Yii::$app->language = $lang;
        return $send;
    }

    /**
     * @param Order $model
     * @param $to
     * @return bool
     */
    public static function sendNewOrderCustomerMail($model, $to)
    {
        $lang = Yii::$app->language;

        Yii::$app->language = 'ru';

        $itemsDataProvider = new ActiveDataProvider([
            'query' => $model->getOrderItems(),
        ]);

        $send = Yii::$app->mailer->compose('new-order-customer', [
            'model' => $model,
            'itemsDataProvider' => $itemsDataProvider,
        ])
            ->setFrom(self::getAdminEmail())
            ->setTo($to)
            ->setSubject('Новый заказ')
            ->send();

        Yii::$app->language = $lang;
        return $send;
    }

    /**
     * @param Order $model
     * @param $to
     * @return bool
     */
    public static function sendPaidOrderMail($model, $to = false)
    {
        if (!$to) {
            $to = self::getAdminEmail();
        }
        $lang = Yii::$app->language;

        Yii::$app->language = 'ru';

        $itemsDataProvider = new ActiveDataProvider([
            'query' => $model->getOrderItems(),
        ]);

        $send = Yii::$app->mailer->compose('paid-order', [
            'model' => $model,
            'itemsDataProvider' => $itemsDataProvider,
        ])
            ->setFrom(self::getAdminEmail())
            ->setTo($to)
            ->setSubject('Заказ оплачен')
            ->send();

        Yii::$app->language = $lang;
        return $send;
    }
}