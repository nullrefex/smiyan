<?php

namespace app\modules\order\migrations;

use app\modules\order\models\Order;
use yii\db\Migration;

class M170804213834Order_lp_order_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'lp_order_id', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn(Order::tableName(), 'lp_order_id');

        return true;
    }
}
