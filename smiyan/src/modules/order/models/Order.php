<?php

namespace app\modules\order\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property int $id
 * @property int $shop_id
 * @property string $delivery_address
 * @property string $delivery_time
 * @property string $customer_name
 * @property string $customer_email
 * @property string $customer_phone
 * @property string $type
 * @property string $total_cost
 * @property string $delivery_price
 * @property string $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $popup_shown
 * @property int $liqpay_data
 * @property string $lp_order_id
 *
 * @property OrderItem[] $orderItems
 */
class Order extends ActiveRecord
{
    const TYPE_DELIVERY = 'delivery';
    const TYPE_SELF_DELIVERY = 'self-delivery';

    const STATUS_NEW = 'new';
    const STATUS_PROCESSING = 'processing';
    const STATUS_PAID = 'paid';
    const STATUS_DONE = 'done';
    const STATUS_DECLINED = 'declined';

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_DELIVERY => Yii::t('order', 'Delivery'),
            self::TYPE_SELF_DELIVERY => Yii::t('order', 'Self Delivery'),
        ];
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_NEW => Yii::t('order', 'New'),
//            self::STATUS_PROCESSING => Yii::t('order', 'Processing'),
            self::STATUS_PAID => Yii::t('order', 'Paid'),
//            self::STATUS_DONE => Yii::t('order', 'Done'),
//            self::STATUS_DECLINED => Yii::t('order', 'Declined'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'total_cost', 'customer_name', 'customer_email', 'customer_phone', 'type', 'status'], 'required'],
            [['shop_id', 'created_at', 'updated_at', 'popup_shown'], 'integer'],
            [['total_cost', 'delivery_price'], 'number'],
            [['customer_name', 'customer_email', 'customer_phone', 'type', 'status', 'delivery_time', 'delivery_address', 'lp_order_id'], 'string', 'max' => 255],
            ['customer_email', 'email'],
            ['liqpay_data', 'string'],
            ['customer_phone', function ($attribute, $value) {
                if (!preg_match("/^\+380[0-9]{9}$/", $this->customer_phone) || strlen($this->customer_phone) !== 13) {
                    $this->addError($attribute, Yii::t('order', 'Формат номера телефона - +380XXXXXXXXX.'));
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('order', 'ID'),
            'shop_id' => Yii::t('order', 'Shop ID'),
            'delivery_address' => Yii::t('order', 'Delivery Address'),
            'delivery_price' => Yii::t('order', 'Delivery Price'),
            'delivery_time' => Yii::t('order', 'Delivery Time'),
            'customer_name' => Yii::t('order', 'Customer Name'),
            'customer_email' => Yii::t('order', 'Customer Email'),
            'customer_phone' => Yii::t('order', 'Customer Phone'),
            'type' => Yii::t('order', 'Type'),
            'total_cost' => Yii::t('order', 'Total Price'),
            'status' => Yii::t('order', 'Status'),
            'created_at' => Yii::t('order', 'Create At'),
            'updated_at' => Yii::t('order', 'Updated At'),
            'popup_shown' => Yii::t('order', 'Popup Shown'),
            'liqpay_data' => Yii::t('order', 'LiqPay Data'),
            'lp_order_id' => Yii::t('order', 'LiqPay Order Id'),
        ];
    }

    /**
     * @inheritdoc
     * @return OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }

    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['order_id' => 'id']);
    }

    public static function findByLpOrderId($lpOrderId)
    {
        return self::find()->where(['lp_order_id' => $lpOrderId])->one();
    }


    /**
     * Return delivery price by order type
     * @param $type
     * @return float
     */
    public static function getDeliveryPrice($type)
    {
        if ($type == Order::TYPE_DELIVERY) {
            return Yii::$app->get('settings')->get('order.delivery_price');
        }
        return 0;
    }

}
