<?php

use app\modules\order\models\Order;
use app\modules\shop\models\Shop;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\order\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('order', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'type',
                'value' => function (Order $model) {
                    $types = $model::getTypes();
                    return $types[$model->type];
                },
                'filter' => Order::getTypes(),
            ],
            [
                'label' => Yii::t('order', 'Address'),
                'value' => function (Order $model) {
                    if ($model->type == $model::TYPE_SELF_DELIVERY) {
                        if ($shop = Shop::findOne($model->shop_id)) {
                            return $shop->name;
                        }
                        return $model->shop_id;
                    } elseif ($model->type == $model::TYPE_DELIVERY) {
                        return $model->delivery_address;
                    }
                },
                'filter' => Shop::getShopList(),
            ],
            'customer_name',
            'customer_email:email',
            'customer_phone',
            // 'options:ntext',
            'total_cost',
            [
                'attribute' => 'status',
                'value' => function (Order $model) {
                    $statuses = $model::getStatuses();
                    return $statuses[$model->status];
                },
                'filter' => Order::getStatuses()
            ],
            'created_at:date',
            // 'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}'
            ]
        ],
    ]); ?>

</div>
