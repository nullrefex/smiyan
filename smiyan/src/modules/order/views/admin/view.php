<?php

use app\modules\order\models\Order;
use app\modules\order\models\OrderItem;
use app\modules\shop\models\Shop;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\order\models\Order */
/* @var $itemsDataProvider \yii\data\ActiveDataProvider */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('order', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('order', 'List'), ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('order', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('order', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'type',
                'value' => function (Order $model) {
                    $types = $model::getTypes();
                    return $types[$model->type];
                }
            ],
            [
                'label' => Yii::t('order', 'Address'),
                'value' => function (Order $model) {
                    if ($model->type == $model::TYPE_SELF_DELIVERY) {
                        if ($shop = Shop::findOne($model->shop_id)) {
                            return $shop->name;
                        };
                        return $model->shop_id;
                    } elseif ($model->type == $model::TYPE_DELIVERY) {
                        return $model->delivery_address;
                    }
                }
            ],
            'delivery_time:deliveryTime',
            'customer_name',
            'customer_email:email',
            'customer_phone',
            'delivery_price',
            'total_cost',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Yii::t('order', 'Order Items') ?>
            </h1>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $itemsDataProvider,
        'layout' => '{items}',
        'columns' => [
            [
                'label' => Yii::t('order', 'Product Name'),
                'value' => function (OrderItem $model) {
                    $name = $model->product_id;
                    if ($product = $model->product) {
                        $name = $product->name;
                    }
                    if ($product->type == $product::TYPE_VARIANT) {
                        $name .= ', ' . $product->options['weight'] . 'г';
                    }
                    return $name;
                }
            ],
            [
                'label' => Yii::t('order', 'Product Type'),
                'value' => function (OrderItem $model) {
                    if ($product = $model->product) {
                        $types = $product::getTypes();
                        return $types[$product->type];
                    }
                }
            ],
            [
                'label' => Yii::t('order', 'Product Weight'),
                'value' => function (OrderItem $model) {
                    if ($product = $model->product) {
                        if (isset($product->options['weight'])) {
                            return $product->options['weight'];
                        }
                    }
                    return 'не указано';
                }
            ],
            'qty',
            'price'
        ],
    ]) ?>
</div>
