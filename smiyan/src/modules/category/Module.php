<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\category;


use nullref\category\Module as BaseModule;
use nullref\core\interfaces\IHasMigrateNamespace;

class Module extends BaseModule implements IHasMigrateNamespace
{
    public $controllerNamespace = '\nullref\category\controllers';

    /**
     * Add custom ns for migrations
     * @param $defaults
     * @return array
     */
    public function getMigrationNamespaces($defaults)
    {
        return array_merge($defaults, ['\nullref\category\migrations']);
    }
}