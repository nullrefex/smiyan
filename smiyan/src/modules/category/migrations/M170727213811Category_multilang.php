<?php

namespace app\modules\category\migrations;

use app\modules\category\models\Category;
use yii\db\Migration;

class M170727213811Category_multilang extends Migration
{
    const TABLE = '{{%category_translation}}';

    public function up()
    {
        $this->createTable(self::TABLE, [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'language' => $this->integer(),
            'title' => $this->string()
        ]);
        $this->dropColumn(Category::tableName(), 'title');
    }

    public function down()
    {
        $this->dropTable(self::TABLE);
        $this->addColumn(Category::tableName(), 'title', $this->string());
        return true;
    }
}
