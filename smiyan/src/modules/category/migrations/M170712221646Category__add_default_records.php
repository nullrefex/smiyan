<?php

namespace app\modules\category\migrations;

use app\modules\category\models\Category;
use yii\db\Migration;

class M170712221646Category__add_default_records extends Migration
{
    public function safeUp()
    {
        $categories = [
            ['title' => 'Торты', 'image' => '/img/categories/cake.svg'],
            ['title' => 'Десерты', 'image' => '/img/categories/desserts.svg'],
            ['title' => 'Эклеры', 'image' => '/img/categories/eclair.svg'],
            ['title' => 'Макаруны', 'image' => '/img/categories/macaroon.svg'],
            ['title' => 'Печенье', 'image' => '/img/categories/biscuit.svg'],
            ['title' => 'Пироги', 'image' => '/img/categories/pie.svg'],
        ];

        foreach ($categories as $category) {
            $model = new Category();
            $model->load(array_merge($category, ['parent_id' => Category::ROOT_PARENT]), '');
            $model->save();
        }
    }

    public function safeDown()
    {
        Category::deleteAll();
    }
}
