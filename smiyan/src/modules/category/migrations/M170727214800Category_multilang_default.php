<?php

namespace app\modules\category\migrations;

use app\modules\category\models\CategoryTranslation;
use yii\db\Migration;

class M170727214800Category_multilang_default extends Migration
{
    public function up()
    {
        $categories = [
            ['category_id' => 1, 'language' => 1, 'title' => 'Торты'],
            ['category_id' => 1, 'language' => 2, 'title' => 'Торты'],
            ['category_id' => 1, 'language' => 3, 'title' => 'Торты'],
            ['category_id' => 2, 'language' => 1, 'title' => 'Десерты'],
            ['category_id' => 2, 'language' => 2, 'title' => 'Десерты'],
            ['category_id' => 2, 'language' => 3, 'title' => 'Десерты'],
            ['category_id' => 3, 'language' => 1, 'title' => 'Эклеры'],
            ['category_id' => 3, 'language' => 2, 'title' => 'Эклеры'],
            ['category_id' => 3, 'language' => 3, 'title' => 'Эклеры'],
            ['category_id' => 4, 'language' => 1, 'title' => 'Макаруны'],
            ['category_id' => 4, 'language' => 2, 'title' => 'Макаруны'],
            ['category_id' => 4, 'language' => 3, 'title' => 'Макаруны'],
            ['category_id' => 5, 'language' => 1, 'title' => 'Печенье'],
            ['category_id' => 5, 'language' => 2, 'title' => 'Печенье'],
            ['category_id' => 5, 'language' => 3, 'title' => 'Печенье'],
            ['category_id' => 6, 'language' => 1, 'title' => 'Пироги'],
            ['category_id' => 6, 'language' => 2, 'title' => 'Пироги'],
            ['category_id' => 6, 'language' => 3, 'title' => 'Пироги'],
        ];

        foreach ($categories as $category) {
            $model = new CategoryTranslation();
            $model->load($category, '');
            $model->save(false);
        }
    }

    public function down()
    {
        CategoryTranslation::deleteAll();
        return true;
    }
}
