<?php

use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use nullref\cms\models\Page;
use unclead\multipleinput\MultipleInput;

/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $form \yii\widgets\ActiveForm
 * @var $model \app\modules\cms\models\Page
 */
?>
<div class="row">

    <div class="col-md-12">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]); ?>
    </div>

    <div class="col-md-12">
        <h4><?= Yii::t('cms', 'Meta Tags') ?></h4>

        <?= $form->field($model, 'meta')->widget(MultipleInput::className(), [
            'columns' => [
                [
                    'name' => 'name',
                    'title' => Yii::t('cms', 'Name'),
                    'type' => 'dropDownList',
                    'items' => call_user_func([Page::getDefinitionClass(), 'getMetaTypesList']),
                ],
                [
                    'name' => 'content',
                    'title' => Yii::t('cms', 'Content'),
                ]
            ]
        ])->label(false) ?>
    </div>

    <div id="editor-wrapper" class="col-md-12">

        <div class="row" data-type="<?= Page::TYPE_CONTENT ?>" style="display: none">
            <div class="col-md-12">
                <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder-backend', [
                        'height' => 300,
                    ]),
                ]) ?>
            </div>
        </div>

        <div class="row" data-type="<?= Page::TYPE_BLOCKS ?>" style="display: none">
            <?= $this->render('_blocks', [
                'model' => $model,
            ]) ?>
        </div>

    </div>
</div>
