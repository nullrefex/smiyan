<?php

use nullref\core\widgets\Multilingual;
use yii\widgets\ActiveForm;

/**
 * @var $form ActiveForm
 * @var $block \app\modules\cms\blocks\title\Block
 * @var $this \yii\web\View
 */

echo Multilingual::widget(['model' => $block,
    'tab' => function (ActiveForm $form, $model) {
        echo $form->field($model, 'inscription')->textInput();
        echo $form->field($model, 'title')->textInput();
        echo $form->field($model, 'link_title')->textInput();
    }
]);

//echo $form->field($block, 'titleClass')->textInput();
