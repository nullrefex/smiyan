<?php

namespace app\modules\cms\blocks\title;

use app\modules\cms\components\BaseBlock;

/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $inscription;
    public $title;
    public $titleClass = 'el-2 title type-1 var-a anim-2';
    public $link_title;

    public function getMultilingualAttributes()
    {
        return ['inscription', 'title', 'link_title'];
    }

    public function getName()
    {
        return 'Title Block';
    }

    public function rules()
    {
        return [
            [['inscription', 'title', 'titleClass', 'link_title'], 'string'],
        ];
    }

}