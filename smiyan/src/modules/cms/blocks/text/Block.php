<?php

namespace app\modules\cms\blocks\text;

use app\modules\cms\components\BaseBlock;

/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $content;

    public function getMultilingualAttributes()
    {
        return ['content'];
    }

    public function getName()
    {
        return 'Text Block';
    }

    public function rules()
    {
        return [
            [['content'], 'string'],
        ];
    }

}