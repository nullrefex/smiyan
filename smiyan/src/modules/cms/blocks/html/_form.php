<?php

use app\helpers\Cms as CmsHelper;
use mihaildev\ckeditor\CKEditor;
use nullref\core\widgets\Multilingual;
use yii\widgets\ActiveForm;

/**
 * @var $form ActiveForm
 * @var $block \app\modules\cms\blocks\html\Block
 * @var $this \yii\web\View
 */

echo Multilingual::widget(['model' => $block,
    'tab' => function (ActiveForm $form, $model) {
        echo $form->field($model, 'content')->widget(CKEditor::className(), CmsHelper::getEditorOptions());
    }
]);

echo $form->field($block, 'tag')->textInput();
echo $form->field($block, 'tagClass')->textInput();