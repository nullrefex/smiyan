<?php

namespace app\modules\cms\models;

use nullref\useful\behaviors\SerializeBehavior;
use Yii;

/**
 * This is the model class for table "{{%cms_page_translation}}".
 *
 * @property int $id
 * @property int $page_id
 * @property int $language
 * @property string $title
 * @property string $content
 * @property string $meta
 */
class PageTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_page_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'language'], 'integer'],
            [['title'], 'required'],
            [['content', 'meta'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'language' => 'Language',
            'title' => 'Title',
            'content' => 'Content',
            'meta' => 'Meta',
        ];
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'serialize' => [
                'class' => SerializeBehavior::className(),
                'fields' => ['meta'],
            ]
        ];
    }
}
