<?php

use app\helpers\Languages;
use app\modules\reviews\models\Reviews;
use app\modules\shop\models\Shop;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\reviews\models\ReviewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('reviews', 'Reviews');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reviews-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('reviews', 'Create Reviews'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'customer_name',
            'contacts',
            [
                'attribute' => 'shop_id',
                'label' => Yii::t('reviews', 'Shop'),
                'value' => function ($model) {
                    $shop = Shop::findOne($model->shop_id);
                    if ($shop) return $shop->name;
                    return $model->shop_id;
                },
                'filter' => Shop::getShopList(),
            ],
            [
                'attribute' => 'rating',
                'filter' => Reviews::getRatingList()
            ],
            [
                'attribute' => 'language',
                'filter' => Languages::getMap(),
                'value' => function (Reviews $model) {
                    return Languages::getTitleById($model->language);
                },
            ],
            [
                'attribute' => 'approved',
                'filter' => Reviews::getApprovedList(),
                'value' => function ($model) {
                    if ($model->approved == $model::APPROVED) {
                        return '<span style="color: green;" class="glyphicon glyphicon-ok"></span>';
                    } elseif ($model->approved == $model::NOT_APPROVED) {
                        return '<span style="color: red;"class="glyphicon glyphicon-remove"></span>';
                    } else {
                        return $model->approved;
                    }
                },
                'format' => 'raw',
            ],

            ['attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y H:i'],
            ],
            [
                'attribute' => 'updated_at',
                'format' => ['date', 'php:d.m.Y H:i'],
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
