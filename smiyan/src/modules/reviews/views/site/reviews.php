<?php
use app\modules\shop\models\Shop;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * @var $message
 * @var $model \app\modules\reviews\models\Reviews
 */
$this->title = Yii::t('reviews', 'Reviews');
?>
<section class="type-4 var-b">
    <?= \app\widgets\Reviews::widget(['praiseUs' => false]) ?>
</section>

<section class="type-8">
    <div class="wrapper">
        <img class="smear-bottom" src="/img/smear_bottom.png" alt="Smear">

        <div class="row type-1">
            <div class="column">
                <div class="el-1">
                    <div class="title type-1 var-b">
                        <img src="/img/smear_red_1.png" alt="Smear">

                        <?= Yii::t('reviews', 'Оставить отзыв') ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row type-2">
            <div class="column">
                <?php Pjax::begin(['timeout' => 9999999]); ?>
                <?php $form = ActiveForm::begin([
                    'fieldConfig' => ['options' => ['class' => 'input-outer']],
                    'enableClientValidation' => false,
                    'options' => [
                        'data-pjax' => true,
                    ],
                ]); ?>
                <div class="blocks type-1 flex space-between wrap">
                    <div class="block type-1">
                        <?= $form->field($model, 'customer_name')->textInput(['maxlength' => true, 'type' => 'text', 'placeholder' => Yii::t('reviews', 'Имя')])->label(false) ?>

                        <?= $form->field($model, 'contacts')->textInput(['maxlength' => true, 'type' => 'text', 'placeholder' => Yii::t('reviews', 'Почта или телефон')])->label(false) ?>
                    </div>

                    <div class="block type-1">
                        <?= $form->field($model, 'shop_id', ['options' => ['class' => 'input-outer select']])->dropDownList(Shop::getShopList(), ['prompt' => Yii::t('reviews', 'Выберите ресторан')])->label(false) ?>

                        <div class="input-outer">
                            <div class="caption"><?= Yii::t('reviews', 'Оцените обслуживание') ?></div>

                            <div class="stars">
                                <div class="star-outer">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </div>

                                <div class="star-outer">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </div>

                                <div class="star-outer">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </div>

                                <div class="star-outer">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </div>

                                <div class="star-outer">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </div>
                            </div>
                            <?= $form->field($model, 'rating')->hiddenInput()->label(false) ?>
                        </div>
                    </div>

                    <div class="block type-2">
                        <?= $form->field($model, 'message', ['options' => ['class' => 'input-outer textarea']])->textarea(['maxlength' => true, 'placeholder' => Yii::t('reviews', 'Сообщение')])->label(false) ?>
                    </div>
                </div>

                <div class="input-outer submit">
                    <input class="button type-1" type="submit" value="<?= Yii::t('reviews', 'Отправить') ?>">
                </div>
                <?php ActiveForm::end(); ?>
                <div id="review-popup" class="pop-up flex middle center">
                    <div class="inner">
                        <div class="title type-3 var-a">
                            <?= $message ?>
                        </div>
                    </div>
                </div>
                <?php
                $js = 'smiyan.sectionType_8();';
                if ($message) {
                    $js .= 'smiyan.showReviewPopup();';
                }
                $this->registerJs($js);
                Pjax::end();
                ?>
            </div>
        </div>
    </div>
</section>