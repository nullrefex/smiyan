<?php

namespace app\modules\reviews\controllers;

use app\components\PageMetaTagsFilter;
use app\helpers\Languages;
use app\modules\reviews\models\Reviews;
use Yii;
use yii\web\Controller;

class SiteController extends Controller
{

    public function behaviors()
    {
        return [
            'seo' => [
                'class' => PageMetaTagsFilter::class,
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new Reviews();
        $message = '';

        if ($model->load(Yii::$app->request->post())) {
            $model->approved = $model::NOT_APPROVED;
            $model->language = Languages::get()->getId();
            if ($model->save()) {
                $message = Yii::t('reviews', 'Спасибо за Ваш отзыв!');
                $model = new Reviews();
            } else {
                if (!Yii::$app->request->isPjax) {
                    $message = Yii::t('reviews', 'Что то пошло не так :(');
                }
            }
        }
        return $this->render('reviews', [
            'model' => $model,
            'message' => $message,
        ]);
    }
}