<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\settings\models;


class ProductList extends SettingsModel
{
    const SETTINGS_SECTION = 'product';

    /** @var array|string */
    public $list;

    /**
     * @return string
     */
    public function getSection()
    {
        return self::SETTINGS_SECTION;
    }

    /**
     * @return array
     */
    public static function getList()
    {
        $model = new self();

        return $model->list;
    }

    /**
     * Convert string to list
     */
    public function init()
    {
        parent::init();
        if ($this->list) {
            $this->list = unserialize($this->list);
        } else {
            $this->list = [];
        }
    }

    /**
     * Convert return list to serialized string
     *
     * @param bool $runValidation
     * @return bool
     */
    public function save($runValidation = true)
    {
        $this->list = serialize($this->list);
        return parent::save($runValidation);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['list'], 'required'],
            [['list'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'list' => 'Список',
        ];
    }
}