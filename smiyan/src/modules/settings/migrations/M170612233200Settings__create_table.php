<?php

namespace app\modules\settings\migrations;

use yii\db\Migration;

class M170612233200Settings__create_table extends Migration
{
    public function safeUp()
    {
        $this->createTable(
            '{{%settings}}',
            [
                'id' => $this->primaryKey(),
                'type' => $this->string(255)->notNull(),
                'section' => $this->string(255)->notNull(),
                'key' => $this->string(255)->notNull(),
                'value' => $this->text(),
                'active' => $this->boolean(),
                'created' => $this->dateTime(),
                'modified' => $this->dateTime(),
            ]
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}
