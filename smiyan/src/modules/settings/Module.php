<?php

namespace app\modules\settings;

use nullref\core\interfaces\IAdminModule;
use pheme\settings\Module as BaseModule;
use Yii;

/**
 * settings module definition class
 */
class Module extends BaseModule implements IAdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\settings\controllers';

    /**
     * @return array
     */
    public static function getAdminMenu()
    {
        return [
            'url' => ['/settings'],
            'icon' => 'cogs',
            'label' => Yii::t('settings', 'Settings'),
        ];
    }
}
