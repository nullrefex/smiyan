<?php
namespace app\modules\blog\models;

use app\behaviors\HasLanguage;
use nullref\blog\models\Post as BasePost;

/**
 * Class Post
 * @package app\modules\blog\models
 *
 *
 * @method getLanguageTitle
 * @property $languageTitle
 * @property $pictures
 *
 */
class Post extends BasePost
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['lang', 'required'];
        $rules[] = ['pictures', 'safe'];
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'hasLanguage' => [
                'class' => HasLanguage::className(),
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['lang'] = \Yii::t('blog', 'Language');
        $labels['pictures'] = \Yii::t('blog', 'Pictures');

        return $labels;
    }

    public function init()
    {
        $this->short_text = '-';
        parent::init();
    }

    public function load($data, $formName = null)
    {
//        $this->pictures = unserialize($this->pictures);
        return parent::load($data, $formName);
    }

    public function beforeSave($insert)
    {
        $this->pictures = serialize($this->pictures);
        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $this->pictures = unserialize($this->pictures);

        parent::afterFind();
    }
}