<?php
namespace app\modules\blog\models;

use app\helpers\Languages;
use nullref\blog\models\PostSearch as BasePostSearch;
use yii\data\ActiveDataProvider;

class PostSearch extends BasePostSearch
{
    public $created_start_date;
    public $created_end_date;
    public $updated_start_date;
    public $updated_end_date;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['lang', 'integer'];
        $rules[] = [['created_start_date', 'created_end_date', 'updated_start_date', 'updated_end_date'], 'safe'];

        return $rules;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Post::find();
        $query->addOrderBy(['created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 6,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'lang' => $this->lang,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'short_text', $this->short_text])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'data', $this->data])
            ->andFilterWhere(['>=', 'created_at', $this->created_start_date ? strtotime($this->created_start_date) : null])
            ->andFilterWhere(['<=', 'created_at', $this->created_end_date ? strtotime($this->created_end_date) : null])
            ->andFilterWhere(['>=', 'updated_at', $this->updated_start_date ? strtotime($this->updated_start_date) : null])
            ->andFilterWhere(['<=', 'updated_at', $this->updated_end_date ? strtotime($this->updated_end_date) : null]);

        return $dataProvider;
    }
}