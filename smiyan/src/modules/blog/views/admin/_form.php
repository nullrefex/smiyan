<?php

use app\helpers\Helper;
use app\helpers\Languages;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\InputFile;
use nullref\blog\models\Post;
use unclead\multipleinput\MultipleInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \nullref\blog\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'picture')->widget(InputFile::className(), Helper::getInputFileOptions()) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList(Post::getStatuses()) ?>

            <?= $form->field($model, 'lang')->dropDownList(Languages::getMap()) ?>
        </div>
        <?php if ($model->picture): ?>
            <div class="col-md-3">
                <?= Html::a(Html::img($model->picture), $model->picture, [
                    'target' => '_blank', 'class' => 'thumbnail']) ?>
            </div>
            <div class="clearfix"></div>

        <?php endif ?>
    </div>

    <?= $form->field($model, 'text')->widget(CKEditor::className()) ?>


<?=
 $form->field($model, 'pictures')->widget(MultipleInput::className(), [
    'columns' => [
        [
            'name' => 'alt',
            'title' => Yii::t('app', 'Alt'),
        ],
        [
            'name' => 'image',
            'title' => Yii::t('app', 'Image'),
            'type' => InputFile::className(),
            'options' => Helper::getInputFileOptions(),
        ]
    ]
]);
?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('blog', 'Create') : Yii::t('blog', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
