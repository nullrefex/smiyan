<?php
/** @var $this \yii\web\View */
use app\helpers\Image;
use app\modules\blog\widgets\OtherActivities;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/** @var $model \app\modules\blog\models\Post */
if (!$this->title) {
    $this->title = $model->title;
}
Yii::$app->view->registerMetaTag([
    'property' => 'og:image',
    'content' => Url::to(Image::getThumbnail($model->picture, 500, 400), true),
]);

$description = trim(StringHelper::truncateWords(strip_tags($model->text), 100));
Yii::$app->view->registerMetaTag([
    'property' => 'og:description',
    'content' => $description,
]);
$shareHref = 'https://www.facebook.com/dialog/share?app_id=269450823532589&href=' . Url::to(['/blog/post/' . $model->slug], true) . '&picture={picture}';

$this->registerJs(<<<JS
jQuery(".share-fb-btn").on('click', function (e) {
    var picture = jQuery('.slider .images ').find('.image:visible').attr('data-thumb');
    var href = '$shareHref'.replace('{picture}', picture);
    window.open(href, 'Facebook', 'width=640,height=580');
    e.preventDefault(e);
    return false;
});
JS
);

?>
<section class="type-10">
    <div class="wrapper">
        <div class="row type-1">
            <div class="column">
                <a class="link type-1 var-b" href="<?= Url::to(['/blog']) ?>">
                    <img class="left" src="/img/svg/arrow_grey.svg" alt="Arrow">
                    <?= Yii::t('app', 'Назад к активностям') ?>
                </a>
            </div>
        </div>

        <div class="row type-2">
            <div class="el-1 column">
                <div class="el-2">
                    <div class="title type-2 var-b flex inline middle">
                        <div class="lines flex">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>

                        <span><?= Yii::$app->formatter->asDatetime($model->created_at, "php:d.m.Y") ?></span>

                        <div class="lines reversed flex">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                </div>

                <div class="el-3">
                    <div class="title type-1 var-b">
                        <img src="/img/smear_red_1.png" alt="Smear">

                        <?= $model->title ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row type-3">
            <div class="column">
                <div class="slider type-1">
                    <div class="images">
                        <?php foreach ($model->pictures as $picture): ?>
                            <div class="image"
                                 data-thumb="<?= Url::to(Image::getThumbnail($picture['image'], 300, 200), true) ?>"
                                 style="background-image: url(<?= Image::getThumbnail($picture['image'], 1440, 2000) ?>);"></div>
                        <?php endforeach; ?>
                    </div>

                    <div class="controls flex space-between">
                        <div class="left">
                            <img src="/img/svg/arrow_black.svg" alt="Arrow">
                        </div>
                        <div class="right">
                            <img src="/img/svg/arrow_black.svg" alt="Arrow">
                        </div>
                    </div>

                    <div class="sharing">
                        <div class="main-icon flex middle center">
                            <img src="/img/sharing_icon.png" alt="Sharing">
                        </div>

                        <div class="another-icons">
                            <a class="share-btn share-fb-btn"
                               href="https://www.facebook.com/sharer/sharer.php?app_id=269450823532589&sdk=joey&u=<?= Url::to(['/blog/post/' .   $model->slug], true) ?>&display=popup&ref=plugin&src=share_button">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>

                            <a target="_blank" href="https://twitter.com/intent/tweet?text=<?= Url::to(['/blog/post/' .   $model->slug], true) ?>">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="slider type-2">
                    <ul>
                        <?php foreach ($model->pictures as $picture): ?>
                            <li style="background-image: url(<?= $picture['image'] ?>);"></li>
                        <?php endforeach; ?>
                    </ul>

                    <div class="controls flex space-between">
                        <div class="left">
                            <img src="/img/svg/arrow_black.svg" alt="Arrow">
                        </div>
                        <div class="right">
                            <img src="/img/svg/arrow_black.svg" alt="Arrow">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row type-4">
            <div class="column">
                <div class="content var-b">
                    <?= $model->text ?>
                </div>
            </div>
        </div>

        <div class="row type-4">
            <div class="column">
                <div class="el-4 lines flex">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>

                <div class="sharing flex middle">
                    <div class="el-5 text text-4 var-c"><?= Yii::t('app', 'Поделиться') ?></div>

                    <div class="icons-outer">
                        <a class="share-btn share-fb-btn"
                           href="https://www.facebook.com/sharer/sharer.php?app_id=269450823532589&sdk=joey&u=<?= Url::to(['/blog/post/' . $model->slug], true) ?>&display=popup&ref=plugin&src=share_button">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                        <a target="_blank" href="https://twitter.com/intent/tweet?text=<?= Url::to(['/blog/post/' .   $model->slug], true) ?>">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
</section>

<section class="type-9 var-b">
    <img class="smear-bottom" src="/img/smear_bottom.png" alt="Smear">

    <div class="wrapper">
        <div class="row type-1">
            <div class="el-1 column">
                <div class="el-2">
                    <div class="title type-2 var-b flex inline middle">
                        <div class="lines flex">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>

                        <span><?= Yii::t('app', 'Новости о нас') ?></span>

                        <div class="lines reversed flex">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                </div>

                <div class="el-3">
                    <div class="title type-1 var-b">
                        <img src="/img/smear_red_1.png" alt="Smear">

                        <?= Yii::t('app', 'Другие активности') ?>
                    </div>
                </div>
            </div>
        </div>

        <?= OtherActivities::widget(['currentId' => $model->id]) ?>

        <div class="row type-3">
            <div class="column el-5">
                <a class="link type-1 var-a" href="<?= Url::to(['/blog']) ?>">
                    <img class="left" src="/img/svg/arrow_red.svg" alt="Arrow">
                    <?= Yii::t('app', 'Назад к активностям') ?>
                </a>
            </div>
        </div>
</section>