<?php

use mranger\load_more_pager\LoadMorePager;
use yii\helpers\Url;
use yii\widgets\ListView;

/** @var $this \yii\web\View */
/** @var $dataProvider \yii\data\ActiveDataProvider */
if (!$this->title) {
    $this->title = Yii::t('blog', 'Активности');
}
?>

<section class="type-9">
    <div class="wrapper">
        <div class="row type-1">
            <div class="el-1 column">
                <div class="el-2">
                    <div class="title type-2 var-b flex inline middle">
                        <div class="lines flex">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>

                        <span><?= Yii::t('blog', 'Новости о нас') ?></span>

                        <div class="lines reversed flex">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                </div>

                <div class="el-3">
                    <div class="title type-1 var-b">
                        <img src="/img/smear_red_1.png" alt="Smear">

                        <?= Yii::t('blog', 'Активности') ?>
                    </div>
                </div>
            </div>
        </div>


        <?= ListView::widget([
                'id' => 'activities',
                'dataProvider' => $dataProvider,
                'emptyText' => Yii::t('blog', 'There are no posts'),
                'itemView' => '_view',
                'options' => [
                    'tag' => 'div',
                    'class' => 'row type-2',
                ],
                'itemOptions' => function ($model, $key, $index, $widget) {
                    return [
                        'tag' => 'a',
                        'class' => 'block type-1',
                        'href' => Url::to(['post/view', 'slug' => $model->slug])
                    ];
                },
                'summary' => '',
                'layout' => '<div class="column"><div class="blocks type-1 flex space-between wrap">{items}</div></div></div>{pager}',
                'pager' => [
                    'class' => LoadMorePager::className(),
                    'buttonText' => Yii::t('blog', 'Показать еще') . '<div class="refresh-outer"><i class="fa fa-refresh" aria-hidden="true"></i></div>',
                    'id' => 'activities-pagination',
                    'options' => [
                        'class' => 'el-6 link type-1 var-a',
                    ],
                    'contentSelector' => '#activities',
                    'contentItemSelector' => '.block.type-1',
                    'template' => '<div class="row type-3"><div class="column el-5">{button}</div></div>',
                    'loaderTemplate' => '',
                ]
            ]
        )
        ?>
</section>