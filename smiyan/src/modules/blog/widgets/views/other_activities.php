<?php
use yii\helpers\Url;
?>
<div class="row type-2">
    <div class="column">
        <div class="blocks type-1 flex space-between wrap">
            <?php /** @var \app\modules\blog\models\Post[] $posts */
            foreach ($posts as $post): ?>
            <a class="block type-1" href="<?= Url::to(['post/view', 'slug' => $post->slug]) ?>">
                <div class="image-outer">
                    <div class="frame"></div>
                    <img class="image" style="background-image: url(<?= $post->picture ?>);">
                </div>
                <div class="el-4 text text-4 var-b"><?= Yii::$app->formatter->asDatetime($post->created_at, "php:d.m.Y") ?></div>
                <div class="title type-4 var-a"><?= $post->title?></div>
            </a>
            <?php endforeach; ?>
        </div>
    </div>
</div>