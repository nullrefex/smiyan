<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\product\models;


class ProductForm extends Product
{
    /**
     * @param $data
     * @return self
     */
    public static function createNew($data)
    {
        $model = new self();

        $model->load($data, '');

        return $model;
    }

    /**
     * @return array
     */
    public function getRedirect()
    {
        switch ($this->type) {
            case self::TYPE_VARIANT:
                return ['view', 'id' => $this->product_id];
            default:
                return ['view', 'id' => $this->id];
        }
    }
}