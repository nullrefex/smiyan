<?php

namespace app\modules\product\models;

use app\helpers\Languages;
use app\modules\category\models\Category;
use nullref\useful\behaviors\JsonBehavior;
use nullref\useful\behaviors\TranslationBehavior;
use Yii;
use yii\base\UserException;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $image
 * @property int $type
 * @property string $options
 * @property string $identifier
 * @property string $price
 * @property int $category_id
 * @property int $product_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $show_on_index
 *
 * @property Category $category
 * @property Product $product
 * @property Product[] $variants
 */
class Product extends ActiveRecord
{
    /**
     * Product types
     */
    const TYPE_SINGLE = 'single';
    const TYPE_VARIOUS = 'various';
    const TYPE_VARIANT = 'variant';
    const TYPE_SOUVENIR = 'souvenir';


    /**
     * Various product sizes
     */
    const SIZE_FULL = 'full';
    const SIZE_HALF = 'half';
    const SIZE_QUARTER = 'quarter';

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_SINGLE => Yii::t('product', 'Single'),
            self::TYPE_VARIOUS => Yii::t('product', 'Various'),
            self::TYPE_VARIANT => Yii::t('product', 'Variant'),
            self::TYPE_SOUVENIR => Yii::t('product', 'Souvenir'),
        ];
    }

    /**
     * @return array
     */
    public static function getVariousSizes()
    {
        return [
            self::SIZE_FULL => Yii::t('product', '100%'),
            self::SIZE_HALF => Yii::t('product', '50%'),
            self::SIZE_QUARTER => Yii::t('product', '25%'),
        ];
    }

    /**
     * @return Product[]
     */
    public function getNeighbors()
    {
        $prevId = Product::find()->andWhere(['<', 'id', $this->id])->andWhere(['type' => $this->type])->max('id');
        $nextId = Product::find()->andWhere(['>', 'id', $this->id])->andWhere(['type' => $this->type])->min('id');
        return [
            'prev' => $prevId ? Product::findOne($prevId) : null,
            'next' => $nextId ? Product::findOne($nextId) : null,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * @return array|\string[]
     */
    public function safeAttributes()
    {
        return $this->addMultilangFileds(parent::safeAttributes());
    }

    /**
     * @return array
     */
    public function activeAttributes()
    {
        return $this->addMultilangFileds(parent::activeAttributes());
    }

    /**
     * @param $attributes
     * @return array
     */
    protected function addMultilangFileds($attributes)
    {
        $result = $attributes;
        foreach ($attributes as $item) {
            if (in_array($item, ['name', 'description'])) {
                foreach (Languages::getSlugMap() as $id => $lang) {
                    $result[] = $item . '_' . $lang;
                }
            }
        }
        return $result;
    }

    /**
     * @return int|string
     */
    public function getScenario()
    {
        if ($this->type && in_array($this->type, array_keys(self::getTypes()))) {
            return $this->type;
        }
        return parent::getScenario();
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'product_id', 'created_at', 'updated_at', 'show_on_index'], 'integer'],
            [['options'], 'safe'],
            [['price'], 'number', 'min' => 0],
            [['name', 'image', 'identifier'], 'string', 'max' => 255],
            [['identifier'], 'unique'],
            [['type', 'name', 'description', 'price', 'image'], 'required'],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::TYPE_SINGLE => ['identifier', 'type', 'name', 'price', 'image', 'category_id', 'description', 'options'],
            self::TYPE_VARIOUS => ['identifier', 'type', 'name', 'price', 'image', 'category_id', 'description', 'show_on_index'],
            self::TYPE_VARIANT => ['identifier', 'type', 'price', 'options', 'product_id'],
            self::TYPE_SOUVENIR => ['identifier', 'type', 'name', 'image', 'price', 'options', 'description'],
        ]);
    }

    /**
     * @return mixed
     */
    public function getTypeTitle()
    {
        return self::getTypes()[$this->type];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('product', 'ID'),
            'name' => Yii::t('product', 'Name'),
            'image' => Yii::t('product', 'Image'),
            'type' => Yii::t('product', 'Type'),
            'options' => Yii::t('product', 'Options'),
            'description' => Yii::t('product', 'Description'),
            'identifier' => Yii::t('product', 'Identifier'),
            'price' => Yii::t('product', 'Price'),
            'category' => Yii::t('product', 'Category'),
            'category_id' => Yii::t('product', 'Category'),
            'created_at' => Yii::t('product', 'Created At'),
            'updated_at' => Yii::t('product', 'Updated At'),
            'show_on_index' => Yii::t('product', 'Show On Index Page'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => 'updated_at',
                'createdAtAttribute' => 'created_at'
            ],
            'slug' => [
                'class' => SluggableBehavior::class,
                'attribute' => 'slugName',
                'slugAttribute' => 'identifier',
                'ensureUnique' => true,
                'skipOnEmpty' => true,
            ],
            'json' => [
                'class' => JsonBehavior::class,
                'fields' => ['options'],
            ],
            'multilanguage' => [
                'class' => TranslationBehavior::className(),
                'languages' => Languages::getSlugMap(),
                'defaultLanguage' => Languages::get()->getSlug(),
                'relation' => 'translations',
                'attributeNamePattern' => '{attr}_{lang}',
                'languageField' => 'language',
                'langClassName' => ProductTranslation::className(),
                'translationAttributes' => [
                    'name', 'description',
                ],
            ],
        ];
    }

    public function getSlugName()
    {
        if ($this->identifier) {
            return $this->identifier;
        }
        $tr = array(
            "А" => "a", "Б" => "b", "В" => "v", "Г" => "g", "Д" => "d",
            "Е" => "e", "Ё" => "yo", "Ж" => "zh", "З" => "z", "И" => "i",
            "Й" => "j", "К" => "k", "Л" => "l", "М" => "m", "Н" => "n",
            "О" => "o", "П" => "p", "Р" => "r", "С" => "s", "Т" => "t",
            "У" => "u", "Ф" => "f", "Х" => "kh", "Ц" => "ts", "Ч" => "ch",
            "Ш" => "sh", "Щ" => "sch", "Ъ" => "", "Ы" => "y", "Ь" => "",
            "Э" => "e", "Ю" => "yu", "Я" => "ya", "а" => "a", "б" => "b",
            "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "yo",
            "ж" => "zh", "з" => "z", "и" => "i", "й" => "j", "к" => "k",
            "л" => "l", "м" => "m", "н" => "n", "о" => "o", "п" => "p",
            "р" => "r", "с" => "s", "т" => "t", "у" => "u", "ф" => "f",
            "х" => "kh", "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch",
            "ъ" => "", "ы" => "y", "ь" => "", "э" => "e", "ю" => "yu",
            "я" => "ya", " " => "-", "." => "", "," => "", "/" => "-",
            ":" => "", ";" => "", "—" => "", "–" => "-"
        );
        return strtr($this->name_ru, $tr);
    }

    /**
     * @inheritdoc
     * @return ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(self::class, ['id' => 'product_id']);
    }

    /**
     * @return bool
     */
    public function isVarious()
    {
        return $this->type === self::TYPE_VARIOUS;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariants()
    {
        return $this->hasMany(self::class, ['product_id' => 'id']);
    }

    /**
     * @return bool
     */
    public function isSingle()
    {
        return $this->type === self::TYPE_SINGLE;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductTranslation::className(), ['product_id' => 'id']);
    }

    /**
     * @param array $params
     * @return ProductCartPosition
     */
    public function getCartPosition($params = [])
    {
        return \Yii::createObject([
            'class' => 'app\modules\product\models\ProductCartPosition',
            'product' => $this,
        ]);
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getId()
    {
        return $this->id;
    }

    public static function findByIdentifier($identifier)
    {
        if ($model = self::find()->where(['identifier' => $identifier])->one()) {
            return $model;
        }
        return false;
    }

    public function getSizeIcon()
    {
        if ($this->type == self::TYPE_VARIANT) {
            $options = $this->options;
            switch ($options['size']) {
                case self::SIZE_FULL:
                    return 3;
                case self::SIZE_HALF:
                    return 2;
                case self::SIZE_QUARTER:
                    return 1;
            }
        }
        throw new UserException('Product type is not variant.');
    }

    /**
     * @return mixed
     */
    public function getSizeTitle()
    {
        return self::getVariousSizes()[$this->options['size']];
    }

    public function getName()
    {
        if ($this->type == self::TYPE_VARIANT) {
            $product = $this->product;
            return $product->{'name_' . Yii::$app->language};
        }
        return $this->{'name_' . Yii::$app->language};
    }

    public function isAttributeChanged($name, $identical = true)
    {
        if ($name == 'slugName') {
            return true;
        }
        return parent::isAttributeChanged($name, $identical);
    }
}
