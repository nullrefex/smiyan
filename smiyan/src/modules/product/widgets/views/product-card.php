<?php

/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $this \yii\web\View
 * @var $model \app\modules\product\models\Product
 */
use app\helpers\Image;
use yii\helpers\Url;

?>

<?php if ($model->isVarious()): ?>
    <div class="item type-2 flex direction-column justify-end">
        <div class="image">
            <img src="<?= Image::getThumbnail($model->image,260, 600) ?>" alt="<?= $model->name ?>" class="mCS_img_loaded">
        </div>

        <div class="text-1">
            <?= $model->name ?>
        </div>

        <div class="text-2">
            <?= Yii::t('app', 'от {price} ₴', ['price' => Yii::$app->formatter->asInteger($model->price)]) ?>
        </div>
        <div class="action flex middle center" data-id="<?= $model->id ?>">
            <a class="button type-1 add-to-cart"
               href="<?= Url::to(['/confectionery/product', 'id' => $model->identifier]) ?>">
                <?= Yii::t('app', 'To Product') ?>
            </a>
        </div>
    </div>
<?php elseif ($model->isSingle()): ?>
    <div class="item type-2 flex direction-column justify-end">
        <a class="info-icon flex middle center"
           href="<?= Url::to(['/confectionery/product', 'id' => $model->identifier]) ?>">
            <span>i</span>
        </a>

        <div class="image">
            <img src="<?= Image::getThumbnail($model->image,260, 600) ?>" alt="<?= $model->name ?>" class="mCS_img_loaded">
        </div>

        <div class="text-1">
            <?= $model->name ?>
        </div>

        <div class="text-2 price" data-price="<?= Yii::t('app', '{price}', [
            'price' => Yii::$app->formatter->asInteger($model->price)]) ?>">
            <?= Yii::t('app', '<span>{price}</span> ₴', [
                'price' => Yii::$app->formatter->asInteger($model->price)]) ?>
        </div>

        <div class="action flex middle center" data-id="<?= $model->id ?>">
            <div class="counter flex middle">
                <div class="minus button type-2"></div>
                <div class="variant-total text text-3 var-a">0</div>
                <div class="plus button type-2"></div>
            </div>

            <a class="button type-1 add-to-cart" data-id="<?= $model->id ?>"><?= Yii::t('app', 'В корзину') ?></a>
        </div>
    </div>
<?php endif ?>
