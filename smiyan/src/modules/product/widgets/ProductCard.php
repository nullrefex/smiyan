<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\product\widgets;


use yii\base\InvalidConfigException;
use yii\base\Widget;

class ProductCard extends Widget
{
    public $model;

    public function init()
    {
        if ($this->model === null) {
            throw new InvalidConfigException('$model must be set');
        }
    }

    public function run()
    {
        return $this->render('product-card', [
            'model' => $this->model
        ]);
    }
}