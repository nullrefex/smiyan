<?php

use app\modules\product\widgets\ProductVariants;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\product\models\Product */

?>

<?= ProductVariants::widget(['model' => $model]) ?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'name',
        'image:image',
        'price',
        'created_at:datetime',
        'updated_at:datetime',
    ],
]) ?>