<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $this \yii\web\View
 * @var $model \app\modules\product\models\ProductForm
 * @var $form \yii\widgets\ActiveForm
 *
 */
use app\modules\product\models\Product;

?>

<?= $form->field($model, 'price')->textInput() ?>

<?= $form->field($model, 'options[size]')->dropDownList(Product::getVariousSizes())->label(Yii::t('product', 'Size')) ?>

<?= $form->field($model, 'options[weight]')->textInput()->label(Yii::t('product', 'Weight')) ?>