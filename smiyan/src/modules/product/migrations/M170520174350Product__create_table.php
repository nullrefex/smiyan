<?php

namespace app\modules\product\migrations;

use yii\db\Migration;

class M170520174350Product__create_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'image' => $this->string(),
            'type' => $this->string(),
            'options' => $this->text(),
            'identifier' => $this->string()->unique(),
            'price' => $this->decimal(10, 2),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%product}}');
    }
}
