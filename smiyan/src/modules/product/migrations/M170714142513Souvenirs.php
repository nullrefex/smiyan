<?php

namespace app\modules\product\migrations;

use app\modules\product\models\Product;
use app\modules\product\models\ProductTranslation;
use Yii;
use yii\db\Migration;

class M170714142513Souvenirs extends Migration
{
    public function up()
    {
        $this->insert(Product::tableName(), [
            'identifier' => 'kniga-desert',
            'type' => 'souvenir',
            'created_at' => '1500039439',
            'updated_at' => '1500039439',
            'price' => '255',
            'image' => '/img/product_1.jpg',
        ]);
        $firstId = Yii::$app->db->getLastInsertID();
        $this->insert(Product::tableName(), [
            'identifier' => 'kniga-desert-2',
            'type' => 'souvenir',
            'created_at' => '1500039439',
            'updated_at' => '1500039439',
            'price' => '255',
            'image' => '/img/product_1.jpg',
        ]);
        $secondId = Yii::$app->db->getLastInsertID();
        $this->batchInsert(ProductTranslation::tableName(), ['product_id', 'language', 'name'] ,[
            [$firstId, '1', 'Книга'],
            [$firstId, '2', 'Книга'],
            [$firstId, '3', 'Book'],
            [$secondId, '1', 'Книга'],
            [$secondId, '2', 'Книга'],
            [$secondId, '3', 'Book'],
        ]);
    }

    public function down()
    {
        return true;
    }
}
