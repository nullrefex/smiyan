<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\controllers;


use app\components\PageMetaTagsFilter;
use app\modules\category\models\Category;
use app\modules\product\models\Product;
use yii\web\Controller;

class ConfectioneryController extends Controller
{
    public function behaviors()
    {
        return [
            'seo' => [
                'class' => PageMetaTagsFilter::class,
            ],
        ];
    }

    public function actionIndex()
    {
        $categories = Category::find()
            ->innerJoinWith('products')
            ->all();

        return $this->render('index', [
            'categories' => $categories,
        ]);
    }

    public function actionProduct($id)
    {
        $model = Product::findByIdentifier($id);

        if ($model && $model->type != $model::TYPE_SOUVENIR) {
            return $this->render('product', [
                'model' => $model,
            ]);
        }
        return $this->goHome();
    }
}