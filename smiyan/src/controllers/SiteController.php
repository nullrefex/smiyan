<?php

namespace app\controllers;

use app\components\PageMetaTagsFilter;
use app\modules\order\helpers\Mail;
use app\modules\order\models\Order;
use app\modules\product\models\Product;
use app\traits\CartTrait;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;


class SiteController extends Controller
{
    use CartTrait;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'seo' => [
                'class' => PageMetaTagsFilter::class,
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $popup = '';
        if ($orderId = Yii::$app->request->get('order')) {
            $order = Order::findByLpOrderId($orderId);
            if ($order && $order->popup_shown == 0 && $order->status == $order::STATUS_PAID) {
                $popup = Yii::t('app', 'Спасибо за покупку!');
                $order->popup_shown = true;
                $this->cart->removeAll();
                $order->save(false);
                Mail::sendPaidOrderMail($order);
                Mail::sendNewOrderCustomerMail($order, $order->customer_email);
            }
        }

        return $this->render('index', [
            'popup' => $popup
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionContacts()
    {
        return $this->render('contacts');
    }

    public function actionDostavka()
    {
        return $this->render('dostavka');
    }

    public function actionKlientam()
    {
        return $this->render('klientam');
    }

    public function actionZakazTorta()
    {
        return $this->render('zakaz-torta');
    }

    public function actionSouvenirs()
    {
        $query = Product::find()->where(['type' => Product::TYPE_SOUVENIR]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => false,
            ],
        ]);

        return $this->render('souvenirs', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionSouvenir($id)
    {
        $model = Product::findByIdentifier($id);

        if ($model && $model->type == $model::TYPE_SOUVENIR) {
            return $this->render('souvenir', [
                'model' => $model,
            ]);
        }
        return $this->goHome();
    }
}
