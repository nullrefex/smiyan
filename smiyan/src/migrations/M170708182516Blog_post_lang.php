<?php
namespace app\migrations;

use yii\db\Migration;

class M170708182516Blog_post_lang extends Migration
{
    public function up()
    {
        $this->addColumn('{{%blog_post}}', 'lang', $this->smallInteger());
        $this->addColumn('{{%blog_post}}', 'pictures', $this->text());
    }

    public function down()
    {
        $this->dropColumn('{{%blog_post}}', 'lang');
        $this->dropColumn('{{%blog_post}}', 'pictures');

        return true;
    }
}
