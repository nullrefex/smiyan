<?php

return \yii\helpers\ArrayHelper::merge(require(__DIR__ . '/installed_modules.php'), [
    'core' => [
        'class' => nullref\core\Module::class,
    ],
    'product' => [
        'class' => app\modules\product\Module::class,
    ],
    'settings' => [
        'class' => app\modules\settings\Module::class,
    ],
    'shop' => [
        'class' => app\modules\shop\Module::class,
    ],
    'admin' => [
        'controllerNamespace' => '\app\modules\admin\controllers',
    ],
    'blog' => [
        'class' => 'nullref\blog\Module',
        'controllerMap' => [
            'post' => app\modules\blog\controllers\PostController::class,
        ],
        'classMap' => [
            'Post' => app\modules\blog\models\Post::class,
            'PostSearch' => app\modules\blog\models\PostSearch::class,
        ],
    ],
    'category' => [
        'class' => \app\modules\category\Module::class,
        'classMap' => [
            'Category' => \app\modules\category\models\Category::class,
            'CategoryQuery' => \app\modules\category\models\CategoryQuery::class,
        ],
    ],
    'reviews' => [
        'class' => \app\modules\reviews\Module::class,
    ],
    'order' => [
        'class' => \app\modules\order\Module::class,
    ],
    'cms' => [
        'class' => app\modules\cms\Module::class,
        'classMap' => [
            'Page' => app\modules\cms\models\Page::class,
        ],
        'components' => [
            'blockManager' => [
                'class' => app\modules\cms\components\BlockManager::class,
            ],
        ],
    ],
]);