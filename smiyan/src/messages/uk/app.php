<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    ' ₴ доставка' => '',
    '<span>{price}</span> ₴' => '',
    'Alt' => '',
    'Are you sure you want to delete this item?' => '',
    'Browse' => '',
    'Image' => '',
    'NA' => '',
    'Please, add category' => '',
    '{price}' => '',
    'Назад к сувенирам' => 'Назад до сувенірів',
    'Спасибо за покупку!' => '',
    'Спасибо за покупку! Номер вашего заказа: #{order_id}' => 'Дякую за покупку! Номер вашого замовлення: #{order_id}',
    'Cart is empty.' => 'Кошик порожнiй',
    'Discount activated' => 'Знижка активована',
    'How we are working' => 'Як ми працюємо',
    'Our best' => 'Наша найкраща',
    'To Product' => 'Замовити',
    'Total: ' => 'Всього: ',
    'discount' => 'знижка',
    'Активности' => 'Активності',
    'Будем на связи' => 'Будемо на зв\'язку',
    'В кондитерскую' => 'У кондитерську',
    'В корзину' => 'У кошик',
    'Видео' => 'Відео',
    'Всего:' => 'Усього:',
    'Десерты' => 'Десерти',
    'Для приготовления всех наших десертов отбираем только натуральные ингридиенты от проверенных поставщиков.' => 'Для приготування всіх наших десертів відбираємо тільки натуральні інгредієнти від перевірених постачальників.',
    'Для приготовления их используются настоящая фисташковая паста, пюре манго, ванильная палочка и миндальная мука.' => 'Для приготування ми використовуємо справжню фісташкову пасту, пюре манго, ванільна паличка і мигдальне борошно.',
    'Другие активности' => 'Інші активності',
    'Заказать' => 'Замовити',
    'Кондитерская' => 'Кондитерська',
    'Контакты' => 'Контакти',
    'Корзина' => 'Кошик',
    'Лучшие десерты <br> в городе' => 'Кращі десерти <br> країни',
    'Мы любим наших гостей и воплощаем эту любовь в наших десертах и тортах!' => 'Ми любимо наших гостей і втілюємо цю любов у наших десертах і тортах!',
    'Мы не используем ароматизаторы, консерванты, стабилизаторы и красители.' => 'Ми не використовуємо ароматизатори, консерванти, стабілізатори та барвники.',
    'На карте' => 'На карті',
    'Назад в кондитерскую' => 'Назад у кондитерську',
    'Назад к активностям' => 'Назад до активностей',
    'Наш девиз — делать все самое вкусное из прошлого, <br> настоящего и будущего!' => 'Наш девіз - Все найсмачніше з минулого, <br> сьогодення і майбутнього!',
    'Новости о нас' => 'Новини про нас',
    'О кондитерском доме' => 'Про кондитерський дім',
    'О нас' => 'Про нас',
    'Оплата замовлення' => 'Оплата замовлення',
    'Отзывы' => 'Відгуки',
    'Поделиться' => 'Поділитись',
    'Поздравляем! <br> Скидка активирована.' => 'Вітаємо! <br> Знижка активована.',
    'Помни и кайфуй' => 'Памя\'тай та кайфуй',
    'Популярные десерты' => 'Популярні десерти',
    'СМІЯН Все права защищены' => 'СМІЯН Всі права захищені',
    'Самые' => 'Сміян',
    'Стараясь угодить вкусам и предпочтениям наших гостей, мы каждый день развиваемся и становимся лучше! Если завтра мы поймем, что вы хотите чего-то абсолютно нового, мы быстро и легко научимся это готовить!' => 'Намагаючись догодити смакам і уподобанням наших гостей, ми кожен день розвиваємося і стаємо кращими! Якщо завтра ми зрозуміємо, що ви хочете чогось абсолютно нового, ми швидко і легко навчимося це готувати!',
    'Сувениры' => 'Сувеніри',
    'г' => 'г',
    'от {price} ₴' => 'від {price} ₴',
];
