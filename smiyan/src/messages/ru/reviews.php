<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'ID' => '',
    'Reset' => 'Сбросить',
    'Search' => 'Поиск',
    'Update' => 'Обновить',
    'Approved' => 'Подтвержденный',
    'Are you sure you want to delete this item?' => 'Удалить?',
    'Choose approved' => 'Сделайте выбор',
    'Choose rating' => 'Сделайте выбор',
    'Choose shop' => 'Сделайте выбор',
    'Contacts' => 'Контакты',
    'Create' => 'Добавить',
    'Create Reviews' => 'Добавить отзыв',
    'Created At' => 'Создано',
    'Customer Name' => 'Имя пользователя',
    'Delete' => 'Удалить',
    'List' => 'Список',
    'Message' => 'Сообщение',
    'Not Approved' => 'Не подтвержден',
    'Rating' => 'Рейтинг',
    'Reviews' => 'Отзывы',
    'Shop' => 'Магазин',
    'Shop ID' => 'Магазин',
    'Updated At' => 'Обновлено',
    'Выберите ресторан' => 'Выбирите ресторан',
    'Емейл должен быть в формате example@email.com, телефон - +380XXXXXXXXX.' => 'Емейл должен быть в формате example@email.com, телефон - +380XXXXXXXXX.',
    'Имя' => 'Имя',
    'О нас говорят' => 'О нас говорят',
    'Оставить отзыв' => 'Оставить отзыв',
    'Отзывы' => 'Отзывы',
    'Отправить' => 'Отправить',
    'Оцените обслуживание' => 'Оцените обслуживание',
    'Похвали нас' => 'Похвали нас',
    'Почта или телефон' => 'Почта или телефон',
    'Сообщение' => 'Сообщение',
    'Спасибо за Ваш отзыв!' => 'Спасибо за Ваш отзыв!',
    'Что то пошло не так :(' => 'Что то пошло не так :(',
    'Language' => 'Язык',
];
