<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\widgets;


use yii\base\Widget;

class About extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('about');
    }
}