<?php
/** @var \app\modules\product\models\Product $product */
/** @var \app\modules\product\models\ProductCartPosition $model */
$product = $model->getProduct();
?>
<div class="name text text-3 var-b">
    <?= $product->getName() ?>
</div>

<div class="info flex space-between middle wrap">
    <div class="icon">
        <?php if ($product->type == $product::TYPE_VARIANT): ?>
            <img src="/img/svg/part_<?= $product->getSizeIcon() ?>.svg" alt="Part">
        <?php endif; ?>
    </div>
    <div class="weight text text-2 var-d">
        <?php if ($product->type == $product::TYPE_VARIANT): ?>
            <?= $product->options['weight'] ?> г
        <?php endif; ?>
    </div>

    <div class="price title type-4 var-a" data-price="<?= Yii::$app->formatter->asInteger($model->getPrice()) ?>">
        <?= Yii::$app->formatter->asInteger($model->getPrice()) ?> ₴
    </div>

    <div class="counter flex middle">
        <div class="minus button type-2"></div>
        <div class="variant-total text text-3 var-a"><?= $model->getQuantity() ?></div>
        <div class="plus button type-2"></div>
    </div>

    <div class="remove flex direction-column space-between remove-from-cart" data-id="<?= $model->id ?>"></div>
</div>