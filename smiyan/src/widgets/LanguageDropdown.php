<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */

namespace app\widgets;

use app\helpers\Languages;
use Yii;
use yii\base\Widget;
use yii\bootstrap\Html;
use yii\helpers\Url;

class LanguageDropdown extends Widget
{
    private $_isError;
    public $languages = [];

    public function init()
    {
        $route = Yii::$app->controller->route;
        $params = $_GET;
        $this->_isError = $route === Yii::$app->errorHandler->errorAction;
        array_unshift($params, '/' . $route);
        $currentSlug = Languages::get()->getSlug();
        foreach (Languages::getSlugedMap() as $language) {
            if ($language->getSlug() === $currentSlug) {
                continue;
            }
            $title = Languages::getShortNames()[$language->getSlug()];
            $this->languages[] = [
                'label' => $title,
                'url' => '/' . $language->getSlug() . Url::current(),
            ];
        }
        parent::init();
    }

    public function run()
    {
        if ($this->_isError) {
            return '';
        } else {
            return $this->render('language_dropdown', [
                'languages' => $this->languages,
            ]);
        }
    }
}