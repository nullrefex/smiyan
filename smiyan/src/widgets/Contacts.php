<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\widgets;


use app\modules\shop\models\Shop;
use Yii;
use yii\base\Widget;

class Contacts extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        $shops = Shop::find()->all();
        $settings = Yii::$app->settings;

        return $this->render('contacts', [
            'shops' => $shops,
            'settings' => $settings
        ]);
    }

}