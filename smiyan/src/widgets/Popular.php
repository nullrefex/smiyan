<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */
namespace app\widgets;

use app\modules\product\models\Product;
use app\modules\settings\models\ProductList;
use yii\base\Widget;

class Popular extends Widget
{
    public function run()
    {
        $ids = ProductList::getList();
        $popular = Product::find()
            ->where(['id' => $ids])
            ->indexBy('id')
            ->all();

        $result = [];

        foreach ($ids as $id) {
            if (isset($popular[$id])) {
                $result[$id] = $popular[$id];
            }
        }

        return $this->render('popular', [
            'popular' => $result,
        ]);
    }

}