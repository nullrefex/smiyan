<?php

namespace app\components;

use app\traits\CartTrait;
use yz\shoppingcart\CartActionEvent;
use yz\shoppingcart\CostCalculationEvent;
use yz\shoppingcart\ShoppingCart as BaseShoppingCart;

/**
 * Class ShoppingCart
 * @package app\components
 *
 * @property $marketId
 *
 */
class ShoppingCart extends BaseShoppingCart
{
    use CartTrait;

    public function getPositionCount()
    {
        return count(self::getPositions());
    }

    /**
     * @param ProductCartPosition[] $positions
     */
    public function updateList($positions)
    {
        foreach ($positions as $position) {
            $this->update($position, $position->quantity);
        }
    }
}