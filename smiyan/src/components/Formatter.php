<?php
/**
 */

namespace app\components;


use app\modules\settings\models\DeliveryTime;
use yii\i18n\Formatter as BaseFormatter;

class Formatter extends BaseFormatter
{
    public $dateFormat = 'dd.MM.yyyy';

    public $datetimeFormat = 'HH:mm dd.MM.yyyy';

    public $timeFormat = 'HH:mm:ss';

    public function asImage($value, $options = [])
    {
        if (!isset($options['style'])) {
            $options['style'] = 'max-width: 200px';
        }
        return parent::asImage($value, $options);
    }

    public function asDeliveryTime($value)
    {
        $list = DeliveryTime::getTimeList();
        if (isset($list[$value])) {
            return $list[$value];
        }
        return '';
    }
}