<?php

namespace app\helpers;

use alexBond\thumbler\Thumbler;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */
class Image
{
    /**
     * @param $image
     * @param $width
     * @param $height
     * @param array $config
     * @return null|string
     */
    public static function getThumbnail($image, $width, $height, $config = [])
    {
        ini_set('memory_limit', '-1');
        $image = urldecode($image);
        $method = ArrayHelper::remove($config, 'method', Thumbler::METHOD_NOT_BOXED);
        $color = ArrayHelper::remove($config, 'color', 'ffffff');
        $default = ArrayHelper::remove($config, 'default', '/img/default-image.jpg');

        /** @var Thumbler $thumbler */
        $thumbler = Yii::$app->get('thumbler');
        $result = null;
        if (empty($image) || !file_exists(Yii::getAlias($thumbler->sourcePath) . DIRECTORY_SEPARATOR . $image)) {
            $image = $default;
        }
        return Yii::$app->urlManager->hostInfo . self::addThumbUrl($thumbler->resize($image, $width, $height, $method, $color));
    }

    /**
     * @param $url
     * @return string
     */
    public static function addThumbUrl($url)
    {
        return '/thumbs/' . str_replace('\\', '/', $url);
    }
}