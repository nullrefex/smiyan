<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\helpers;


use Yii;

class Helper
{
    public static function getInputFileOptions()
    {
        return [
            'buttonName' => Yii::t('app', 'Browse'),
            'language' => Yii::$app->language,
            'controller' => 'elfinder-backend',
            'filter' => 'image',
            'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
            'options' => ['class' => 'form-control'],
            'buttonOptions' => ['class' => 'btn btn-default'],
            'multiple' => false
        ];
    }
}