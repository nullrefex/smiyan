function updateCart(id, type, amount) {
    var discount = smiyan.checkDiscount();
    $.ajax({
        url: '/cart/' + type,
        data: {
            id: id,
            amount: amount,
            discount: discount
        },
        type: 'post',
        success: function (data) {
            $.pjax.reload({
                container: '#cart',
                timeout: 999999,
                data: {
                    discount: discount
                },
                type: 'post'
            });
            smiyan.updateCheckoutPrice();
            var counter = $('.basket-button span.text');
            counter.each(function () {
                $(this).html(data.count);
            });
            if (data.count) {
                $('.basket-button').addClass('not-empty');
            } else {
                $('.basket-button').removeClass('not-empty');
            }
        }
    });
}

$(document).on('click', '.add-to-cart', function () {
    var item = $(this).parents('.item');
    var total = item.find('.variant-total').html();
    var id = $(this).data('id');

    updateCart(id, 'put', total);
    item.find('.variant-total').html('0');
    var price = item.find('.text-2.price');
    price.find('span').html(price.data('price'));
})

$(document).on('click', '.remove-from-cart', function () {
    $(this).parents('.item').slideUp(500);

    var id = $(this).data('id');

    setTimeout(function () {
        updateCart(id, 'put', 0);
    }, 500);
})

function basketQuantityCalc() {
    var eventThrottle;

    $(document).on('click', '.basket-menu .counter .button', function () {
        // CALCULATIONS
        $this = $(this);

        clearTimeout(eventThrottle);

        var $calcButton = $(this),
            $varTotal = $(this).parent().find('.variant-total');

        if ($(this).hasClass('minus')) {
            if (parseInt($varTotal.text()) !== 1) {
                $varTotal.text(parseInt($varTotal.text()) - 1);
            }
        } else {
            $varTotal.text(parseInt($varTotal.text()) + 1);
        }
        // CALCULATIONS END

        var item = $(this).parents('.item');
        var count = item.find('.variant-total').html();
        eventThrottle = setTimeout(function () {
            if (count > 0) {
                var id = item.data('key');
                updateCart(id, 'update', count);
            }
        }, 1000);
    })
}

basketQuantityCalc();

$(document).on('click', '.add-various', function () {
    var info = $(this).parents('.info');
    var items = info.find('.variant-total');
    if (items.length) {
        items.each(function () {
            var variant = $(this);
            var count = variant.html();
            var id = variant.data('id');
            if (count > 0) {
                updateCart(id, 'put', count);
                variant.html(0);
                info.find('.total .number span').html(0)
            }
        });
    } else {
        var item = info.find('.number.title');
        var id = item.data('id');
        updateCart(id, 'put', 1);
    }
});