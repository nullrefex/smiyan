<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\models;


use app\modules\order\helpers\Mail;
use Yii;
use yii\base\Model;

class Callback extends Model
{
    /**
     * @var
     */
    public $name;

    /**
     * @var
     */
    public $telephone;

    /**
     * @var
     */
    public $email;

    /**
     * @var bool
     */
    public $isProcessed = false;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'telephone'], 'required'],
            [['name', 'telephone', 'email'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Имя'),
            'telephone' => Yii::t('app', 'Номер телефона'),
            'email' => Yii::t('app', 'Почта'),
        ];
    }

    /**
     *
     */
    public function process()
    {
        $this->isProcessed = Mail::sendCallbackMail($this);
    }
}