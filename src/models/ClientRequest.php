<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\models;


use app\modules\order\helpers\Mail;
use yii\base\Model;

class ClientRequest extends Model
{
    public $name;
    public $contactInfo;
    public $message;
    public $isProcessed = false;

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'contactInfo' => 'Почта или телефон',
            'message' => 'Сообщение',
        ];
    }

    public function rules()
    {
        return [
            [['name', 'contactInfo', 'message'], 'required'],
            [['name', 'contactInfo', 'message'], 'string'],
        ];
    }

    /**
     *
     */
    public function process()
    {
        $this->isProcessed = Mail::sendClientRequestMail($this);
        $this->name = '';
        $this->contactInfo = '';
        $this->message = '';
    }


}