<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\settings\models;


class DeliveryTime extends SettingsModel
{
    const SETTINGS_SECTION = 'delivery';

    /** @var array|string */
    public $time;

    /**
     * @return array
     */
    public static function getTimeList()
    {
        $model = new self();

        return array_combine($model->time, $model->time);
    }

    /**
     * @return string
     */
    public function getSection()
    {
        return self::SETTINGS_SECTION;
    }

    /**
     * Convert string to list
     */
    public function init()
    {
        parent::init();
        if ($this->time) {
            $this->time = unserialize($this->time);
        } else {
            $this->time = [];
        }
    }

    /**
     * Convert return list to serialized string
     *
     * @param bool $runValidation
     * @return bool
     */
    public function save($runValidation = true)
    {
        $this->time = serialize($this->time);
        return parent::save($runValidation);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time'], 'required'],
            [['time'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'time' => 'Список',
        ];
    }
}