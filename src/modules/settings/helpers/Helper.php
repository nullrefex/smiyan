<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\settings\helpers;


class Helper
{
    public static function getPublicSetting()
    {
        return ['phone', 'email', 'facebook', 'instagram', 'pinterest', 'video', 'delivery_price'];
    }
}