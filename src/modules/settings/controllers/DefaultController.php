<?php

/**
 * @link http://phe.me
 * @copyright Copyright (c) 2014 Pheme
 * @license MIT http://opensource.org/licenses/MIT
 */

namespace app\modules\settings\controllers;

use app\modules\settings\helpers\Helper;
use app\modules\settings\models\DeliveryTime;
use app\modules\settings\models\ProductList;
use app\modules\settings\models\SettingSearch;
use dektrium\user\filters\AccessRule;
use nullref\core\interfaces\IAdminController;
use nullref\fulladmin\filters\AccessControl;
use pheme\settings\controllers\DefaultController as BaseController;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * Class DefaultController
 * @package app\modules\settings\controllers
 */
class DefaultController extends BaseController implements IAdminController
{
    /**
     * Defines the controller behaviors
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'class' => AccessRule::className(),
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Settings.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SettingSearch();
        $dataProvider = $searchModel->search(ArrayHelper::merge(Yii::$app->request->queryParams,
            ['SettingSearch' => ['key' => Helper::getPublicSetting()]]));

        return $this->render('index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * @return string
     */
    public function actionDeliveryTime()
    {
        $model = new DeliveryTime();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('delivery-time', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionProductList()
    {
        $model = new ProductList();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('product-list', [
            'model' => $model,
        ]);
    }
}
