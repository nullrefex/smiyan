<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */

use app\modules\product\models\Product;
use pheme\settings\Module;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var \app\modules\settings\models\ProductList $model
 */

$this->title = 'Товары на главной';
$this->params['breadcrumbs'][] = ['label' => Module::t('settings', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-create">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'list')->widget(MultipleInput::className(), [
        'allowEmptyList' => false,
        'columns' => [
            [
                'name' => 'list',
                'type' => MultipleInputColumn::TYPE_DROPDOWN,
                'items' => Product::find()->andWhere(['type' => Product::TYPE_VARIOUS])->getMap(),
                'options' => [
                    'prompt' => '',
                ],
            ],
        ],
        'enableGuessTitle' => true,
        'addButtonPosition' => MultipleInput::POS_HEADER
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
