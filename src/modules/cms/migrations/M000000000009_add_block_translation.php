<?php

namespace app\modules\cms\migrations;

use nullref\core\traits\MigrationTrait;
use yii\db\Migration;

class M000000000009_add_block_translation extends Migration
{
    use MigrationTrait;

    public function up()
    {
        $this->createTable('{{%cms_block_translation}}', [
            'id' => $this->primaryKey(),
            'block_id' => $this->integer(),
            'language' => $this->integer(),
            'config' => $this->text(),
        ], $this->getTableOptions());

        $this->dropColumn('{{%cms_block}}', 'config');
    }

    public function down()
    {
        $this->addColumn('{{%cms_block}}', 'config', $this->text());
        $this->dropTable('{{%cms_block_translation}}');
        return true;
    }
}
