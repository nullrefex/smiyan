<?php

namespace app\modules\cms\migrations;

use nullref\core\traits\MigrationTrait;
use yii\db\Migration;

class M000000000008_add_page_translation extends Migration
{
    use MigrationTrait;

    public function up()
    {
        $this->createTable('{{%cms_page_translation}}', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'language' => $this->integer(),
            'title' => $this->string()->notNull(),
            'content' => $this->text(),
            'meta' => $this->text(),
        ], $this->getTableOptions());

        $this->dropColumn('{{%cms_page}}', 'title');
        $this->dropColumn('{{%cms_page}}', 'content');
        $this->dropColumn('{{%cms_page}}', 'meta');
    }

    public function down()
    {
        $this->addColumn('{{%cms_page}}', 'title', $this->string()->notNull());
        $this->addColumn('{{%cms_page}}', 'content', $this->text());
        $this->addColumn('{{%cms_page}}', 'meta', $this->text());
        $this->dropTable('{{%cms_page_translation}}');
        return true;
    }
}
