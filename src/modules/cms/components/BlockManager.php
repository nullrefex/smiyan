<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\cms\components;


class BlockManager extends \nullref\cms\components\BlockManager
{
    public function getList()
    {
        return [
            'html' => 'app\modules\cms\blocks\html',
            'title' => 'app\modules\cms\blocks\title',
            'text' => 'app\modules\cms\blocks\text',
            'slider' => 'app\modules\cms\blocks\slider',
        ];
    }
}