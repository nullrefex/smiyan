<?php
/**
 *
 */

namespace app\modules\cms\blocks\text;

use app\modules\cms\components\BaseWidget;
use yii\helpers\Html;


class Widget extends BaseWidget
{
    public $content;

    public function run()
    {
        $lang = $this->languageManager->getLanguage()->getSlug();

        return $this->content[$lang];
    }
}