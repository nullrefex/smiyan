<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $images array
 */
use yii\helpers\Url;

?>
<div class="slider type-1">
    <div class="images">

        <?php foreach ($images as $image): ?>
            <div class="image" style="background-image: url(<?= $image['image'] ?>);"></div>
        <?php endforeach ?>
    </div>

    <div class="controls flex space-between">
        <div class="left">
            <img src="/img/svg/arrow_black.svg" alt="Arrow">
        </div>
        <div class="right">
            <img src="/img/svg/arrow_black.svg" alt="Arrow">
        </div>
    </div>

    <div class="sharing">
        <div class="main-icon flex middle center">
            <img src="/img/sharing_icon.png" alt="Sharing">
        </div>

        <div class="another-icons">
            <a href="https://www.facebook.com/sharer/sharer.php?app_id=269450823532589&sdk=joey&u=<?= Url::current([], true) ?>&display=popup&ref=plugin&src=share_button">
                <i class="fa fa-facebook" aria-hidden="true"></i>
            </a>

            <a href="https://twitter.com/intent/tweet?text=<?= Url::current([], true) ?>">
                <i class="fa fa-twitter" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</div>

<div class="slider type-2">
    <ul>
        <?php foreach ($images as $image): ?>
            <li style="background-image: url(<?= $image['image'] ?>);"></li>
        <?php endforeach ?>
    </ul>

    <div class="controls flex space-between">
        <div class="left">
            <img src="/img/svg/arrow_black.svg" alt="Arrow">
        </div>
        <div class="right">
            <img src="/img/svg/arrow_black.svg" alt="Arrow">
        </div>
    </div>
</div>