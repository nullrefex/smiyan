<?php

namespace app\modules\cms\blocks\slider;

use app\modules\cms\components\BaseWidget;

class Widget extends BaseWidget
{
    public $images;

    public function run()
    {
        return $this->render('slider', [
            'images' => $this->images,
        ]);
    }
}