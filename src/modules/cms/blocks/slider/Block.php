<?php

namespace app\modules\cms\blocks\slider;

use app\modules\cms\components\BaseBlock;
use Yii;

/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $images;

    public function getName()
    {
        return 'Слайдер';
    }


    public function rules()
    {
        return [
            [['images'], 'safe'],
        ];
    }

    public function getMultilingualAttributes()
    {
        return [];
    }

    public function attributeLabels()
    {
        return [
            'images' => Yii::t('app', 'Images'),
        ];
    }
}