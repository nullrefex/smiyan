<?php
/**
 *
 */

namespace app\modules\cms\blocks\html;

use app\modules\cms\components\BaseWidget;
use yii\helpers\Html;


class Widget extends BaseWidget
{
    public $content;
    public $tag = '';
    public $tagClass = '';

    public function run()
    {
        $lang = $this->languageManager->getLanguage()->getSlug();

        if (!empty($this->tag)) {
            return Html::tag($this->tag, $this->content[$lang], empty($this->tagClass) ? [] : ['class' => $this->tagClass]);
        }
        return $this->content[$lang];
    }
}