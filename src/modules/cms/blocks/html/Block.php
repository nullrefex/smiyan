<?php

namespace app\modules\cms\blocks\html;

use app\modules\cms\components\BaseBlock;

/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $content;
    public $tag = 'div';
    public $tagClass = 'alert alert-success';

    public function getMultilingualAttributes()
    {
        return ['content'];
    }

    public function getName()
    {
        return 'HTML Block';
    }

    public function rules()
    {
        return [
            [['content', 'tag'], 'safe'],
            [['tagClass'], 'string'],
        ];
    }

}