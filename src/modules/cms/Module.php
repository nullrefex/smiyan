<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\cms;


use Yii;

class Module extends \nullref\cms\Module
{
    public $controllerNamespace = 'nullref\cms\controllers';


    public static function getAdminMenu()
    {
        return [
            'label' => Yii::t('cms', 'CMS'),
            'icon' => 'columns',
            'order' => 2,
            'items' => [
                [
                    'label' => Yii::t('cms', 'Pages'),
                    'icon' => 'copy',
                    'url' => '/cms/admin/page',
                ],
                [
                    'label' => Yii::t('cms', 'Blocks'),
                    'icon' => 'clone',
                    'url' => '/cms/admin/block',
                ],
                [
                    'label' => Yii::t('cms', 'Files'),
                    'icon' => 'files-o',
                    'url' => '/cms/admin/files',
                ],
            ]
        ];
    }
}