<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\cms\models;


use app\helpers\Languages;
use nullref\cms\models\PageQuery;
use nullref\useful\behaviors\TranslationBehavior;

class Page extends \nullref\cms\models\Page
{
    /**
     * @inheritdoc
     * @return PageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PageQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = array_merge(parent::behaviors(), [
            'multilingual' => [
                'class' => TranslationBehavior::class,
                'languages' => Languages::getSlugMap(),
                'defaultLanguage' => Languages::get()->getSlug(),
                'relation' => 'translations',
                'attributeNamePattern' => '{attr}_{lang}',
                'languageField' => 'language',
                'langClassName' => PageTranslation::className(),
                'translationAttributes' => [
                    'title', 'meta', 'content',
                ],
            ],
        ]);
        unset($behaviors['serialize']);
        return $behaviors;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageTranslation::class, ['page_id' => 'id']);
    }

    /**
     * @return array|\string[]
     */
    public function safeAttributes()
    {
        return $this->addMultilingualFileds(parent::safeAttributes());
    }

    /**
     * @param $attributes
     * @return array
     */
    protected function addMultilingualFileds($attributes)
    {
        $result = $attributes;
        foreach ($attributes as $item) {
            if (in_array($item, ['name', 'description'])) {
                foreach (Languages::getSlugMap() as $id => $lang) {
                    $result[] = $item . '_' . $lang;
                }
            }
        }
        return $result;
    }

    /**
     * @return array
     */
    public function activeAttributes()
    {
        return $this->addMultilingualFileds(parent::activeAttributes());
    }
}