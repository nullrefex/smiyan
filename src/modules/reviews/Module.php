<?php

namespace app\modules\reviews;

use nullref\core\interfaces\IAdminModule;
use Yii;
use yii\base\Module as BaseModule;

/**
 * settings module definition class
 */
class Module extends BaseModule implements IAdminModule
{
    /**
     * @return array
     */
    public static function getAdminMenu()
    {
        return [
            'url' => ['/reviews/admin'],
            'icon' => 'comment',
            'label' => Yii::t('reviews', 'Reviews'),
        ];
    }
}
