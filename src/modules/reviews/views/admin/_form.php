<?php

use app\helpers\Languages;
use app\modules\shop\models\Shop;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\reviews\models\Reviews */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reviews-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'customer_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contacts')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shop_id')->dropDownList(Shop::getShopList(), ['prompt' => Yii::t('reviews', 'Choose shop')]) ?>

    <?= $form->field($model, 'rating')->dropDownList($model::getRatingList(), ['prompt' => Yii::t('reviews', 'Choose rating')]) ?>

    <?= $form->field($model, 'approved')->dropDownList($model::getApprovedList(), ['prompt' => Yii::t('reviews', 'Choose approved')]) ?>

    <?= $form->field($model, 'language')->dropDownList(Languages::getMap()) ?>

    <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('reviews', 'Create') : Yii::t('reviews', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
