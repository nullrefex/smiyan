<?php

namespace app\modules\reviews\migrations;

use yii\db\Migration;

class M170713084753Review_fix extends Migration
{
    const TABLE_REVIEWS = '{{%reviews}}';

    public function up()
    {
        $this->addColumn(self::TABLE_REVIEWS, 'language', $this->integer());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_REVIEWS, 'language');
        return true;
    }
}
