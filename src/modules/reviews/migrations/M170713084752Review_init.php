<?php

namespace app\modules\reviews\migrations;

use yii\db\Migration;

class M170713084752Review_init extends Migration
{
    const TABLE_REVIEWS = '{{%reviews}}';

    public function up()
    {
        $this->createTable(self::TABLE_REVIEWS, [
            'id' => $this->primaryKey(),
            'customer_name' => $this->string(),
            'contacts' => $this->string(),
            'shop_id' => $this->integer(),
            'rating' => $this->smallInteger(),
            'message' => $this->text(),
            'approved' => $this->smallInteger(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_REVIEWS);
        return true;
    }
}
