<?php

namespace app\modules\reviews\models;

use app\modules\shop\models\Shop;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\validators\EmailValidator;

/**
 * This is the model class for table "{{%reviews}}".
 *
 * @property int $id
 * @property string $customer_name
 * @property string $contacts
 * @property int $shop_id
 * @property int $rating
 * @property string $message
 * @property integer $approved
 * @property int $language
 * @property int $created_at
 * @property int $updated_at
 */
class Reviews extends ActiveRecord
{
    const APPROVED = 1;
    const NOT_APPROVED = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%reviews}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shop_id', 'rating', 'created_at', 'updated_at', 'approved', 'language'], 'integer'],
            [['message'], 'string'],
            [['customer_name', 'message'], 'string', 'max' => 255],
            ['contacts', function ($attribute, $value) {
            $error = false;
                if (strpos($this->contacts, '@') !== false) {
                    $validator = new EmailValidator();

                    if (!$validator->validate($this->contacts, $error)) {
                        $error = true;
                    }
                } else {
                    if (!preg_match("/^\+380[0-9]{9}$/", $this->contacts)) {
                        $error = true;
                    }
                }
                if ($error) {
                    $this->addError($attribute, Yii::t('reviews', 'Емейл должен быть в формате example@email.com, телефон - +380XXXXXXXXX.'));
                }
            }],
            [['shop_id', 'rating', 'approved', 'message', 'customer_name', 'contacts', 'language'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('reviews', 'ID'),
            'customer_name' => Yii::t('reviews', 'Customer Name'),
            'contacts' => Yii::t('reviews', 'Contacts'),
            'shop_id' => Yii::t('reviews', 'Shop ID'),
            'rating' => Yii::t('reviews', 'Rating'),
            'message' => Yii::t('reviews', 'Message'),
            'approved' => Yii::t('reviews', 'Approved'),
            'language' => Yii::t('reviews', 'Language'),
            'created_at' => Yii::t('reviews', 'Created At'),
            'updated_at' => Yii::t('reviews', 'Updated At'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }

    public static function getRatingList()
    {
        return [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5];
    }

    public static function getApprovedList()
    {
        return [
            self::NOT_APPROVED => Yii::t('reviews', 'Not Approved'),
            self::APPROVED => Yii::t('reviews', 'Approved'),
        ];
    }

    public function getShopName()
    {
        $shop = Shop::findOne($this->shop_id);
        if ($shop) {
            return $shop->name;
        }
        return $this->shop_id;
    }
}
