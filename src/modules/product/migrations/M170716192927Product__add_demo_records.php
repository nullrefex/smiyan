<?php

namespace app\modules\product\migrations;

use app\modules\category\models\Category;
use app\modules\product\models\Product;
use app\modules\product\models\ProductForm;
use yii\db\Migration;

class M170716192927Product__add_demo_records extends Migration
{
    public function up()
    {
        foreach (Category::find()->all() as $category) {
            $this->createSingleProduct($category->id);
            $this->createVariousProduct($category->id);
        }

    }

    public function createSingleProduct($categoryId)
    {
        $model = ProductForm::createNew(['type' => Product::TYPE_SINGLE]);
        $model->load([
            'name_uk' => 'Торт',
            'name_ru' => 'Торт',
            'name_en' => 'Торт',
            'price' => 100,
            'image' => '/img/product_1.png',
            'category_id' => $categoryId,
        ], '');
        $model->save();
    }

    public function createVariousProduct($categoryId)
    {
        $model = ProductForm::createNew(['type' => Product::TYPE_VARIOUS]);
        $model->load([
            'name_uk' => 'Торт',
            'name_ru' => 'Торт',
            'name_en' => 'Торт',
            'description_uk' => 'Описание',
            'description_ru' => 'Описание',
            'description_en' => 'Описание',
            'price' => 100,
            'image' => '/img/product_1.png',
            'category_id' => $categoryId,
        ], '');
        $model->save();

        $weight = 100;
        $price = 100;
        foreach (Product::getVariousSizes() as $variousSize => $name) {
            $variant = ProductForm::createNew(['type' => Product::TYPE_VARIANT, 'product_id' => $model->id]);
            $variant->load([
                'options' => [
                    'weight' => $weight,
                    'size' => $variousSize,
                ],
                'price' => $price,
            ], '');
            $weight *= 2;
            $price *= 2;
            $variant->save();
        }
    }

    public function down()
    {

    }
}
