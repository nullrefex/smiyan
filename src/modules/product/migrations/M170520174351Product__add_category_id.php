<?php

namespace app\modules\product\migrations;

use yii\db\Migration;

class M170520174351Product__add_category_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'category_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%product}}', 'category_id');
    }
}
