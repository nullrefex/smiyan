<?php

namespace app\modules\product\migrations;

use app\modules\product\models\Product;
use yii\db\Migration;

class M170716192926Product_on_index extends Migration
{
    public function up()
    {
        $this->addColumn(Product::tableName(), 'show_on_index', $this->smallInteger()->defaultValue('0'));
    }

    public function down()
    {
        $this->dropColumn(Product::tableName(), 'show_on_index');

        return true;
    }
}
