<?php

namespace app\modules\product\migrations;

use yii\db\Migration;

class M170714130840Product__improve_products extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%product_translation}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'language' => $this->integer(),
            'name' => $this->string(),
            'description' => $this->text(),
        ]);
        $this->dropColumn('{{%product}}', 'name');
    }

    public function safeDown()
    {
        $this->addColumn('{{%product}}', 'name', $this->string());
        $this->dropTable('{{%product_translation}}');
    }
}
