<?php

namespace app\modules\product\migrations;

use yii\db\Migration;

class M170520174352Product__add_product_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'product_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%product}}', 'product_id');
    }
}
