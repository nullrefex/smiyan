<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\product\helpers;


use app\modules\product\models\Product;

class Helper
{
    /**
     * @return array
     */
    public static function getPublicTypes()
    {
        return [
            Product::TYPE_SINGLE,
            Product::TYPE_VARIOUS,
            Product::TYPE_SOUVENIR,
        ];
    }

    /**
     * @return array
     */
    public static function getPublicConfectioneryTypes()
    {
        return [
            Product::TYPE_SINGLE,
            Product::TYPE_VARIOUS,
        ];
    }

    /**
     * @param Product $product
     * @return string
     */
    public static function getTypeCssClass($product)
    {
        switch ($product->type) {
            case Product::TYPE_VARIOUS:
                return 'type-1';
            case Product::TYPE_SINGLE:
                return 'type-2';
            default:
                return '';
        }
    }
}