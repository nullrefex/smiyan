<?php

namespace app\modules\product\models;

use Yii;

/**
 * This is the model class for table "{{%product_translation}}".
 *
 * @property int $id
 * @property int $product_id
 * @property int $language
 * @property string $name
 * @property string $description
 */
class ProductTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'language'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('product', 'ID'),
            'product_id' => Yii::t('product', 'Product ID'),
            'language' => Yii::t('product', 'Language'),
            'name' => Yii::t('product', 'Name'),
            'description' => Yii::t('product', 'Description'),
        ];
    }
}
