<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $this \yii\web\View
 * @var $model \app\modules\product\models\ProductForm
 * @var $form \yii\widgets\ActiveForm
 *
 */
use app\helpers\Helper;
use app\modules\category\models\Category;
use mihaildev\elfinder\InputFile;
use nullref\core\widgets\Multilingual;
use yii\base\Model;
use yii\widgets\ActiveForm;

?>

<?= $form->field($model, 'identifier')->textInput() ?>

<div class="row">
    <div class="col-md-12">
        <?= Multilingual::widget(['model' => $model,
            'tab' => function (ActiveForm $form, Model $model) {
                echo $form->field($model, 'name')->textInput()->label(Yii::t('product', 'Name'));
                echo $form->field($model, 'description')->textarea()->label(Yii::t('product','Description'));
            }
        ]) ?>
    </div>
</div>

<?= $form->field($model, 'image')->widget(InputFile::className(), Helper::getInputFileOptions()) ?>

<?= $form->field($model, 'options[alt]')->textInput()->label(Yii::t('product', 'Alt')) ?>

<?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'category_id')->dropDownList(Category::find()->getMap('title')) ?>
