<?php

use app\modules\category\models\Category;
use app\modules\product\models\Product;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\product\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('product', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('product', 'Single Product'), ['create', 'data' => ['type' => Product::TYPE_SINGLE]], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('product', 'Various Product'), ['create', 'data' => ['type' => Product::TYPE_VARIOUS]], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('product', 'Souvenir Product'), ['create', 'data' => ['type' => Product::TYPE_SOUVENIR]], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'header' => '#',
                'attribute' => 'id',
                'filter' => false,
            ],

            'name',
            'price:decimal',
            'image:image',
            [
                'attribute' => 'type',
                'filter' => Product::getTypes(),
                'value' => 'typeTitle',
            ],
            [
                'attribute' => 'category_id',
                'filter' => Category::getMap('title', 'id', [], false),
                'value' => function (Product $model) {
                    $category = $model->category;
                    if (isset($category)) {
                        return $category->title;
                    }
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
