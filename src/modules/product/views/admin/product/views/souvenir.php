<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\product\models\Product */

?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'name',
        'image:image',
        'type',
        'identifier',
        'price',
        'created_at:datetime',
        'updated_at:datetime',
    ],
]) ?>