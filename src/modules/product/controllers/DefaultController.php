<?php

namespace app\modules\product\controllers;

use nullref\cms\components\PageMetaTagsFilter;
use yii\web\Controller;

/**
 * Default controller for the `product` module
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'seo' => [
                'class' => PageMetaTagsFilter::class,
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
