<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\product\widgets;


use app\modules\product\models\Product;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\data\ActiveDataProvider;

class ProductVariants extends Widget
{
    /** @var  Product */
    public $model;

    public function init()
    {
        if ($this->model === null) {
            throw new InvalidConfigException('$model must be set');
        }
    }

    /**
     * @return string
     */
    public function run()
    {
        if ($this->model->isNewRecord) {
            return '';
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $this->model->getVariants(),
        ]);

        return $this->render('product-variants', [
            'dataProvider' => $dataProvider,
            'model' => $this->model
        ]);
    }


}