<?php

use app\modules\product\models\Product;
use yii\grid\GridView;
use yii\helpers\Html;

/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $model \app\modules\product\models\Product
 */

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '{items}',
    'columns' => [
        [
            'label' => Yii::t('product', 'Size'),
            'value' => 'sizeTitle',
        ],
        [
            'label' => Yii::t('product', 'Weight'),
            'value' => 'options.weight',
        ],
        'price',
        [
            'class' => \yii\grid\ActionColumn::class,
        ],
    ],
]) ?>

<p>
    <?= Html::a(Yii::t('product', 'Add variant'),
        ['create', 'data' => ['product_id' => $model->id, 'type' => Product::TYPE_VARIANT]],
        ['class' => 'btn btn-primary']) ?>
</p>
