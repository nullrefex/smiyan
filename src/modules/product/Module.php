<?php

namespace app\modules\product;

use nullref\core\interfaces\IAdminModule;
use Yii;
use yii\base\Module as BaseModule;

/**
 * product module definition class
 */
class Module extends BaseModule implements IAdminModule
{
    const MODULE_ID = 'order';

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\product\controllers';

    public static function getAdminMenu()
    {
        return [
            'label' => Yii::t('product', 'Products'),
            'icon' => 'archive',
            'url' => '/product/admin/product',
            'order' => 2,
        ];
    }
}
