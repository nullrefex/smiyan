<?php

namespace app\modules\shop\migrations;

use yii\db\Migration;

class M170613102323Shop__multilang_name extends Migration
{
    const SHOP_TABLE = '{{%shop}}';
    const SHOP_TRANSLATION_TABLE = '{{%shop_translation}}';

    public function safeUp()
    {
        $this->dropColumn(self::SHOP_TABLE, 'name');
        $this->createTable(self::SHOP_TRANSLATION_TABLE, [
            'id' => $this->primaryKey(),
            'language' => $this->integer(),
            'name' => $this->string(),
            'shop_id' => $this->integer(),
        ]);
    }

    public function safeDown()
    {
        $this->addColumn(self::SHOP_TABLE, 'name', $this->string());
        $this->dropTable(self::SHOP_TRANSLATION_TABLE);
    }
}
