<?php

namespace app\modules\shop\migrations;

use pheme\settings\models\Setting;
use yii\db\Migration;

class M170708175852Contacts_settings_data extends Migration
{
    public function up()
    {
        $this->insert(Setting::tableName(), [
            'type' => 'string',
            'section' => 'contacts',
            'key' => 'facebook',
            'value' => 'https://www.facebook.com/',
            'active' => '1',
            'created' => '2017-07-08 20:43:30',
            'modified' => null,
        ]);

        $this->insert(Setting::tableName(), [
            'type' => 'string',
            'section' => 'contacts',
            'key' => 'instagram',
            'value' => 'https://www.instagram.com/',
            'active' => '1',
            'created' => '2017-07-08 20:43:31',
            'modified' => null,
        ]);

        $this->insert(Setting::tableName(), [
            'type' => 'string',
            'section' => 'contacts',
            'key' => 'pinterest',
            'value' => 'https://ru.pinterest.com/',
            'active' => '1',
            'created' => '2017-07-08 20:43:31',
            'modified' => null,
        ]);
    }

    public function down()
    {
        $this->delete(Setting::tableName(), ['section' => 'contacts', 'key' => 'facebook']);
        $this->delete(Setting::tableName(), ['section' => 'contacts', 'key' => 'instagram']);
        $this->delete(Setting::tableName(), ['section' => 'contacts', 'key' => 'pinterest']);
        return true;
    }
}
