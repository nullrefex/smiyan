<?php

namespace app\modules\shop;

use nullref\core\interfaces\IAdminModule;
use Yii;


/**
 * shop module definition class
 */
class Module extends \yii\base\Module implements IAdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\shop\controllers';

    /**
     * @return array
     */
    public static function getAdminMenu()
    {
        return [
            'url' => ['/shop/shop'],
            'icon' => 'shopping-bag',
            'label' => Yii::t('shop', 'Shops'),
        ];
    }
}
