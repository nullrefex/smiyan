<?php

namespace app\modules\shop\models;

use app\helpers\Languages;
use nullref\useful\behaviors\TranslationBehavior;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%shop}}".
 *
 * @property int $id
 * @property string $name
 * @property string $name_uk
 * @property string $name_en
 * @property string $name_ru
 * @property string $schedule
 * @property string $schedule_uk
 * @property string $schedule_en
 * @property string $schedule_ru
 * @property string $lat
 * @property string $lng
 */
class Shop extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lat', 'lng'], 'number'],
            [['lat', 'lng'], 'required'],
            [['name', 'schedule'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('shop', 'ID'),
            'name' => Yii::t('shop', 'Название'),
            'name_en' => Yii::t('shop', 'Название'),
            'name_ru' => Yii::t('shop', 'Название'),
            'name_uk' => Yii::t('shop', 'Название'),
            'schedule' => Yii::t('shop', 'График'),
            'schedule_en' => Yii::t('shop', 'График'),
            'schedule_ru' => Yii::t('shop', 'График'),
            'schedule_uk' => Yii::t('shop', 'График'),
            'lat' => Yii::t('shop', 'Lat'),
            'lng' => Yii::t('shop', 'Lng'),
        ];
    }

    /**
     * @inheritdoc
     * @return ShopQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ShopQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'multilanguage' => [
                'class' => TranslationBehavior::className(),
                'languages' => Languages::getSlugMap(),
                'defaultLanguage' => Languages::get()->getSlug(),
                'relation' => 'translations',
                'attributeNamePattern' => '{attr}_{lang}',
                'languageField' => 'language',
                'langClassName' => ShopTranslation::className(),
                'translationAttributes' => [
                    'name', 'schedule',
                ],
            ],
        ];
    }

    /**
     * @param $name
     * @return bool|static
     */
    public static function getObject($name)
    {
        $shopTranslation = ShopTranslation::findOne(['name' => $name]);
        $shop = $shopTranslation ? self::findOne(['id' => $shopTranslation->shop_id]) : false;
        return $shop ? $shop : false;
    }

    /**
     * @param $name
     * @return bool|int
     */
    public static function getId($name)
    {
        $shopTranslation = ShopTranslation::findOne(['name' => $name]);
        $shop = $shopTranslation ? self::findOne(['id' => $shopTranslation->shop_id]) : false;
        return $shop ? $shop->id : false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ShopTranslation::className(), ['shop_id' => 'id']);
    }

    public static function getShopList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}
