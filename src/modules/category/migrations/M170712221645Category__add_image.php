<?php

namespace app\modules\category\migrations;

use yii\db\Migration;

class M170712221645Category__add_image extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%category}}', 'image', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%category}}', 'image');
    }
}
