<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\category\models;


use app\helpers\Languages;
use app\modules\product\models\Product;
use nullref\category\models\Category as BaseCategory;
use nullref\useful\behaviors\TranslationBehavior;
use nullref\useful\traits\Mappable;
use Yii;

/**
 * Class Category
 * @package app\modules\category\models
 *
 * @method static CategoryQuery find()
 * @property string $image
 *
 * @property Product[] $products
 */
class Category extends BaseCategory
{
    use Mappable;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['image'], 'required'],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::class, ['category_id' => 'id']);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge([
            'image' => Yii::t('category', 'Image'),
        ], parent::attributeLabels());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'multilanguage' => [
                'class' => TranslationBehavior::className(),
                'languages' => Languages::getSlugMap(),
                'defaultLanguage' => Languages::get()->getSlug(),
                'relation' => 'translations',
                'attributeNamePattern' => '{attr}_{lang}',
                'languageField' => 'language',
                'langClassName' => CategoryTranslation::className(),
                'translationAttributes' => [
                    'title',
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(CategoryTranslation::className(), ['category_id' => 'id']);
    }
}