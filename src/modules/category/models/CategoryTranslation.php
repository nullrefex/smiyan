<?php

namespace app\modules\category\models;

use Yii;

/**
 * This is the model class for table "{{%category_translation}}".
 *
 * @property int $id
 * @property int $category_id
 * @property int $language
 * @property string $title
 */
class CategoryTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'language'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('product', 'ID'),
            'category_id' => Yii::t('product', 'Category ID'),
            'language' => Yii::t('product', 'Language'),
            'title' => Yii::t('product', 'Title'),
        ];
    }
}
