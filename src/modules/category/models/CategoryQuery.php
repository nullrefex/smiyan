<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\category\models;


use nullref\category\models\CategoryQuery as BaseCategoryQuery;
use nullref\useful\traits\MappableQuery;

class CategoryQuery extends BaseCategoryQuery
{
    use MappableQuery;
}