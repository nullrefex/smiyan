<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\category\helpers;


use app\modules\category\models\Category;

class Helper
{
    /**
     * @param Category $model
     * @return bool|string
     */
    public static function getCategorySvg($model)
    {
        $path = \Yii::getAlias('@webroot' . $model->image);
        if (file_exists($path)) {
            return file_get_contents($path);
        }
        return '';
    }
}