<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */


namespace app\modules\admin\controllers;


use app\modules\admin\helpers\Db;
use nullref\core\interfaces\IAdminController;
use nullref\useful\filters\RedirectFilter;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class ToolsController extends Controller implements IAdminController
{
    public function behaviors()
    {
        return [
            'redirect' => RedirectFilter::className(),
        ];
    }

    public function actionFiles()
    {
        return $this->render('files');
    }

    /**
     *
     */
    public function actionFlushThumbs()
    {
        $fs = new Filesystem();
        $files = Finder::create()->in(Yii::getAlias('@webroot/thumbs'));
        $fs->remove($files);
        Yii::$app->session->setFlash('success', 'Кеш был удален');
    }

    /**
     *
     */
    public function actionFlushCache()
    {
        if (Yii::$app->cache->flush()) {
            Yii::$app->session->setFlash('success', 'Кеш был удален');
        } else {
            Yii::$app->session->setFlash('error', 'Произошла ошибка');
        }
    }

    /**
     * @return Response
     */
    public function actionDbDump()
    {
        $filename = Yii::getAlias('@webroot/db_backups/full_backup_' . date('d-m-Y_H-i-s') . '.sql.gz');

        $user = Yii::$app->db->username;
        $pass = Yii::$app->db->password;
        $name = Db::getDsnAttribute('dbname', Yii::$app->db->dsn);
        $host = Db::getDsnAttribute('host', Yii::$app->db->dsn);
        $cmd = "mysqldump -u $user -p$pass $name --host=$host --single-transaction | gzip -c > $filename";

        exec($cmd);

        return Yii::$app->response->sendFile($filename);
    }
}