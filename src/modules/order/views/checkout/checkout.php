<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\order\models\CheckoutForm $model
 */

use app\helpers\Cms as CmsHelper;
use app\modules\settings\models\DeliveryTime;
use app\modules\shop\models\Shop;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;

/**
 * @var $liqpay string
 * @var $popup string
 */

$this->title = Yii::t('order', 'Checkout');
if ($liqpay) {
    $liqpay .= Html::script("sessionStorage.setItem('discountCounter', 0);");
}

$this->registerJs(<<<JS
$('form#checkout-form').submit(function(e) {
    var discountInput = $('#discount');
    discountInput.val(smiyan.checkDiscount());
});
$('#checkout_delivery_time').on('change', function() {
  var value = $(this).val();
  $('#checkoutform-self_delivery_time').val(value);
  $('#checkoutform-delivery_time').val(value);
}).trigger('change');
JS
);
?>
<section class="type-7">
    <div class="wrapper">
        <div class="row type-1">
            <div class="column">
                <div class="el-1">
                    <div class="title type-1 var-b">
                        <img src="/img/smear_red_1.png" alt="Smear">

                        <?= Yii::t('order', 'Оформление заказа') ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row type-2">
            <div class="column">
                <div class="blocks type-1 flex wrap space-between">
                    <div class="block type-1">
                        <div class="el-2 title type-4 var-a"><?= Yii::t('order', 'Доставка') ?></div>

                        <div class="list type-1">
                            <ul>
                                <li>
                                    <div class="number flex middle">
                                        <div class="lines flex">
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                        </div>

                                        <span>01</span>
                                    </div>

                                    <div class="content var-b">
                                        <p>
                                            <?= Yii::t('order', 'Стоимость {price} грн.', [
                                                'price' => Yii::$app->get('settings')->get('order.delivery_price')
                                            ]) ?>
                                        </p>
                                    </div>
                                </li>

                                <li>
                                    <div class="number flex middle">
                                        <div class="lines flex">
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                        </div>

                                        <span>02</span>
                                    </div>

                                    <div class="content var-b">
                                        <p>
                                            <?= Yii::t('order', 'Производится после 100% предоплаты') ?>
                                        </p>
                                    </div>
                                </li>

                                <li>
                                    <div class="number flex middle">
                                        <div class="lines flex">
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                        </div>

                                        <span>03</span>
                                    </div>

                                    <div class="content var-b">
                                        <p>
                                            <?= Yii::t('order', 'В течение 3 часов') ?>
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="block type-2">
                        <?php Pjax::begin(['timeout' => 9999999]); ?>
                        <?php $form = ActiveForm::begin([
                            'id' => 'checkout-form',
                            'fieldConfig' => ['options' => ['class' => 'input-outer']],
                            'options' => [
                                'data-pjax' => true,
                            ]
                        ]); ?>
                        <div class="sides flex space-between wrap">
                            <div class="side-1">
                                <?= $form->field($model, 'customer_name')->textInput(['maxlength' => true, 'placeholder' => Yii::t('order', 'Имя')])->label(false) ?>

                                <?= $form->field($model, 'customer_email')->textInput(['maxlength' => true, 'type' => 'email', 'placeholder' => Yii::t('order', 'Почта')])->label(false) ?>

                                <?= $form->field($model, 'customer_phone')->widget(MaskedInput::className(), [
                                    'mask' => '+380999999999',
                                    'options' => [
                                        'maxlength' => true,
                                        'type' => 'tel',
                                        'placeholder' => Yii::t('order', 'Телефон')
                                    ]
                                ])->label(false) ?>
                            </div>

                            <div class="side-2">
                                <div class="input-outer radio-buttons-outer flex middle">
                                    <label class="radio-label flex middle" for="radio_1">
                                        <input id="radio_1" type="radio" name="CheckoutForm[group]"
                                               data-price="<?= $model::getDeliveryPrice($model::TYPE_SELF_DELIVERY) ?>"
                                               value="<?= $model::TYPE_SELF_DELIVERY ?>" checked>
                                        <div class="visual-radio"></div>

                                        <span><?= Yii::t('order', 'Самовывоз') ?></span>
                                    </label>

                                    <label class="radio-label flex middle" for="radio_2">
                                        <input id="radio_2" type="radio" name="CheckoutForm[group]"
                                               data-price="<?= $model::getDeliveryPrice($model::TYPE_DELIVERY) ?>"
                                               value="<?= $model::TYPE_DELIVERY ?>">
                                        <div class="visual-radio"></div>

                                        <span><?= Yii::t('order', 'Доставка') ?></span>
                                    </label>
                                </div>

                                <?= $form->field($model, 'shop_id', ['options' => ['class' => 'for-radio for-radio-1 input-outer select']])->dropDownList(Shop::getShopList(), ['prompt' => Yii::t('order', 'Выберите ресторан')])->label(false) ?>


                                <?= $form->field($model, 'delivery_address', ['options' => ['class' => 'for-radio for-radio-2 input-outer']])->textInput(['maxlength' => true, 'placeholder' => Yii::t('order', 'Адрес')])->label(false) ?>

                                <div style="display: none;">
                                    <?= $form->field($model, 'discount')->hiddenInput(['id' => 'discount'])->label(false) ?>
                                    <?= $form->field($model, 'self_delivery_time')->hiddenInput()->label(false) ?>
                                    <?= $form->field($model, 'delivery_time')->hiddenInput()->label(false) ?>
                                </div>

                                <div id="checkout_delivery_time_inputs" class="inputs flex space-between wrap">
                                    <div class="for-radio for-radio-1 text text-2 checkout-el-1">
                                        <?= Yii::t('order', 'В какое время вас ожидать?') ?>
                                    </div>
                                    <div class="for-radio for-radio-2 text text-2 checkout-el-1">
                                        <?= Yii::t('order', 'Желаемое время доставки') ?>
                                    </div>

                                    <div class="input-outer select">
                                        <select>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                        </select>
                                    </div>

                                    <div class="input-outer select">
                                        <select>
                                            <option value="Январь">Январь</option>
                                            <option value="Февраль">Февраль</option>
                                            <option value="Март">Март</option>
                                            <option value="Апрель">Апрель</option>
                                            <option value="Май">Май</option>
                                            <option value="Июнь">Июнь</option>
                                            <option value="Июль">Июль</option>
                                            <option value="Август">Август</option>
                                            <option value="Сентябрь">Сентябрь</option>
                                            <option value="Октябрь">Октябрь</option>
                                            <option value="Ноябрь">Ноябрь</option>
                                            <option value="Декабрь">Декабрь</option>
                                        </select>
                                    </div>

                                    <div class="input-outer select">
                                        <?= Html::dropDownList('time', null, DeliveryTime::getTimeList()) ?>
                                    </div>
                                </div>

                                <input id="checkout_delivery_time" type="hidden">
                            </div>
                        </div>

                        <div class="input-outer order-total">
                            <div class="total title type-4 var-a">
                                <?= Yii::t('app', 'Total: ') ?>
                                <span class="total-cost-text"><?= $model->total_cost ?></span> ₴
                                <span class="delivery-text"> (
                                    <span class="cost-text"><?= $model->total_cost ?></span> +
                                    <span class="delivery-cost"></span>
                                    <?= Yii::t('app', ' ₴ доставка') ?>
                                    )
                                </span>
                            </div>
                        </div>

                        <div class="input-outer submit">
                            <input class="button type-1" type="submit" value="<?= Yii::t('order', 'Заказать') ?>">
                        </div>
                        <?php ActiveForm::end(); ?>
                        <div style="display: none;">
                            <?= $liqpay ?>
                        </div>
                        <div id="checkout-popup" class="pop-up flex middle center active">
                            <div class="inner">
                                <div class="title type-3 var-a">
                                    <?= $popup ?>
                                </div>
                            </div>
                        </div>
                        <?php
                        if ($popup) {
                            $this->registerJs('smiyan.showCheckoutPopup();');
                        }
                        Pjax::end(); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row type-3">
            <div class="column el-3">
                <div class="text text-1 var-b">
                    <?= CmsHelper::getBlock('checkout-info') ?>
                </div>
            </div>
        </div>
    </div>
</section>