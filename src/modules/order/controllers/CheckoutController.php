<?php

namespace app\modules\order\controllers;

use app\components\PageMetaTagsFilter;
use app\modules\order\helpers\Mail;
use app\modules\order\models\CheckoutForm;
use app\modules\order\models\Order;
use app\traits\CartTrait;
use delagics\liqpay\LiqPay;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Session;

class CheckoutController extends Controller
{
    use CartTrait;

    public function behaviors()
    {
        return [
            'seo' => [
                'class' => PageMetaTagsFilter::class,
            ],
        ];
    }

    public function actionIndex()
    {
        $html = '';
        $popup = '';
        $model = new CheckoutForm();
        $model->total_cost = $this->cart->getCost();
        if ($this->cart->getCount() == 0) {
            $popup = Yii::t('order', 'Ваша корзина пуста.');
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->createOrder()) {
                    Mail::sendNewOrderMail($model);
                    $private_key = getenv('LIQPAY_PRIVAT_KEY');
                    $public_key = getenv('LIQPAY_PUBLIC_KEY');
                    $liqpay = new LiqPay($public_key, $private_key);
                    $this->cart->removeAll();
                    $html = $liqpay->cnb_form([
                        'sandbox' => getenv('LIQPAY_SANDBOX'),
                        'action' => 'pay',
                        'amount' => $model->total_cost,
                        'currency' => 'UAH',
                        'description' => Yii::t('app', 'Оплата замовлення'),
                        'order_id' => $model->lp_order_id,
                        'version' => '3',
                        'result_url' => Url::home(true) . '?order=' . $model->lp_order_id,
                        'server_url' => Url::toRoute('/order/checkout/callback', true),
                    ], 'liqpay', true);
                }
            }
        }
        return $this->render('checkout', [
            'model' => $model,
            'liqpay' => $html,
            'popup' => $popup,
        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionCallback()
    {
        $data = Yii::$app->request->post('data');
        file_put_contents(Yii::getAlias('@webroot/log.log'), print_r($_POST, true), FILE_APPEND);
        if ($data) {
            $encodeData = base64_decode($data);
            $data = json_decode($encodeData);
            file_put_contents(Yii::getAlias('@webroot/log.log'), print_r($data, true), FILE_APPEND);
            $status = $data->status;
            $allow = [
                'success',
                'wait_accept',
            ];
            $sandbox = getenv('LIQPAY_SANDBOX');
            if ($sandbox) {
                $allow[] = 'sandbox';
            }
            if (in_array($status, $allow)) {
                $orderId = $data->order_id;
                $order = Order::findByLpOrderId($orderId);
                if ($order) {
                    $order->status = $order::STATUS_PAID;
                    $order->liqpay_data = $encodeData;
                    $order->save(false);
                }
            }
        }
    }

    public function actionTest($id = false)
    {
        if ($id == false) {
            $id = Yii::$app->session->getId();
            echo $id;

            $s = new Session();
            $s->setId($id);

        } else {

            echo $id;
        }
        echo '<pre>';
        print_r($_SESSION);
    }
}