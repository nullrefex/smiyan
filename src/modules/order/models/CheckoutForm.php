<?php

namespace app\modules\order\models;

use app\components\ShoppingCart;
use app\modules\product\models\ProductCartPosition;
use app\traits\CartTrait;
use Yii;

class CheckoutForm extends Order
{
    use CartTrait;

    public $self_delivery_time;
    public $group;
    public $discount = false;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['self_delivery_time'], 'string', 'max' => 255];
        $rules[] = ['group', 'required'];
        $rules[] = [['self_delivery_time', 'shop_id'], 'required', 'when' => function ($model) {
            return $model->group == $model::TYPE_SELF_DELIVERY;
        }, 'whenClient' => "function (attribute, value) {
    return ($( \"input[name='CheckoutForm[group]']:checked\" ).val() == 'self-delivery');
}"];
        $rules[] = [['delivery_time', 'delivery_address'], 'required', 'when' => function ($model) {
            return $model->group == $model::TYPE_DELIVERY;
        }, 'whenClient' => "function (attribute, value) {
    return ($( \"input[name='CheckoutForm[group]']:checked\" ).val() == 'delivery');
}"];
        $rules[] = ['discount', 'safe'];

        return $rules;
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['self_delivery_time'] = Yii::t('order', 'В какое время вас ожидать?');
        $labels['delivery_address'] = Yii::t('order', 'Адрес');
        $labels['delivery_time'] = Yii::t('order', 'Желаемое время доставки');
        $labels['customer_name'] = Yii::t('order', 'Имя');
        $labels['customer_email'] = Yii::t('order', 'Почта');
        $labels['customer_phone'] = Yii::t('order', 'Телефон');

        return $labels;
    }

    /**
     *
     */
    public function createOrder()
    {
        $time = time();
        $this->created_at = $time;
        $this->updated_at = $time;
        $this->type = $this->group;
        $this->cart->on(ShoppingCart::EVENT_COST_CALCULATION, function ($event) {
            $event->discountValue = $event->baseCost * ($this->discount ? 0.1 : 0);
        });
        $this->delivery_price = Order::getDeliveryPrice($this->type);
        $this->total_cost = $this->cart->getCost(true) + $this->delivery_price;
        if ($this->group == self::TYPE_SELF_DELIVERY) {
            $this->delivery_time = $this->self_delivery_time;
        }
        $this->status = self::STATUS_NEW;
        if ($this->save()) {
            $this->lp_order_id = $this->id . '_' . time();
            if ($this->save(false)) {
                /** @var ProductCartPosition[] $orderPositions */
                $orderPositions = Yii::$app->cart->getPositions();
                foreach ($orderPositions as $position) {
                    $item = new OrderItem();
                    $item->order_id = $this->id;
                    $item->product_id = $position->getId();
                    $item->qty = $position->getQuantity();
                    $item->price = $position->getPrice();
                    $item->save();
                }
                return true;
            }
        }
    }

}