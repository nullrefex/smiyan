<?php

namespace app\modules\order\migrations;

use yii\db\Migration;

class M170717134552Order_init extends Migration
{
    const TABLE_ORDER = '{{%order}}';
    const TABLE_ORDER_ITEM = '{{%order_item}}';

    public function up()
    {
        $this->createTable(self::TABLE_ORDER, [
            'id' => $this->primaryKey(),
            'shop_id' => $this->integer(),
            'delivery_address' => $this->string(),
            'delivery_time' => $this->string(),
            'customer_name' => $this->string(),
            'customer_email' => $this->string(),
            'customer_phone' => $this->string(),
            'type' => $this->string(),
            'total_cost' => $this->decimal(10, 2),
            'status' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createTable(self::TABLE_ORDER_ITEM, [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'product_id' => $this->integer(),
            'qty' => $this->integer(),
            'price' => $this->decimal(10, 2),
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_ORDER);
        $this->dropTable(self::TABLE_ORDER_ITEM);

        return true;
    }
}
