<?php

namespace app\modules\order\migrations;

use app\modules\order\models\Order;
use yii\db\Migration;

class M170803163724Order_update extends Migration
{
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'popup_shown', $this->smallInteger()->defaultValue(0));
        $this->addColumn(Order::tableName(), 'liqpay_data', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn(Order::tableName(), 'popup_shown');
        $this->dropColumn(Order::tableName(), 'liqpay_data');
        return true;
    }
}
