<?php

namespace app\modules\order\migrations;

use app\modules\order\models\Order;
use yii\db\Migration;

class M170804213835Order_add_delivery_price extends Migration
{
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'delivery_price', $this->decimal(10, 2));
    }

    public function safeDown()
    {
        $this->dropColumn(Order::tableName(), 'delivery_price');

        return true;
    }
}
