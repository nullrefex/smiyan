<?php

namespace app\modules\blog\widgets;

use app\helpers\Languages;
use app\modules\blog\models\Post;
use yii\base\Widget;

class OtherActivities extends Widget
{
    public $currentId;

    /**
     * @return string
     */
    public function run()
    {
        $languageId = Languages::get()->getId();
        $posts = Post::find()
            ->where(['lang' => $languageId])
            ->andFilterWhere(['!=', 'id', $this->currentId])
            ->orderBy(['updated_at' => SORT_DESC])
            ->limit(3)
            ->all();
        return $this->render('other_activities', [
            'posts' => $posts,
        ]);
    }
}