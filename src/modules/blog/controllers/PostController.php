<?php

namespace app\modules\blog\controllers;

use app\components\PageMetaTagsFilter;
use app\helpers\Languages;
use nullref\blog\models\PostSearch;
use Yii;
use nullref\blog\controllers\PostController as BasePostController;

class PostController extends BasePostController
{
    public function behaviors()
    {
        return [
            'seo' => [
                'class' => PageMetaTagsFilter::class,
            ],
        ];
    }
    public function actionIndex()
    {
        /** @var PostSearch $searchModel */
        $searchModel = Yii::createObject(PostSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $languageId = Languages::get()->getId();
        $dataProvider->query->where(['lang' => $languageId])->published();
        $dataProvider->pagination->pageSize = 6;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
