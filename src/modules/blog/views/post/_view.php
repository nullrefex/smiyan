<?php
/**
 * @var $this \yii\web\View
 * @var $model \nullref\blog\models\Post
 * @var $key mixed
 * @var $index integer
 * @var $widget \yii\widgets\ListView
 */
?>
    <div class="image-outer">
        <div class="frame"></div>
        <img class="image" style="background-image: url(<?= $model->picture ?>);">
    </div>

    <div class="el-4 text text-4 var-b"><?= Yii::$app->formatter->asDatetime($model->created_at, 'php:d.m.Y') ?></div>

    <div class="title type-4 var-a"><?= $model->title ?></div>