<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */


namespace app\components;


use app\modules\cms\models\Page;
use Yii;
use yii\base\ActionFilter;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;

class PageMetaTagsFilter extends ActionFilter
{

    public function beforeAction($action)
    {
        $route = Yii::$app->request->url;
        $view = $action->controller->view;
        /** @var Page $page */
        $page = Page::getDb()->cache(function () use ($route) {
            return Page::find()->byRoute($route)->one();
        }, null, new TagDependency(['tags' => 'cms.page.' . $route]));
        if ($page !== null) {
            $page->registerMetaTags($view);
            $view->params['cms.page.meta'] = ArrayHelper::map($page->meta, 'name', 'content');
            $view->params['cms.content'] = $page->content;
            $view->title = $page->title;
        }
        $view->registerMetaTag(['url' => Yii::$app->request->url], 'pathInfo');

        return parent::beforeAction($action);
    }
}