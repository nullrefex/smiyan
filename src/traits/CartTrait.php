<?php


namespace app\traits;

use app\components\ShoppingCart;
use Yii;


/**
 * Trait CartTrait
 * @package app\components
 *
 * @property ShoppingCart $cart
 *
 */
trait CartTrait
{
    /**
     * @var ShoppingCart
     */
    protected $_cart;

    /**
     * @return ShoppingCart
     */
    public function getCart()
    {
        if (!isset($this->_cart)) {
            $this->_cart = Yii::$app->get('cart');
        }
        return $this->_cart;
    }
} 