<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 * @var $this \yii\web\View
 * @var $model \app\models\ClientRequest
 */
use app\helpers\Helper;
use app\helpers\Settings;
use yii\widgets\ActiveForm;

if (!$this->title) {
    $this->title = Yii::t('app', 'Корпоративным клиентам');
}
if ($model->isProcessed) {
    $this->registerJs('smiyan.showRequestPopup()');
}
?>

<section class="type-13">
	<div class="wrapper">
		<div class="row">
			<div class="el-1 column">
		        <div class="el-3">
		            <div class="title type-1 var-b">
		                <img src="/img/smear_red_1.png" alt="Smear">
                        <?= $this->title ?>
		            </div>
		        </div>
			</div>
		</div>

		<div class="row">
			<div class="el-4 column small-12 medium-6 large-6">
                <?php if (isset($this->params['cms.content'])): ?>
                    <?= $this->params['cms.content'] ?>
                <?php else: ?>
                    <div class="text-el-1 title type-4 var-a">
                        Мы готовы воплотить в жизнь любые самые смелые идеи и разработать для вас уникальный ассортимент
                        десертов.
                    </div>

                    <div class="text-el-2 text text-1 var-b">
                        Кондитерский дом Смиян всегда открыт для сотрудничества с бизнес-партнерами, которые разделяют
                        нашу любовь к высококачественным десертам.
                    </div>

                    <div class="text-el-3">
                        <a class="link type-3"
                           href="tel:<?= Helper::trimPhoneNumber(Settings::get('contacts.phone')) ?>">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <span><?= Settings::get('contacts.phone') ?></span>
                        </a>
                    </div>

                    <div class="text-el-4">
                        <a class="link type-3" href="mailto:<?= Settings::get('contacts.email') ?>">
                            <i class="fa fa-envelope var-a" aria-hidden="true"></i>
                            <span><?= Settings::get('contacts.email') ?></span>
                        </a>
                    </div>
                <?php endif ?>
			</div>

			<div class="column small-12 medium-1 large-1"></div>

			<div class="column small-12 medium-5 large-5">
                <?php $form = ActiveForm::begin([
                    'fieldConfig' => [
                        'template' => "{input}\n{hint}\n{error}",
                    ],
                ]) ?>
					<div class="inputs">
						<div class="input-outer">
                            <?= $form->field($model, 'name')
                                ->textInput(['placeholder' => Yii::t('app', 'Имя')]) ?>
						</div>

						<div class="input-outer">
                            <?= $form->field($model, 'contactInfo')
                                ->textInput(['placeholder' => Yii::t('app', 'Почта или телефон')]) ?>
						</div>

						<div class="input-outer">
                            <?= $form->field($model, 'message')
                                ->textInput(['placeholder' => Yii::t('app', 'Сообщение')]) ?>
						</div>

						<div class="input-outer submit">
		                    <input class="button type-1" type="submit" value="Отправить">
		                </div>
					</div>
                <?php ActiveForm::end() ?>
			</div>
		</div>
	</div>
</section>

<div id="request-successs-popup" class="pop-up flex middle center">
    <div class="inner">
        <div class="title type-3 var-a">
            <?= Yii::t('app', 'Благодарим за заявку. <br> В скором времени мы свяжемся с вами') ?>
        </div>
    </div>
</div>