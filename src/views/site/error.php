<?php

use app\helpers\Helper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

if (!$this->title) {
    $this->title = $name;
}

?>

<section class="type-11">
	<div class="wrapper-outer flex column middle center wrap">
		<div class="wrapper">
            <?php if ($exception instanceof NotFoundHttpException): /** for 404 */ ?>
				
				<div class="el-1-outer">
			    	<img class="el-1" src="/img/404.png" alt="404">
			    </div>

			    <div class="el-2 title type-4 var-b">
                    <?= Yii::t('app', 'Увы, но эта страница где-то затерялась в галактике интернета') ?>
			    </div>

                <div class="el-3-outer">
                    <a class="link type-1 var-a" href="<?= Helper::getBackUrl() ?>">
	                    <img class="left" src="/img/svg/arrow_red.svg" alt="Arrow">
                        <?= Yii::t('app', 'Назад') ?>
	                </a>
			    </div>

			<?php else: /** for another errors */ ?>

			    <div class="site-error">
			        <h1><?= Html::encode($this->title) ?></h1>

			        <div class="alert alert-danger">
			            <?= nl2br(Html::encode($message)) ?>
			        </div>
			    </div>
			    
			<?php endif ?>
		</div>
	</div>
</section>
