<?php

use app\widgets\Contacts;

if (!$this->title) {
    $this->title = Yii::t('app', 'Контакты');
}
?>
<section class="type-5 var-b">
    <div class="wrapper">
        <?= Contacts::widget() ?>
    </div>
</section>

