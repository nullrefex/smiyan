<?php

use yii\widgets\ListView;

/**
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

if (!$this->title) {
    $this->title = Yii::t('app', 'Сувениры');
}
?>
<section class="type-6 var-b">
    <div class="wrapper">
        <div class="row">
            <div class="el-1 column">
                <div class="el-2">
                    <div class="title type-2 var-b flex inline middle">
                        <div class="lines flex">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>

                        <span><?= Yii::t('app', 'Помни и кайфуй') ?></span>

                        <div class="lines reversed flex">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                </div>

                <div class="el-3">
                    <div class="title type-1 var-b">
                        <img src="/img/smear_red_1.png" alt="Smear">

                        <?= Yii::t('app', 'Сувениры') ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="blocks type-1 flex wrap">
            <div class="block type-2">
                <?= ListView::widget([
                        'id' => 'souvenirs',
                        'dataProvider' => $dataProvider,
                        'itemView' => '_souvenir',
                        'options' => [
                            'tag' => 'div',
                            'class' => 'scrollbar-outer flex wrap',
                        ],
                        'itemOptions' => function ($model, $key, $index, $widget) {
                            return [
                                'tag' => 'div',
                                'class' => 'item type-2 flex direction-column justify-end',
                            ];
                        },
                        'summary' => '',
                        'layout' => '{items} {pager}',
                    ]
                )
                ?>
            </div>
        </div>
    </div>
</section>