<?php

/* @var $this yii\web\View */

use app\helpers\Cms;
use app\helpers\Settings;
use app\widgets\About;
use app\widgets\Contacts;
use app\widgets\LanguageDropdown;
use app\widgets\Popular;
use app\widgets\Reviews;
use yii\helpers\Url;

if (!$this->title) {
    $this->title = Yii::$app->name;
}
?>

<div class="section-pagination light">
    <div class="wrapper">
        <div class="outer flex direction-column middle center">
            <div class="section-title">
                <div><?= Yii::t('app', 'Видео') ?></div>
                <div><?= Yii::t('app', 'Десерты') ?></div>
                <div><?= Cms::getBlockField('index-title-section-3', 'inscription') ?></div>
                <div><?= Yii::t('app', 'Отзывы') ?></div>
                <div><?= Yii::t('app', 'Контакты') ?></div>
            </div>

            <div class="pagination anchors">
                <a class="active" href="#section_1">
                    <span>1</span>
                </a>

                <a href="#section_2">
                    <span>2</span>
                </a>

                <a href="#section_3">
                    <span>3</span>
                </a>

                <a href="#section_4">
                    <span>4</span>
                </a>

                <a href="#section_5">
                    <span>5</span>
                </a>
            </div>
        </div>
    </div>
</div>

<section class="type-1" id="section_1">
    <style>
        body {
            padding-top: 0px;
        }

        header.hidden {
            -webkit-transform: translateY(-100px);
            -ms-transform: translateY(-100px);
            -o-transform: translateY(-100px);
            transform: translateY(-100px);
        }
    </style>

    <video src="<?= Settings::get('main.video') ?>" autoplay></video>

    <div class="section-type-1-header row align-justify align-middle">
        <div class="column shrink">
            <a class="logo" href="/">
                <img src="/img/svg/logo.svg" alt="Logo">
            </a>
        </div>

        <div class="column shrink">
            <?= LanguageDropdown::widget() ?>
        </div>

        <div class="column shrink">
            <div class="menu-button">
                <div class="inner flex middle center">
                    <div class="lines">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>

            <div class="menu">
                <ul class="flex anchors">
                    <li>
                        <a class="text text-1 var-a" href="<?= Url::to(['/confectionery']) ?>">
                            <?= Yii::t('app', 'Кондитерская') ?>
                        </a>
                    </li>
                    <li>
                        <a class="text text-1 var-a" href="<?= Url::to(['/site/souvenirs']) ?>">
                            <?= Yii::t('app', 'Сувениры') ?>
                        </a>
                    </li>
                    <li>
                        <a class="text text-1 var-a" href="<?= Url::to(['/#section_3']) ?>">
                            <?= Cms::getBlockField('index-title-section-3', 'inscription') ?>
                        </a>
                    </li>
                    <li>
                        <a class="text text-1 var-a" href="<?= Url::to(['/site/klientam']) ?>">
                            <?= Yii::t('app', 'Корпоративным клиентам') ?>
                        </a>
                    </li>
                    <li>
                        <a class="text text-1 var-a" href="<?= Url::to(['/blog']) ?>">
                            <?= Yii::t('app', 'Активности') ?>
                        </a>
                    </li>
                    <li>
                        <a class="text text-1 var-a" href="<?= Url::to(['/reviews/site']) ?>">
                            <?= Yii::t('app', 'Отзывы') ?>

                        </a>
                    </li>
                    <li>
                        <a class="text text-1 var-a" href="<?= Url::to(['/site/dostavka']) ?>">
                            <?= Yii::t('app', 'Доставка') ?>
                        </a>
                    </li>
                    <li>
                        <a class="text text-1 var-a" href="<?= Url::to(['/site/contacts']) ?>">
                            <?= Yii::t('app', 'Контакты') ?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="column shrink">
            <div class="basket-button basket flex middle <?= Yii::$app->cart->getCount() ? 'not-empty' : '' ?>">
                <span class="text text-2 var-a"><?= Yii::$app->cart->getCount() ?></span>
                <img class="icon" src="/img/svg/basket_white.svg" alt="Basket">
            </div>
        </div>
    </div>

    <div class="section-type-1-content flex direction-column middle center">
        <div class="el-1 title type-2 var-a flex inline middle anim-1">
            <div class="lines flex">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>

            <span><?= Cms::getBlockField('index-title-section-1','inscription') ?></span>

            <div class="lines reversed flex">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>

        <div class="el-2 title type-1 var-a anim-2">
            <?= Cms::getBlockField('index-title-section-1','title') ?>
        </div>

        <div class="buttons-outer anim-3">
            <a class="button type-3 var-a min-width-230" href="<?= Url::to(['/site/zakaz-torta']) ?>">
                <?= Cms::getBlockField('index-title-section-1','link2_title') ?>
            </a>
            <a class="button type-3 var-a min-width-230" href="<?= Url::to(['/confectionery']) ?>">
                <?= Cms::getBlockField('index-title-section-1','link_title') ?>
            </a>
        </div>

        <div class="socials var-a flex middle">
            <a href="<?= Settings::get('contacts.facebook') ?>" target="_blank" rel="nofollow">
                <i class="fa fa-facebook" aria-hidden="true"></i>
            </a>

            <a href="<?= Settings::get('contacts.instagram') ?>" target="_blank" rel="nofollow">
                <i class="fa fa-instagram" style="font-size: 18px;" aria-hidden="true"></i>
            </a>

            <a href="<?= Settings::get('contacts.pinterest') ?>" target="_blank" rel="nofollow">
                <i class="fa fa-pinterest-p" aria-hidden="true"></i>
            </a>
        </div>
    </div>

    <div class="tip-arrow-outer anchors">
        <a class="tip-arrow" href="#section_2">
            <img src="/img/svg/arrow.svg" alt="Arrow">
        </a>
    </div>

    <img class="smear-top" src="/img/smear_top.png" alt="Smear">
</section>

<section class="type-2 var-a" id="section_2">
    <div class="wrapper">
        <?= Popular::widget() ?>
    </div>
</section>

<section class="type-3" id="section_3">
    <div class="wrapper">
        <img class="smear-bottom" src="/img/smear_bottom.png" alt="Smear">
        <?= About::widget() ?>
        <img class="smear-top" src="/img/smear_top.png" alt="Smear">
    </div>
</section>

<section class="type-4" id="section_4">
    <?= Reviews::widget(['praiseUs' => true]) ?>
</section>

<section class="type-5" id="section_5">
    <div class="wrapper">
        <?= Contacts::widget() ?>
    </div>
</section>

<?php if ($popup): ?>
    <div id="home-popup" class="pop-up flex middle center">
        <div class="inner">
            <div class="title type-3 var-a">
                <?= $popup ?>
            </div>
        </div>
    </div>
    <?php
    $this->registerJs('smiyan.showHomePopup();');
endif; ?>
