<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 * @var $this \yii\web\View
 */
use app\helpers\Helper;
use app\helpers\Settings;
use app\modules\shop\models\Shop;
use yii\helpers\Url;

?>

<footer>
    <div class="wrapper">
        <div class="main-row row">
            <div class="el-1 small-12 medium-6 large-3 column flex direction-column space-between wrap">
                <div class="logo_button-outer">
                    <a class="logo" href="<?= Url::to(['/']) ?>">
                        <img src="/img/svg/logo_black.svg" alt="Logo">
                    </a>

                    <a class="button-el-1 button type-3" href="<?= Url::to(['/site/zakaz-torta']) ?>"><?= Yii::t('app', 'Торт на заказ') ?></a>
                </div>

                <div class="text text-1 var-b">©<?= date('Y') ?> <?= Yii::t('app', 'СМІЯН Все права защищены') ?></div>
            </div>

            <div class="small-12 medium-6 large-3 column">
                <div class="el-2">
                    <ul class="anchors">
                        <li>
                            <a class="text text-5 var-a" href="<?= Url::to(['/confectionery']) ?>">
                                <?= Yii::t('app', 'Кондитерская') ?>
                            </a>
                        </li>
                        <li>
                            <a class="text text-5 var-a" href="<?= Url::to(['/site/souvenirs']) ?>">
                                <?= Yii::t('app', 'Сувениры') ?>
                            </a>
                        </li>
                        <li>
                            <a class="text text-5 var-a" href="<?= Url::to(['/#section_3']) ?>">
                                <?= Yii::t('app', 'О нас') ?>
                            </a>
                        </li>
                        <li>
                            <a class="text text-5 var-a" href="<?= Url::to(['/site/klientam']) ?>">
                                <?= Yii::t('app', 'Корпоративным клиентам') ?>
                            </a>
                        </li>
                        <li>
                            <a class="text text-5 var-a" href="<?= Url::to(['/blog']) ?>">
                                <?= Yii::t('app', 'Активности') ?>
                            </a>
                        </li>
                        <li>
                            <a class="text text-5 var-a" href="<?= Url::to(['/reviews/site']) ?>">
                                <?= Yii::t('app', 'Отзывы') ?>
                            </a>
                        </li>
                        <li>
                            <a class="text text-5 var-a" href="<?= Url::to(['/site/dostavka']) ?>">
                                <?= Yii::t('app', 'Доставка') ?>
                            </a>
                        </li>
                        <li>
                            <a class="text text-5 var-a" href="<?= Url::to(['/site/contacts']) ?>">
                                <?= Yii::t('app', 'Контакты') ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="small-12 medium-6 large-3 column">
                <div class="el-3">
                    <?php foreach (Shop::find()->all() as $shop): ?>
                        <div class="element">
                            <div class="text-el-1 text text-5 var-a">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <?= $shop->name ?>
                            </div>

                            <div class="text text-2 var-d">
                                <?= $shop->schedule ?>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>

            <div class="small-12 medium-6 large-3 column">
                <div class="el-4">
                    <div class="text-el-1">
                        <a class="link type-3" href="tel:<?= Helper::trimPhoneNumber(Settings::get('contacts.phone')) ?>">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <span><?= Settings::get('contacts.phone') ?></span>
                        </a>
                    </div>
                    
                    <div class="text-el-1">
                        <a class="link type-3" href="mailto:<?= Settings::get('contacts.email') ?>">
                            <i class="fa fa-envelope var-a" aria-hidden="true"></i>
                            <span><?= Settings::get('contacts.email') ?></span>
                        </a>
                    </div>

                    <div class="socials var-b flex middle">
                        <a href="<?= Settings::get('contacts.facebook') ?>" target="_blank" rel="nofollow">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>

                        <a href="<?= Settings::get('contacts.instagram') ?>" target="_blank" rel="nofollow">
                            <i class="fa fa-instagram" style="font-size: 18px;" aria-hidden="true"></i>
                        </a>

                        <a href="<?= Settings::get('contacts.pinterest') ?>" target="_blank" rel="nofollow">
                            <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
