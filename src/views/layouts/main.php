<?php

use app\assets\AppAsset;
use app\helpers\Cms;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="format-detection" content="telephone=no">

    <link rel="shortcut icon" href="/img/favicon.png">

    <!-- ADAPTIVE -->
    <meta name="viewport" content="width=device-width">
    <!-- ADAPTIVE END -->

    <!-- LINKS -->
    <link href="https://fonts.googleapis.com/css?family=Cormorant+Infant:300,400,500,600|Roboto:300,400,500&amp;subset=cyrillic"
          rel="stylesheet">
    <!-- LINKS END -->

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?= Cms::getBlock('head') ?>
    <?php $this->head() ?>
</head>
<body>
<?= $this->render('_fb') ?>
<?php $this->beginBody() ?>
<style>
    .loader {
        background-color: #fff;
        width: 100%;
        height: 100%;
        position: fixed;
        top: 0px;
        left: 0px;
        z-index: 333;
    }
</style>

<div class="loader"></div>

<div class="dark-background"></div>

<div class="discount-pop-up flex middle center">
    <div class="inner">
        <div class="title type-3 var-a">
            <?= Yii::t('app', 'Поздравляем! <br> Скидка активирована.') ?>
        </div>
    </div>
</div>

<?= $this->render('_header') ?>

<?= $content ?>

<?= $this->render('_footer') ?>

<!-- SCRIPTS -->
<script>
    var mapMarkerUrl = '/img/map_marker_inactive.png',
        mapMarkerActiveUrl = '/img/map_marker_active.png';
</script>

<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHxSvzhImhRQU436Dh312eH9MVpC2wumQ">
</script>
<!-- SCRIPTS END -->

<div id="develop-popup" class="pop-up flex middle center">
    <div class="inner">
        <div class="title type-3 var-a">
            Сайт находится на этапе разработки. <br>
            Оплата заказов временно недоступна. <br>
            По всем вопросам обращайтесь +380442237822
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>