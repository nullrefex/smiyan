<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $this \yii\web\View
 * @var $categories \app\modules\category\models\Category[]
 */
use app\modules\category\helpers\Helper as CategoryHelper;
use app\modules\product\widgets\ProductCard;
use app\widgets\Title;

$this->title = Yii::t('app', 'Кондитерская');
?>

<section class="type-6 var-a">
    <div class="wrapper">

        <?= Title::widget([
            'title' => Yii::t('app', 'Кондитерская'),
            'subtitle' => Yii::t('app','Our best'),
        ]) ?>

        <div class="blocks type-1 flex wrap">
            <div class="block type-1">
                <div class="category-title-adaptive">
                    <?php foreach ($categories as $index => $category): ?>
                        <div class="item flex middle <?= ($index == 0) ? 'active' : '' ?>"
                             data-category="<?= $category->id ?>">
                            <div class="icon var-b "> <!-- @todo fix small css class -->
                                <?= CategoryHelper::getCategorySvg($category) ?>
                            </div>

                            <div class="text">
                                <?= $category->title ?>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>

                <div class="list type-1">

                    <?php foreach ($categories as $index => $category): ?>
                        <div class="item flex middle <?= ($index == 0) ? 'active' : '' ?>"
                             data-category="<?= $category->id ?>">
                            <div class="icon var-b "> <!-- @todo fix small css class -->
                                <?= CategoryHelper::getCategorySvg($category) ?>
                            </div>

                            <div class="text">
                                <?= $category->title ?>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>

            <div class="block type-2">
                <div class="scrollbar-outer flex wrap">
                    <?php foreach ($categories as $index => $category): ?>
                        <div class="divider" data-category="<?= $category->id ?>"></div>
                        <?php foreach ($category->products as $product): ?>
                            <?= ProductCard::widget(['model' => $product]) ?>
                        <?php endforeach ?>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</section>
