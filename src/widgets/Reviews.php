<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\widgets;


use app\helpers\Languages;
use app\modules\reviews\models\Reviews as ReviewsModel;
use yii\base\Widget;

class Reviews extends Widget
{
    public $praiseUs;

    /**
     * @return string
     */
    public function run()
    {
        $reviews = ReviewsModel::find()
            ->where([
                'approved' => ReviewsModel::APPROVED,
                'language' => Languages::get()->getId(),
            ])->all();

        if (count($reviews)) {
            return $this->render('reviews', [
                'reviews' => $reviews,
                'praiseUs' => $this->praiseUs,
            ]);
        }
    }
}