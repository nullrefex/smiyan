<?php

namespace app\widgets;

use app\components\ShoppingCart;
use app\traits\CartTrait;
use Yii;
use yii\base\Widget;
use yii\data\ArrayDataProvider;
use yii\widgets\ListView;

class Cart extends Widget
{
    use CartTrait;
    /**
     * @return string
     */
    public function run()
    {
        if ($discount = Yii::$app->request->post('discount')) {
            $this->cart->on(ShoppingCart::EVENT_COST_CALCULATION, function ($event) use ($discount) {
                $event->discountValue = $event->baseCost * ($discount ? 0.1 : 0);
            });
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => Yii::$app->cart->getPositions(),
        ]);
        return ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '@app/widgets/views/cart',
                'options' => [
                    'tag' => 'div',
                    'class' => 'list type-1',
                ],
                'itemOptions' => function ($model, $key, $index, $widget) {
                    return [
                        'tag' => 'div',
                        'class' => 'item flex space-between middle wrap',
                    ];
                },
                'summary' => '',
                'emptyText' => Yii::t('app', 'Cart is empty.'),
            ]
        );
    }
}