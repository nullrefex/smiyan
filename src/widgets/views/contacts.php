<?php
/**
 * @var $this \yii\web\View
 * @var $settings \pheme\settings\components\Settings
 * @var $shops \app\modules\shop\models\Shop[]
 */
use app\helpers\Cms;
use app\helpers\Helper;

?>
<img class="smear-bottom" src="/img/smear_bottom.png" alt="Smear">

<div class="row type-1">
    <div class="el-1 column">
        <div class="el-2">
            <div class="title type-2 var-b flex inline middle">
                <div class="lines flex">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>

                <span><?= Cms::getBlockField('index-title-section-5','inscription') ?></span>

                <div class="lines reversed flex">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>

        <div class="el-3">
            <div class="title type-1 var-b">
                <img class="var-b" src="/img/smear_red_1.png" alt="Smear">
                <?= Cms::getBlockField('index-title-section-5','title') ?>
            </div>
        </div>
    </div>
</div>

<div class="row type-2">
    <div class="column">
        <div class="blocks type-1 flex space-between wrap">
            <div class="block type-1">
                <div class="elements type-1 flex space-between wrap">
                    <a class="link type-2"
                       href="tel:<?= Helper::trimPhoneNumber($settings->get('contacts.phone')) ?>">
                        <i class="fa fa-phone" aria-hidden="true"></i>

                        <span><?= $settings->get('contacts.phone') ?></span>
                    </a>

                    <a class="link type-2" href="mailto:<?= $settings->get('contacts.email') ?>">
                        <i class="fa fa-envelope var-a" aria-hidden="true"></i>

                        <span><?= $settings->get('contacts.email') ?></span>
                    </a>
                </div>

                <div class="lines flex">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>

                <div class="elements type-2 flex space-between wrap">
                    <?php foreach ($shops as $shop): ?>
                        <div class="element">
                            <div class="el-4 text text-5 var-a">
                                <?= $shop->name ?>
                            </div>

                            <div class="el-5 text text-2 var-d">
                                <?= $shop->schedule ?>
                            </div>

                            <div class="link type-1 var-a" data-lat="<?= $shop->lat ?>"
                                 data-lng="<?= $shop->lng ?>">
                                <?= Yii::t('app', 'На карте') ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>

            <div class="block type-2">
                <div class="map" id="map">
                    <?php foreach ($shops as $shop): ?>
                        <div class="marker" data-lat="<?= $shop->lat ?>" data-lng="<?= $shop->lng ?>"></div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
