<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var string $subtitle
 * @var string $title
 */
?>

<div class="row">
    <div class="el-1 column">
        <div class="el-2">
            <div class="title type-2 var-b flex inline middle">
                <div class="lines flex">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>

                <span><?= $subtitle ?></span>

                <div class="lines reversed flex">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>

        <div class="el-3">
            <div class="title type-1 var-b">
                <img src="/img/smear_red_1.png" alt="Smear">
                <?= $title ?>
            </div>
        </div>
    </div>
</div>
