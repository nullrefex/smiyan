<?php


/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 * @var $model
 * @var $this \yii\web\View
 */
use yii\widgets\ActiveForm;

if ($model->isProcessed) {
    $this->registerJs('smiyan.showCallbackPopup();');
}

?>

<div class="back-call-button-outer" role="button">
    <div class="el-1 flex middle center">
        <i class="fa fa-phone" aria-hidden="true"></i>
    </div>

    <div class="el-2">
        <div class="inner flex middle">
            <span><?= Yii::t('app', 'Обратный звонок') ?></span>
            <i class="fa fa-phone" aria-hidden="true"></i>
        </div>
    </div>
</div>

<div id="backcall-popup" class="pop-up flex middle center">
    <div class="inner">
        <?php $form = ActiveForm::begin([
            'fieldConfig' => [
                'template' => "{input}\n{hint}\n{error}",
            ],
        ]) ?>
        <div class="inputs">
            <div class="input-outer">
                <?= $form->field($model, 'name')
                    ->textInput(['placeholder' => Yii::t('app', 'Имя')]) ?>
            </div>

            <div class="input-outer ">
                <?= $form->field($model, 'email')
                    ->textInput(['placeholder' => Yii::t('app', 'Почта')]) ?>
            </div>

            <div class="input-outer ">
                <?= $form->field($model, 'telephone')
                    ->textInput(['placeholder' => Yii::t('app', 'Номер телефона')]) ?>
            </div>

            <div class="input-outer submit">
                <input class="button type-1" type="submit" value="<?= Yii::t('app', 'Отправить') ?>">
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>


<div id="backcall-successs-popup" class="pop-up flex middle center">
    <div class="inner">
        <div class="title type-3 var-a">
            <?= Yii::t('app', 'Благодарим за заявку. <br> В скором времени мы свяжемся с вами') ?>
        </div>
    </div>
</div>
