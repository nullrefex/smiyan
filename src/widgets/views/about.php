<?php
/**
 * @var $this \yii\web\View
 */
use app\helpers\Cms;

?>

<div class="row type-1">
    <div class="el-1 column">
        <div class="el-2">
            <div class="title type-2 var-b flex inline middle">
                <div class="lines flex">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>

                <span><?= Cms::getBlockField('index-title-section-3', 'inscription') ?></span>

                <div class="lines reversed flex">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>

        <div class="el-3">
            <div class="title type-1 var-b">
                <img src="/img/smear_red_1.png" alt="Smear">
                <?= Cms::getBlockField('index-title-section-3', 'title') ?>
            </div>
        </div>
    </div>
</div>

<div class="row type-2">
    <div class="column">
        <div class="blocks type-1 flex space-between wrap">
            <div class="block type-1">
                <img src="/img/image_1.png" alt="Cake">
            </div>

            <?= Cms::getBlock('how-we-work') ?>
        </div>
    </div>
</div>

<div class="row type-3">
    <div class="column">
        <div class="inner">
            <div class="frame-1"></div>
            <div class="frame-2"></div>
            <?= Cms::getBlock('slogan') ?>
        </div>
    </div>
</div>