<?php
/**
 * @var $this \yii\web\View
 * @var $reviews \app\modules\reviews\models\Reviews[]
 */
use app\helpers\Cms;
use yii\helpers\Url;

?>
<div class="wrapper">
    <div class="row type-1">
        <div class="el-1 column">
            <div class="el-2">
                <div class="title type-2 var-b flex inline middle">
                    <div class="lines flex">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>

                    <span><?= Cms::getBlockField('index-title-section-4','inscription') ?></span>

                    <div class="lines reversed flex">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>

            <div class="el-3">
                <div class="title type-1 var-b">
                    <img src="/img/smear_red_1.png" alt="Smear">

                    <?= Cms::getBlockField('index-title-section-4','title') ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row type-3">
        <div class="column">
            <div class="review">
                <div class="inner">
                    <div class="another-review left prev flex middle wrap">
                        <div class="list">
                            <div><?= $reviews[count($reviews)-1]->customer_name; ?></div>

                            <?php
                                for ($i=0; $i < count($reviews)-1; $i++) { 
                                    ?>
                                        <div><?= $reviews[$i]->customer_name ?></div>
                                    <?php
                                }
                            ?>
                        </div>

                        <img class="arrow" src="/img/svg/arrow_black.svg" alt="Arrow">
                    </div>

                    <div class="another-review right next flex middle wrap">
                        <div class="list">
                            <?php
                                for ($i=1; $i < count($reviews); $i++) { 
                                    ?>
                                        <div><?= $reviews[$i]->customer_name ?></div>
                                    <?php
                                }
                            ?>

                            <div><?= $reviews[0]->customer_name; ?></div>
                        </div>

                        <img class="arrow" src="/img/svg/arrow_black.svg" alt="Arrow">
                    </div>

                    <div class="frame-1"></div>
                    <div class="frame-2"></div>

                    <div class="slides-counter flex middle">
                        <div class="current">1</div>
                        /
                        <div class="total">5</div>
                    </div>

                    <div class="review-row flex space-between middle">
                        <div class="blocks type-1 flex space-between wrap">
                            <div class="block type-1">
                                <div class="list type-1">
                                    <?php foreach ($reviews as $review): ?>
                                        <div class="title type-5 var-a">
                                            <?= $review->getShopName() ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>

                                <div class="lines">
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                            </div>

                            <div class="block type-2">
                                <div class="list type-2">
                                    <?php foreach ($reviews as $review): ?>
                                        <div class="element">
                                            <div class="el-4 title type-3 var-a">
                                                <?= $review->customer_name ?>
                                            </div>

                                            <div class="el-5 stars">
                                                <?php for ($i = 0; $i < 5; $i++): ?>
                                                    <i class="<?= $i < $review->rating ? 'active' : '' ?> fa fa-star"
                                                       aria-hidden="true"></i>
                                                <?php endfor; ?>
                                            </div>

                                            <div class="el-6 text text-4 var-a">
                                                <?= \Yii::$app->formatter->asDate($review->created_at, 'php:d.m.Y'); ?>
                                            </div>

                                            <div class="content var-a">
                                                <p>
                                                    <?= $review->message ?>
                                                </p>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if ($praiseUs): ?>
        <div class="row type-3">
            <div class="el-7 column">
                <a class="link type-1 var-a" href="<?= Url::to(['/reviews/site#review']) ?>">
                    <?= Cms::getBlockField('index-title-section-4','link_title') ?>
                    <img src="/img/svg/arrow_red.svg" alt="Arrow">
                </a>
            </div>
        </div>
    <?php endif; ?>
</div>