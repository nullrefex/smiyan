<?php

/**
 * @var \nullref\core\objects\Language[] $languages
 */
use app\helpers\Languages;

$this->registerJs(<<<JS
    $('.language-outer').on('click', '.language-list .text', function(event) {
        document.location = $(this).data('url');
        event.preventDefault();
    });
JS
)
?>
<div class="language-outer" style="display: none;">
    <div class="current text text-1 var-a"><?= Languages::getShortNames()[Languages::get()->getSlug()] ?></div>
    <div class="language-list">
        <?php foreach ($languages as $key => $language): ?>
            <div class="text text-1 var-a" data-url="<?= $language['url'] ?>"><?= $language['label'] ?></div>
        <?php endforeach; ?>
    </div>
</div>