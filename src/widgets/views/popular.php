<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $popular \app\modules\product\models\Product[]
 */
use app\helpers\Cms;
use app\helpers\Image;

?>
<div class="row">
    <div class="el-1 column">
        <div class="el-2">
            <div class="title type-2 var-b flex inline middle">
                <div class="lines flex">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>

                <span><?= Cms::getBlockField('index-title-section-2','inscription') ?></span>

                <div class="lines reversed flex">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>

        <div class="el-3">
            <div class="title type-1 var-b">
                <img src="/img/smear_red_1.png" alt="Smear">

                <?= Cms::getBlockField('index-title-section-2','title') ?>
            </div>
        </div>
    </div>
</div>

<?php
/** @var \app\modules\product\models\Product[] $popular_2 */
$popular_2 = array_values($popular);
if($popular_2): ?>

<div class="row">
    <div class="column">
        <div class="product">
            <div class="side left">
                <div class="images">
                    <img src="<?= Image::getThumbnail($popular_2[count($popular_2) - 1]->image, 450, 600) ?>"
                         alt="<?= $popular_2[count($popular_2) - 1]->getAlt() ?>">

                    <?php for ($i = 0; $i < count($popular_2) - 1; $i++): ?>
                        <img src="<?= Image::getThumbnail($popular_2[$i]->image, 450, 600) ?>"
                             alt="<?= $popular_2[$i]->getAlt() ?>">
                    <?php endfor ?>
                </div>
            </div>

            <div class="side right">
                <div class="images">
                    <?php
                        for ($i=1; $i < count($popular_2); $i++) { 
                            ?>
                            <img src="<?= Image::getThumbnail($popular_2[$i]->image, 450, 600) ?>"
                                 alt="<?= $popular_2[$i]->getAlt()?>">
                            <?php
                        }
                    ?>

                    <img src="<?= Image::getThumbnail($popular_2[0]->image, 450, 600) ?>"
                         alt="<?= $popular_2[0]->getAlt() ?>">
                </div>
            </div>

            <div class="inner">
                <div class="another-product left prev flex middle wrap">
                    <div class="list">
                        <div><?= $popular_2[count($popular_2)-1]->name ?></div>

                        <?php
                            for ($i=0; $i < count($popular_2)-1; $i++) { 
                                ?>
                                    <div><?= $popular_2[$i]->name ?></div>
                                <?php
                            }
                        ?>
                    </div>

                    <img class="arrow" src="/img/svg/arrow_black.svg" alt="Arrow">
                </div>

                <div class="another-product right next flex middle wrap">
                    <div class="list">
                        <?php
                            for ($i=1; $i < count($popular_2); $i++) { 
                                ?>
                                    <div><?= $popular_2[$i]->name ?></div>
                                <?php
                            }
                        ?>

                        <div><?= $popular_2[0]->name ?></div>
                    </div>

                    <img class="arrow" src="/img/svg/arrow_black.svg" alt="Arrow">
                </div>

                <div class="frame-1"></div>
                <div class="frame-2"></div>

                <div class="slides-counter flex middle">
                    <div class="current">1</div>
                    /
                    <div class="total"><?= count($popular) ?></div>
                </div>

                <div class="product-row flex space-between middle wrap">
                    <div class="images">
                        <?php foreach ($popular as $product): ?>
                            <img src="<?= Image::getThumbnail($product->image, 450, 600) ?>"
                                 alt="<?= $product->getAlt() ?>">
                        <?php endforeach; ?>
                    </div>

                    <div class="info-outer">
                        <?php foreach ($popular as $product): ?>
                            <div class="info">
                                <div class="text-info-outer">
                                    <div class="el-4 title type-3 var-a">
                                        <?= $product->name ?>
                                    </div>

                                    <div class="el-5 content var-a">
                                        <p>
                                            <?= $product->description ?>
                                        </p>
                                    </div>
                                </div>

                                <?php if (!$product->isSingle()): ?>
                                    <div class="variants el-6">
                                        <?php foreach ($variants = $product->variants as $variant): ?>
                                            <div class="variant flex middle space-between">
                                                <div class="icon">
                                                    <img src="/img/svg/part_<?= $variant->getSizeIcon() ?>.svg"
                                                         alt="Part">
                                                </div>

                                                <div class="weight text text-2 var-d">
                                                    <?= $variant->options['weight'] ?> <?= Yii::t('app', 'г') ?>
                                                </div>

                                                <div class="price title type-4 var-a"
                                                     data-price="<?= Yii::$app->formatter->asInteger($variant->price) ?>">
                                                    <?= Yii::$app->formatter->asInteger($variant->price) ?> ₴
                                                </div>

                                                <div class="counter flex middle">
                                                    <div class="minus button type-2"></div>
                                                    <div class="variant-total text-3 var-a"
                                                         data-id="<?= $variant->id ?>">0
                                                    </div>
                                                    <div class="plus button type-2"></div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>

                                <?php endif ?>

                                <div class="total flex middle">
                                    <div class="el-7 number title type-4 var-a" data-id="<?= $product->id ?>">

                                        <?php if ($product->isSingle()): ?>
                                            <span><?= Yii::$app->formatter->asInteger($product->price) ?></span> ₴
                                        <?php else: ?>
                                            <span>0</span> ₴
                                        <?php endif ?>
                                    </div>

                                    <a class="button type-1 add-various"><?= Yii::t('app', 'В корзину') ?></a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<div class="row">
    <div class="el-8 column">
        <a class="link type-1 var-a" href="/confectionery">
            <?= Cms::getBlockField('index-title-section-2','link_title') ?>
            <img src="/img/svg/arrow_red.svg" alt="Arrow">
        </a>
    </div>
</div>