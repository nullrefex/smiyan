<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\widgets;


use app\models\Callback as CallbackModel;
use Yii;
use yii\base\Widget;

class Callback extends Widget
{
    public function run()
    {
        $model = new CallbackModel();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->process();
        }
        return $this->render('callback', [
            'model' => $model,
        ]);
    }
}