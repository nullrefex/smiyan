<?php

namespace app\migrations;

use pheme\settings\models\Setting;
use yii\db\Migration;

class M170708175853Settings__add_delivery_price extends Migration
{
    public function up()
    {
        $this->insert(Setting::tableName(), [
            'type' => 'float',
            'section' => 'order',
            'key' => 'delivery_price',
            'value' => '100',
            'active' => '1',
            'created' => '2017-07-08 20:43:30',
            'modified' => null,
        ]);
    }

    public function down()
    {
        $this->delete(Setting::tableName(), ['section' => 'order', 'key' => 'delivery_price']);
        return true;
    }
}
