<?php

namespace app\migrations;

use pheme\settings\models\Setting;
use yii\db\Migration;

class M170708175852Settings__add_video extends Migration
{
    public function up()
    {
        $this->insert(Setting::tableName(), [
            'type' => 'string',
            'section' => 'main',
            'key' => 'video',
            'value' => '#',
            'active' => '1',
            'created' => '2017-07-08 20:43:30',
            'modified' => null,
        ]);
    }

    public function down()
    {
        $this->delete(Setting::tableName(), ['section' => 'main', 'key' => 'video']);
        return true;
    }
}
