<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var \app\models\Callback $model
 */
?>

<h3>Обратный звонок</h3>
<p>Имя: <?= $model->name ?></p>
<p>Телефон: <?= $model->telephone ?></p>
<p>Почта: <?= $model->email ?></p>