<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var \app\models\ClientRequest $model
 */
?>

<h3>Сообщение от корпоративного клиента</h3>
<p>Имя: <?= $model->name ?></p>
<p>Почта или телефон: <?= $model->contactInfo ?></p>
<p>Сообщение: <?= $model->message ?></p>