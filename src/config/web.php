<?php

$params = require(__DIR__ . '/params.php');
$modules = require(__DIR__ . '/modules.php');

$config = [
    'id' => 'smiyan',
    'name' => 'Smiyan',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'runtimePath' => dirname(dirname(__DIR__)) . '/runtime',
    'bootstrap' => [
        'category', // app\modules\category\Module
        'log',
        'languageManager',
        \nullref\cms\Bootstrap::class,
    ],
    'language' => 'ru',
    'components' => [
        'assetManager' => [
            'class' => \yii\web\AssetManager::class,
            'bundles' => [
                mranger\load_more_pager\LoadMorePagerWidgetAsset::class => [
                    'sourcePath' => '@webroot/vendors/load-more-pagination',
                ],
                nullref\fulladmin\assets\AdminAsset::class => [
                    'js' => [
                        'js/admin/scripts.js',
                        'js/tools.js',
                    ],
                ],
            ],
        ],
        'thumbler' => [
            'class' => \alexBond\thumbler\Thumbler::class,
            'sourcePath' => '@webroot',
            'thumbsPath' => '@webroot/thumbs',
        ],
        'formatter' => [
            'class' => app\components\Formatter::class,
        ],
        'cache' => [
            'class' => yii\caching\FileCache::class,
        ],
        'user' => [
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => yii\swiftmailer\Mailer::class,
            'useFileTransport' => YII_MAIL_USE_FILE_TRANSPORT,
        ],
        'i18n' => [
            'translations' => [
                '*' => ['class' => yii\i18n\PhpMessageSource::class],
                'admin' => ['class' => nullref\core\components\i18n\PhpMessageSource::class],
                'user' => ['class' => nullref\core\components\i18n\PhpMessageSource::class],
                'category' => ['class' => nullref\core\components\i18n\PhpMessageSource::class],
                'blog' => ['class' => nullref\core\components\i18n\PhpMessageSource::class],
                'cms' => ['class' => nullref\core\components\i18n\PhpMessageSource::class],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => '/site/index',
                '/souvenirs' => '/site/souvenirs',
                '/souvenir/<id:[\w-]+>' => '/site/souvenir',
                '/confectionery/<id:[\w-]+>' => '/confectionery/product',
                '/contacts' => '/site/contacts',
                '/reviews' => '/reviews/site',
                '/dostavka' => '/site/dostavka',
                '/klientam' => '/site/klientam',
                '/zakaz_torta' => '/site/zakaz-torta',
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'languageManager' => [
            'class' => app\components\LanguageManager::class,
            'languages' => [
                ['id' => 1, 'slug' => 'uk', 'title' => 'Українська'],
                ['id' => 2, 'slug' => 'ru', 'title' => 'Русский'],
                ['id' => 3, 'slug' => 'en', 'title' => 'English'],
            ],
        ],
        'request' => [
            'class' => 'app\components\Request',
            'baseUrl' => '',
            'cookieValidationKey' => 'RiAveGUdUACvWZppHVevMJRGd5Rij8uh',
        ],
        'settings' => [
            'class' => 'pheme\settings\components\Settings'
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@nullref/blog/views' => '@app/modules/blog/views',
                    '@nullref/fulladmin/views' => '@app/modules/admin/views',
                ],
            ],
        ],
        'cart' => [
            'class' => 'app\components\ShoppingCart',
            'cartId' => 'shopping_cart',
        ],
        'jsUrlManager' => [
            'class' => \dmirogin\js\urlmanager\JsUrlManager::class,
        ],
    ],
    'modules' => $modules,
    'params' => $params,
    'on beforeAction' => function ($event) {
        if (Yii::$app->controller instanceof \nullref\core\interfaces\IAdminController) {
            Yii::$app->language = 'ru';
        }
    },

];

$config['controllerMap']['elfinder-backend'] = [
    'class' => 'mihaildev\elfinder\Controller',
    'access' => ['@'],
    'disabledCommands' => ['netmount'],
    'roots' => [
        [
            'path' => 'uploads',
            'name' => 'Uploads',
            'options' => [
                'attributes' => [
                    [
                        'pattern' => '/\.(?:gitignore)$/',
                        'read' => false,
                        'write' => false,
                        'hidden' => true,
                        'locked' => false
                    ],
                ],
            ],
        ],
        [
            'path' => 'img',
            'name' => 'Images',
            'options' => [
                'attributes' => [
                    [
                        'pattern' => '/\.(?:gitignore)$/',
                        'read' => false,
                        'write' => false,
                        'hidden' => true,
                        'locked' => false
                    ],
                ],
            ],
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
