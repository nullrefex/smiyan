<?php
return [
    "user" => [
        "class" => "nullref\\fulladmin\\modules\\user\\Module"    
    ],
    "admin" => [
        "class" => "nullref\\fulladmin\\Module"    
    ],
    "category" => [
        "class" => "nullref\\category\\Module"    
    ],
    "cms" => [
        "class" => "nullref\\cms\\Module"    
    ]
    ];