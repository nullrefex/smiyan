<?php

$params = require(__DIR__ . '/params.php');
$modules = require(__DIR__ . '/modules.php');

Yii::setAlias('@webroot', dirname(dirname(__DIR__)) . '/web');
return [
    'id' => 'console-app',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'runtimePath' => dirname(dirname(__DIR__)) . '/runtime',
    'bootstrap' => ['log', 'core'],
    'modules' => $modules,
    'params' => $params,
    'components' => [
        'db' => require(__DIR__ . '/db.php'),
        'i18n' => [
            'translations' => [
                '*' => ['class' => 'yii\i18n\PhpMessageSource'],
            ],
        ],
        'languageManager' => [
            'class' => 'app\components\LanguageManager',
            'languages' => [
                ['id' => 1, 'slug' => 'uk', 'title' => 'Українська'],
                ['id' => 2, 'slug' => 'ru', 'title' => 'Русский'],
                ['id' => 3, 'slug' => 'en', 'title' => 'English'],
            ],
            'languageCookie' => false,
            'languageSession' => false,
        ],
    ]
];
