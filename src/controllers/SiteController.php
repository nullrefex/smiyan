<?php

namespace app\controllers;

use app\components\PageMetaTagsFilter;
use app\models\ClientRequest;
use app\modules\order\helpers\Mail;
use app\modules\order\models\Order;
use app\modules\product\models\Product;
use app\traits\CartTrait;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;


class SiteController extends Controller
{
    use CartTrait;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'seo' => [
                'class' => PageMetaTagsFilter::class,
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $popup = '';
        if ($orderId = Yii::$app->request->get('order')) {
            $order = Order::findByLpOrderId($orderId);
            if ($order && $order->popup_shown == 0 && $order->status == $order::STATUS_PAID) {
                $popup = Yii::t('app', 'Спасибо за покупку!');
                $order->popup_shown = true;
                $this->cart->removeAll();
                $order->save(false);
                Mail::sendPaidOrderMail($order);
                Mail::sendNewOrderCustomerMail($order, $order->customer_email);
            }
        }

        return $this->render('index', [
            'popup' => $popup
        ]);
    }

    /**
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * @return string
     */
    public function actionContacts()
    {
        return $this->render('contacts');
    }

    /**
     * @return string
     */
    public function actionDostavka()
    {
        return $this->render('dostavka');
    }

    /**
     * @return string
     */
    public function actionKlientam()
    {
        $model = new ClientRequest();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->process();
        }
        return $this->render('klientam', [
            'model' => $model,
        ]);

    }

    /**
     * @return string
     */
    public function actionZakazTorta()
    {
        return $this->render('zakaz-torta');
    }

    /**
     * @return string
     */
    public function actionSouvenirs()
    {
        $query = Product::find()->where(['type' => Product::TYPE_SOUVENIR]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => false,
            ],
        ]);

        return $this->render('souvenirs', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionSouvenir($id)
    {
        $model = Product::findByIdentifier($id);

        if ($model && $model->type == $model::TYPE_SOUVENIR) {
            return $this->render('souvenir', [
                'model' => $model,
            ]);
        }
        return $this->goHome();
    }
}
