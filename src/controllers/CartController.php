<?php

namespace app\controllers;

use app\components\PageMetaTagsFilter;
use app\components\ShoppingCart;
use app\modules\product\models\Product;
use app\traits\CartTrait;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class CartController extends Controller
{
    public function behaviors()
    {
        return [
            'seo' => [
                'class' => PageMetaTagsFilter::class,
            ],
        ];
    }

    use CartTrait;

    public function beforeAction($action)
    {
        $discount = Yii::$app->request->post('discount');
        if (isset($discount)) {
            $this->cart->on(ShoppingCart::EVENT_COST_CALCULATION, function ($event) use ($discount) {
                $event->discountValue = $event->baseCost * ($discount ? 0.1 : 0);
            });
        }
        return parent::beforeAction($action);
    }

    public function actionPut()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $amount = Yii::$app->request->post('amount');
            $id = Yii::$app->request->post('id');
            if ($id) {
                $model = Product::findOne($id);
                if ($model) {
                    if ($amount == 0) {
                        $this->cart->remove($model);
                        return ['count' => $this->cart->getCount()];
                    } else {
                        $this->cart->put($model->getCartPosition(), $amount);
                        $position = $this->cart->getPositionById($id);
                        return ['cost' => $position->getCost(true), 'count' => $this->cart->getCount()];
                    }
                }
            }
        }
    }

    public function actionUpdate()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $amount = Yii::$app->request->post('amount');
            $id = Yii::$app->request->post('id');
            if ($id) {
                $model = Product::findOne($id);
                if ($model) {
                    if ($amount == 0) {
                        $this->cart->remove($model);
                        return ['count' => $this->cart->getCount()];
                    } else {
                        $this->cart->update($model->getCartPosition(), $amount);
                        $position = $this->cart->getPositionById($id);
                        return ['cost' => $position->getCost(true), 'count' => $this->cart->getCount()];
                    }
                }
            }
        }
    }

    public function actionClear()
    {
        $this->cart->removeAll();
    }

    public function actionCheckCart()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['status' => $this->cart->positions ? true : false];
    }

    public function actionRemove($id)
    {
        $this->cart->removeById($id);

        $this->redirect(['cart/shopping-list']);
    }

    public function actionCheckPrice()
    {
        if (Yii::$app->request->isAjax) {
            return $this->asJson(['cost' => $this->cart->getCost(true)]);
        }
        return false;
    }
}