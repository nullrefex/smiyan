<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\helpers;


use Yii;
use yii\helpers\Url;

class Helper
{
    /**
     * @return array
     */
    public static function getInputFileOptions()
    {
        return [
            'buttonName' => Yii::t('app', 'Browse'),
            'language' => Yii::$app->language,
            'controller' => 'elfinder-backend',
            'filter' => 'image',
            'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
            'options' => ['class' => 'form-control'],
            'buttonOptions' => ['class' => 'btn btn-default'],
            'multiple' => false
        ];
    }

    /**
     * @param string $value
     * @return string
     */
    public static function trimPhoneNumber($value)
    {
        return str_replace([' ', '-'], '', $value);
    }

    /**
     * @return mixed|null|string
     */
    public static function getBackUrl()
    {
        $backUrl = Yii::$app->request->referrer;
        if (!$backUrl || strpos($backUrl, Yii::$app->request->hostInfo) === false) {
            $backUrl = Url::to('/', true);
        }
        return $backUrl;
    }
}