<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */

namespace app\helpers;

use nullref\core\components\LanguageManager;
use Yii;
use yii\helpers\ArrayHelper;

class Languages
{
    /**
     * @return array
     */
    public static function getMap()
    {
        return ArrayHelper::map(self::getManager()->getLanguages(), 'id', 'title');
    }

    /**
     * @return LanguageManager
     * @throws \yii\base\InvalidConfigException
     */
    public static function getManager()
    {
        return Yii::$app->get('languageManager');
    }

    /**
     * @return array
     */
    public static function getSlugMap()
    {
        $map = ArrayHelper::map(self::getManager()->getLanguages(), 'id', 'slug');
        return $map;
    }

    public static function getSlugedMap()
    {
        $map = ArrayHelper::map(self::getManager()->getLanguages(), 'slug', function ($item) {
            return $item;
        });
        return $map;
    }

    /**
     * @return array
     */
    public static function getShortNames()
    {
        return [
            'uk'=>'Ukr',
            'ru'=>'Rus',
            'en'=>'Eng',
        ];
    }

    /**
     * @return \nullref\core\interfaces\ILanguage
     */
    public static function get()
    {
        return self::getManager()->getLanguage();
    }

    /**
     * @param $langId
     * @return string
     */
    public static function getTitleById($langId)
    {
        $list = self::getManager()->getLanguages();

        if (isset($list[$langId])) {
            return $list[$langId]->getTitle();
        }
        return Yii::t('app', 'N\A');
    }
}