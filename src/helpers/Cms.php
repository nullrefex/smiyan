<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */


namespace app\helpers;


use mihaildev\elfinder\ElFinder;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\View;

class Cms
{
    protected static $_blockCache = [];

    /**
     * Merge option for file input
     * @param array $options
     * @return array
     */
    public static function getInputFileOptions($options = [])
    {
        $defaults = [
            'buttonName' => 'Обзор',
            'language' => 'ru',
            'controller' => 'elfinder-backend',
            'filter' => 'image',
            'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
            'options' => ['class' => 'form-control'],
            'buttonOptions' => ['class' => 'btn btn-default'],
            'multiple' => false
        ];

        return array_merge($defaults, $options);
    }

    public static function getEditorOptions($options = [])
    {

        list(, $footnotesUrl) = Yii::$app->assetManager->publish('@nullref/cms/assets/ckeditor-plugins/codemirror');
        Yii::$app->view->registerJs("CKEDITOR.plugins.addExternal( 'codemirror', '" . $footnotesUrl . "/','plugin.js');
        Object.keys(CKEDITOR.dtd.\$removeEmpty).forEach(function(key){CKEDITOR.dtd.\$removeEmpty[key] = 0;});
        ", View::POS_END);

        $defaults = [
            'id' => 'editor',
            'editorOptions' => [
                'preset' => 'full',
                'inline' => false,
                'extraPlugins' => 'codemirror',
                'allowedContent' => true,
                'basicEntities' => false,
                'entities' => false,
                'entities_greek' => false,
                'entities_latin' => false,
                'htmlEncodeOutput' => false,
                'entities_processNumerical' => false,
                'fillEmptyBlocks' => false,
                'fullPage' => false,
                'codemirror' => [
                    'autoCloseBrackets' => true,
                    'autoCloseTags' => true,
                    'autoFormatOnStart' => true,
                    'autoFormatOnUncomment' => true,
                    'continueComments' => true,
                    'enableCodeFolding' => true,
                    'enableCodeFormatting' => true,
                    'enableSearchTools' => true,
                    'highlightMatches' => true,
                    'indentWithTabs' => false,
                    'lineNumbers' => true,
                    'lineWrapping' => true,
                    'mode' => 'htmlmixed',
                    'matchBrackets' => true,
                    'matchTags' => true,
                    'showAutoCompleteButton' => true,
                    'showCommentButton' => true,
                    'showFormatButton' => true,
                    'showSearchButton' => true,
                    'showTrailingSpace' => true,
                    'showUncommentButton' => true,
                    'styleActiveLine' => true,
                    'theme' => 'default',
                    'useBeautify' => true,
                ],
            ],
        ];

        $defaults['editorOptions'] = ElFinder::ckeditorOptions('elfinder-backend', $defaults['editorOptions']);

        return ArrayHelper::merge($defaults, $options);
    }

    /**
     * @param $id
     * @return \app\modules\cms\components\BaseWidget
     */
    public static function getBlock($id)
    {
        if (!isset(self::$_blockCache[$id])) {
            self::$_blockCache[$id] = Yii::$app->getModule('cms')->get('blockManager')->getWidget($id);
        }
        return self::$_blockCache[$id];
    }

    /**
     * @param $id
     * @param $field
     * @return mixed
     */
    public static function getBlockField($id, $field)
    {
        try {

            $block = self::getBlock($id);
            if ($block == null || $block->getBlock() == null ) {
                return '';
            }
            if (in_array($field, $block->getBlock()->getMultilingualAttributes())) {
                return $block->{$field}[$block->getLanguageManager()->getLanguage()->getSlug()];
            }
            return $block->{$field};
        } catch (\Exception $exception) {
            return $field;
        }
    }
}