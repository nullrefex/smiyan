<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\helpers;


use Yii;

class Settings
{
    /**
     * @param string $key
     * @return mixed
     */
    public static function get($key)
    {
        return Yii::$app->get('settings')->get($key);
    }
}